﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Breakout
{
    class Block
    {
        protected RectangleShape shape;
        protected Vector2f center;
        protected int health;
        protected bool destroyed;
        protected float baseVelocity;
        protected float velocity;
        protected Texture texture;

        public Block(Vector2f position, int width, int height)
        {
            Console.WriteLine("neuer lok");
            shape = new RectangleShape(new Vector2f(width, height));
            Center = position;
            destroyed = false;
        }

        public void Destroy()
        {
            destroyed = true;
        }

        public Line[] getSides()
        {
            Line[] side = new Line[4];
			
			//Top
            side[0] = new Line(new Vector2f(Center.X - (shape.Size.X / 2), Center.Y - (shape.Size.Y / 2)), new Vector2f(Center.X + (shape.Size.X / 2), Center.Y - (shape.Size.Y / 2)));
            //Right
            side[1] = new Line(new Vector2f(Center.X + (shape.Size.X / 2), Center.Y - (shape.Size.Y / 2)), new Vector2f(Center.X + (shape.Size.X / 2), Center.Y + (shape.Size.Y / 2)));
            //Bottom
            side[2] = new Line(new Vector2f(Center.X - (shape.Size.X / 2), Center.Y + (shape.Size.Y / 2)), new Vector2f(Center.X + (shape.Size.X / 2), Center.Y + (shape.Size.Y / 2)));
            //Left
            side[3] = new Line(new Vector2f(Center.X - (shape.Size.X / 2), Center.Y - (shape.Size.Y / 2)), new Vector2f(Center.X - (shape.Size.X / 2), Center.Y + (shape.Size.Y / 2)));
            return side;
        }

        public RectangleShape Shape
        {
            get { return shape; }
        }

        public Vector2f Center
        {
            get { return center; }
            set
            {
                center = value;
                shape.Position = new Vector2f(center.X - (shape.Size.X / 2), center.Y - (shape.Size.Y / 2));
            }
        }

		public bool Destroyed
		{
			get { return destroyed; }
			set { destroyed = value; }
		}

		public int Health
        {
            get { return health; }
            set { health = value; }
        }
        
    }
}
