﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace Breakout
{
    class Ball
    {
        private CircleShape shape;
        private Vector2f center;
        private Vector2f direction;
        private float velocity;
        private float baseVelocity;
        private Texture texture;

        public Ball(Vector2f position, float radius)
        {
            shape = new CircleShape(radius);
            //shape.FillColor = Color.Red;
            Center = position;
            baseVelocity = .35f;
            Direction = new Vector2f(1f, -0.5f);
            texture = new Texture("./assets/ball.png");
            texture.Smooth = true;
            shape.Texture = texture;
        }

        public void Move()
        {
            Center += velocity * Direction;
        }

        public CircleShape Shape
        {
            get { return shape; }
        }

        public Vector2f Center
        {
            get { return center; }
            set
            {
                center = value;
                shape.Position = new Vector2f(center.X - (shape.Radius), center.Y - (shape.Radius));
            }
        }

        public Vector2f Direction
        {
            get { return direction; }
            set { direction = Vector.Unit(value); }
        }

        public float Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }

        public float BaseVelocity
        {
            get { return baseVelocity; }
            set { baseVelocity = value; }
        }
    }
}
