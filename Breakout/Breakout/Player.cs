﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SFML.Graphics;

namespace Breakout
{
    class Player : Block
    {
        public Player(Vector2f position, int width, int height) : base(position, width, height)
        {
            texture = new Texture("./assets/player.png");
            texture.Smooth = true;
            shape.Texture = texture;
            health = -1;
            baseVelocity = .25f;
        }

        public void Move()
        {
            Center = new Vector2f(Center.X + velocity, Center.Y);
        }

        public float BaseVelocity
        {
            get { return baseVelocity; }
            set { baseVelocity = value; }
        }

        public float Velocity
        {
            get { return velocity; }
            set { velocity = value; }
        }
    }
}
