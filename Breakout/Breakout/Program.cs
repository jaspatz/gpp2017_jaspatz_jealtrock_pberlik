﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace Breakout
{
    class Program
    {
        private bool running;
        private uint width = 543;
        private uint height = 693;
        private RenderWindow window;
        private int FPS;

        private Font font;
        private Text text_highscore;
        private Text  text_score;
        private Sprite sprite_background;
        private RectangleShape[] sprite_health;

        private Player player;
		private int health;
        private Ball ball;
        private Block[] level;
        private Block frame;
        private uint score;
        private uint highscore;
		private int aliveBlocks = 80;

		private Vertex[] line_ballVelocity;
        private bool oob = false;

        private void Run()
        {
            window = new RenderWindow(new VideoMode(width, height), "Breakout", Styles.Default);
            window.Closed += (sender, evtArgs) => running = false;
            sprite_background = new Sprite(new Texture("./assets/background-lvl-1.png"));
            font = new Font("./fonts/atari.ttf");

            text_score = new Text("00000000", font, 25);
            text_score.Position = new Vector2f(width - 210, 5);
            text_score.Color = Color.Yellow;
            text_score.Style = Text.Styles.Bold;

            text_highscore = new Text("Highscore: 00000000", font, 11);
            text_highscore.Position = new Vector2f(width - 210, 34);
            text_highscore.Color = Color.Yellow;
            text_highscore.Style = Text.Styles.Regular;

            running = true;
            GameRestart();

            float delta;
            float deltaAverage = 1;
            float deltaSum = 0;
            int deltaCount = 0;

            DateTime current_time;
            DateTime previous_time = DateTime.Now;
            float framesTillSecond = 0;

            while (running)
            {
                current_time = DateTime.Now;
                delta = (float) current_time.Subtract(previous_time).TotalMilliseconds;
                previous_time = current_time;

				window.DispatchEvents();

				//fps counting
				if (framesTillSecond + delta < 1000)
                {
                    FPS++;
                    framesTillSecond += delta;
                }
                else
                {
                    Console.WriteLine("--FPS: " + FPS);
                    FPS = 0;
                    framesTillSecond = 0;
                }

                if (deltaCount < 10)
                {
                    deltaSum += delta;
                    deltaCount++;
                }
                else
                {
                    deltaAverage = deltaSum / deltaCount;
                    deltaSum = 0;
                    deltaCount = 0;
                }

                Update(deltaAverage);
                Render();
            }
        }

        private void Update(float delta)
        {

            //--------------------------------------------------Player Movement

            if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
            {
                player.Velocity = delta * player.BaseVelocity;
                player.Move();
                if(player.Center.X + player.Shape.Size.X / 2 > width)
                {
                    player.Center = new Vector2f(width - player.Shape.Size.X / 2, player.Center.Y);
                }
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
            {
                player.Velocity = -delta * player.BaseVelocity;
                player.Move();
                if (player.Center.X - player.Shape.Size.X / 2 < 0)
                {
                    player.Center = new Vector2f(player.Shape.Size.X / 2, player.Center.Y);
                }
            }

            //--------------------------------------------------Ball Movement

            ball.Velocity = delta * ball.BaseVelocity;

            if ((ball.Center.X < 0 || ball.Center.X > width || ball.Center.Y < 0 || ball.Center.Y > height) && oob == false)
            {
                Console.WriteLine("Out of bounds || ball Center: " + ball.Center);
                Console.WriteLine("Out of bounds || Boundarys  : " + frame.Shape.GetGlobalBounds());
                oob = true;
            }

            if (!oob)
            {
                CheckAllCollisions();
            }
            Console.WriteLine("********************************************************ball alte position  -- " + ball.Center);
            ball.Move();
            Console.WriteLine("********************************************************ball moved in update-- " + ball.Center);
            Console.WriteLine("********************************************************ball moved in direct-- " + ball.Direction);
            Console.WriteLine("********************************************************ball moved in velocity " + ball.Velocity);
        }
        private bool CheckAllCollisions()
        {
            
            CheckCollisionWithBounds(ball);
			CheckCollisionWithPlayer(ball, player);
            //CheckCollisionWithBlock(ball, player);

			for(int i = 0; i < 80; i++)
			{
				if (!level[i].Destroyed)
				{
					CheckCollisionWithBlock(ball, level[i]);
				}
			}

            return false;
        }

        private bool CheckCollisionWithBounds(Ball ball)
        {
            Line[] bounds = new Line[4];
            bounds[0] = new Line(new Vector2f(0, 0), new Vector2f(width, 0));
            bounds[1] = new Line(new Vector2f(width, 0), new Vector2f(width, height));
            bounds[2] = new Line(new Vector2f(0, height), new Vector2f(width, height));
            bounds[3] = new Line(new Vector2f(0, 0), new Vector2f(0, height));

            Vector2f[] xPoint = new Vector2f[4];
            int indexOfNearestLine = 0;

            for (int i = 0; i < 4; i++)
            {
                Line ballToNewPos = new Line(ball.Center, ball.Center + ball.Velocity * ball.Direction);
                float t = ballToNewPos.IntersectionWithLine(bounds[i]);
                xPoint[i] = bounds[i].ProjectPointOnLine(ball.Center);
                if (new Line(ball.Center, xPoint[i]).Length <= new Line(ball.Center, xPoint[indexOfNearestLine]).Length && t > 0)
                {
                    indexOfNearestLine = i;
                }
            }
            Console.WriteLine("nearest line: " + indexOfNearestLine);
			if(indexOfNearestLine != 2)
			{
				if (CheckCollisionWithSide(ball, bounds[indexOfNearestLine], indexOfNearestLine))
				{
					return true;
				}
			}
			else
			{
				if (CheckCollisionWithSide(ball, bounds[2], 2)) //Ball ist nach unten gefallen
				{
					health -= 1;
					RoundRestart();
					return true;
				}
			}
            return false;
        }

		private bool CheckCollisionWithPlayer(Ball ball, Player player)
		{
			if(CheckCollisionWithBlock(ball, player))
			{

			}
			return false;
		}

		private bool CheckCollisionWithBlock(Ball ball, Block block)
        {
            Line[] side = block.getSides();
            Vector2f[] xPoint = new Vector2f[4];

            int indexOfNearestLine = 0;
			bool indexSet = false;

            for (int i = 0; i < 4; i++)
            {
                xPoint[i] = side[i].ProjectPointOnLine(ball.Center);
                if (block.Shape.GetGlobalBounds().Contains(xPoint[i].X, xPoint[i].Y))
                {
                    if (new Line(ball.Center, xPoint[i]).Length <= new Line(ball.Center, xPoint[indexOfNearestLine]).Length)
                    {
                        indexOfNearestLine = i;
						indexSet = true;
                    }
                }
            }
			if (indexSet == false) return false;
            if (CheckCollisionWithSide(ball, side[indexOfNearestLine], indexOfNearestLine))
            {
                Console.WriteLine("global bounds: " + block.Shape.GetGlobalBounds());
				block.Health--;
				if(block.Health == 0)
				{
					block.Destroy();
					aliveBlocks--;
					if(aliveBlocks == 0)
					{
						GameOver();
					}
					score += 10;
				}
				score += 10;
				text_score.DisplayedString = score.ToString("D8");
				return true;
            }

            return false;
        }
        
        private bool CheckCollisionWithSide(Ball ball, Line line, int i)
        {
            Line ballToNewPos = new Line(ball.Center, ball.Center + ball.Velocity * ball.Direction);

            if (ballToNewPos.IntersectionWithLine(line) <= 0) return false;

            Vector2f xPoint_ball = ballToNewPos.CrossPointWithLine(line);
            Vector2f xPoint_ball_line = line.ProjectPointOnLine(ball.Center);

            float a = new Line(xPoint_ball, xPoint_ball_line).Length;
            float b = line.DistanceToPoint(ball.Center);
            float c = new Line(ball.Center, xPoint_ball).Length;

            float sinus = a / c;
            float cosinus = b / c;

            float b2 = ball.Shape.Radius;
            float c2 = b2 / cosinus;
            float a2 = c2 / sinus;

            float distance = Vector.Length(ball.Velocity * ball.Direction);

            float t = (c - c2) / distance;

            if (t <= 1 && t >= -1)
            {
                
                Console.WriteLine("-------------------Collision id = " + i);
                Console.WriteLine("t von intersect = " + ballToNewPos.IntersectionWithLine(line));
                Console.WriteLine("c = " + c + " || c2 = " + c2);
                Console.WriteLine("xPoint_ball = " + xPoint_ball);
                Console.WriteLine("BallVelocity = " + ball.Velocity);

                HandleCollisionWithSide(ball, line, t, distance);
                return true;
            }
            else
            {
                Console.WriteLine("es wurde keine collision festgestellt, t = " + t);
            }
            return false;
        }

        private void HandleCollisionWithSide(Ball ball, Line side, float t, float distance)
        {
            float circleVelocity = ball.Velocity; //Die velocity speichern

            ball.Velocity = circleVelocity * t * 0.9999f; //aus rechnerischer ungenauigkeit (floats), dürfen wir den ball nur FAST (99,99%) an die Seite Bewegen
            ball.Move();

            ball.Direction = Vector.Mirror(ball.Direction, Vector.Normal(side.End - side.Start));

            float restT = (1 - t);
            ball.Velocity = circleVelocity * restT;
            Console.WriteLine("-t: " + t + " -rt: " + restT + " -distance: " + distance);
            Console.WriteLine("-neue velocity: " + ball.Velocity);
            Console.WriteLine("-neue position: " + ball.Center);
            CheckAllCollisions();
        }

        private void HandleCollisionWithCorner(Ball ball, Vector2f corner, float t)
        {
            
        }

        private void Render()
        {
            window.Clear(Color.Black);
            window.Draw(sprite_background);

            for (int i = 0; i < 80; i++)
            {
				if (!level[i].Destroyed)
				{
					window.Draw(level[i].Shape);
				}
            }

			line_ballVelocity = new Vertex[2];
			line_ballVelocity[0] = new Vertex(ball.Center);
			line_ballVelocity[1] = new Vertex(ball.Center + 50 * Vector.Unit(ball.Direction * ball.Velocity));
			window.Draw(line_ballVelocity, PrimitiveType.Lines);

            window.Draw(player.Shape);
            window.Draw(ball.Shape);

            for (int i = 0; i < health; i++)
            {
                window.Draw(sprite_health[i]);
            }
            window.Draw(text_score);
            window.Draw(text_highscore);

            window.Display();
        }

        private void GameRestart()
        {
            level = new Block[80];
            player = new Player(new Vector2f(width / 2, height - 40), 90, 17);
            frame = new Block(new Vector2f(width / 2, height / 2), (int) width, (int) height);

			health = 3;
			if(score > highscore)
			{
				highscore = score;
				text_highscore.DisplayedString = "Highscore: " + highscore.ToString("D8");
			}
			score = 0;
			text_score.DisplayedString = score.ToString("D8");

			sprite_health = new RectangleShape[health];
            for(int i = 0; i < health; i++)
            {
                sprite_health[i] = new RectangleShape(new Vector2f(40, 40));
                sprite_health[i].Texture = new Texture("./assets/ball2.png");
                sprite_health[i].Position = new Vector2f(5 + i * 50, 5);
                sprite_health[i].Texture.Smooth = true;
            }

            int index = 0;
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 10; j++)
                {
					if (i == 0)
					{
						level[index] = new StoneBlock(new Vector2f(29 + j * 54, 80 + 35 * i), 53, 30);
					}
					if (i >= 1 && i < 5)
					{
						level[index] = new WoodBlock(new Vector2f(29 + j * 54, 80 + 35 * i), 53, 30);
					}
					if (i >= 5)
					{
						level[index] = new LeafBlock(new Vector2f(29 + j * 54, 80 + 35 * i), 53, 30);
					}
                    index++;
                }
            }
            RoundRestart();
        }

        private void RoundRestart()
        {
            player.Center = new Vector2f(width / 2, height - 40);
            if(health < 0)
            {
                GameOver();
            }
            ball = new Ball(new Vector2f(width / 2, height - 100), 10);
        }

        private void GameOver()
        {
            GameRestart();
        }

        static void Main(string[] args)
        {
            Program game = new Program();
            game.Run();
        }
    }
}
