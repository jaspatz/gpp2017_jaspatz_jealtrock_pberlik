﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SFML.Graphics;

namespace Breakout
{
    class NormalBlock : Block
    {
        public NormalBlock(Vector2f position, int width, int height) : base(position, width, height)
        {
            shape.FillColor = Color.Red;
            health = 1;
        }
    }
}
