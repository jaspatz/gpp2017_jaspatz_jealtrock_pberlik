﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace Breakout
{
	static class Vector
	{

		public static Vector2f Unit(Vector2f vector)
		{
			float length = Length(vector);

			Vector2f unit = new Vector2f(vector.X / length, vector.Y / length);
			return unit;
		}
		public static Vector2f Normal(Vector2f vector)
		{
			return Unit(new Vector2f(-vector.Y, vector.X));
		}
		public static float Dot(Vector2f vector1, Vector2f vector2)
		{
			return vector1.X * vector2.X + vector1.Y * vector2.Y;
		}
		public static float Length(Vector2f vector)
		{
			return (float)Math.Sqrt(vector.X * vector.X + vector.Y * vector.Y);
		}
        public static Vector2f Mirror(Vector2f vector, Vector2f normal)
        {
            return vector - 2 * (Dot(vector, normal) * normal);
        }
	}
}
