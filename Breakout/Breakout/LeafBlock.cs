﻿using SFML.System;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Breakout
{
	class LeafBlock : Block
	{
		public LeafBlock(Vector2f position, int width, int height) : base(position, width, height)
		{
			texture = new Texture("./assets/leaf.png");
			texture.Smooth = true;
			shape.Texture = texture;
			health = 1;
		}
    }
}
