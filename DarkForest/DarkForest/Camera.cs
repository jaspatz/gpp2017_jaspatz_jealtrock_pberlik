﻿using DarkForest.Components.Interfaces;
using DarkForest.Entities;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest
{
    class Camera
    {
        public View View { get; set; }

        private Vector2f _boundarys;
        private Vector2f _deadzone;
        private Entity _followed;
        private int _width;
        private int _height;
        private Vector2f _offset;

        public Camera(Entity followed, Vector2f boundarys, Renderer renderer)
        {
            _followed = followed;
            _boundarys = boundarys;
            _deadzone = new Vector2f();

            View = new View(renderer.Window.DefaultView);
            _width = (int) renderer.Window.Size.X;
            _height = (int) renderer.Window.Size.Y;
            View.Center = followed.Position;
        }

        public void SetDeadZone(int width, int height, Vector2f offset = new Vector2f())
        {
            _offset = offset; // offset of the centre of the screen
            _deadzone = new Vector2f(width, height);
        }

        public void Update()
        {
            Vector2f position = new Vector2f(_followed.Position.X, _followed.Position.Y);
            Vector2f center = View.Center + _offset;
            
            //Check if player is out of deadzone. if so, camera moves with player
            if (position.X < (center.X - _deadzone.X/2))
            {
                float delta = Math.Abs(position.X - (center.X - (_deadzone.X / 2)));
                center.X = center.X - delta;
            }
            else if(position.X > (center.X + _deadzone.X / 2))
            {
                float delta = Math.Abs(position.X - (center.X + (_deadzone.X / 2)));
                center.X = center.X + delta;
            }
            if (position.Y < (center.Y - _deadzone.Y / 2))
            {
                float delta = Math.Abs(position.Y - (center.Y - (_deadzone.Y / 2)));
                center.Y = center.Y - delta;
            }
            else if (position.Y > (center.Y + _deadzone.Y / 2))
            {
                float delta = Math.Abs(position.Y - (center.Y + (_deadzone.Y / 2)));
                center.Y = center.Y + delta;
            }

            center = center - _offset;

            //If Map is smaller than window size, the camera needs to be fixed
            if (_width >= _boundarys.X && _height >= _boundarys.Y)
            {
                View.Center = new Vector2f((int)_boundarys.X / 2, (int)_boundarys.Y / 2);
                return;
            }

            if (_width >= _boundarys.X && _height < _boundarys.Y) //Nach Oben/unten offen
            {
                if (center.Y < (_height / 2))
                {
                    center.Y = _height / 2;
                }
                else if (center.Y > _boundarys.Y - (_height / 2))
                {
                    center.Y = _boundarys.Y - (_height / 2);
                }
                View.Center = new Vector2f((int)_boundarys.X / 2, (int)center.Y);
                return;
            }

            if (_width < _boundarys.X && _height >= _boundarys.Y) //Nach Rechts/links
            {
                if (center.X < (_width / 2))
                {
                    center.X = _width / 2;
                }
                else if (center.X > _boundarys.X - (_width / 2))
                {
                    center.X = _boundarys.X - (_width / 2);
                }
                View.Center = new Vector2f((int)center.X, (int)_boundarys.Y / 2);
                return;
            }

            //check if player is on the edge of world. If so, camera needs to stop scrolling
            if (center.X < (_width / 2))
            {
                center.X = _width / 2;
            }
            else if (center.X > _boundarys.X - (_width / 2))
            {
                center.X = _boundarys.X - (_width / 2);
            }
            if (center.Y < (_height / 2))
            {
                center.Y = _height / 2;
            }
            else if (center.Y > _boundarys.Y - (_height / 2))
            {
                center.Y = _boundarys.Y - (_height / 2);
            }
            center = new Vector2f((int)Math.Round(center.X), (int)Math.Round(center.Y));
            View.Center = center;
        }

    }
}
