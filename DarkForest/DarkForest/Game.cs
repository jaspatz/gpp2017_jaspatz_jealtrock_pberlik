﻿using DarkForest.Factories;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using DarkForest.TileMap;
using FarseerPhysics.Dynamics;
using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Loader;
using SFML.Audio;
using SFML.Window;
using DarkForest.Components.Components.Skills;
using DarkForest.Screens;

namespace DarkForest
{
    class Game
    {
        public static World World = new World(new Microsoft.Xna.Framework.Vector2());
        public static Random Random;

        public event EventHandler<GameOverEventArgs> OnGameOver;
        public event EventHandler<RoundStartedEventArgs> OnRoundStarted;
        public event EventHandler<RoundEndedEventArgs> OnRoundEnded;
        public event EventHandler<CleanupEventArgs> OnCleanUp;

        public int Level { get { return _level; } private set { _level = value; _totalEnemiesToSpawn += (int)(_enemy_inc * (_level / 2.0f)); Console.WriteLine(_totalEnemiesToSpawn + " soviele enemys"); } }
        public Entity Player { get; set; }
        public Map Map { get; set; }
        public List<Vector2f> EnemySpawns { get { return Map.EnemySpawns; } }

        public bool HasGameStarted { get; set; }
        public bool HasRoundStarted { get; set; }
        public bool IsGameOver { get; set; }
        public bool IsSpawning { get; set; }

        private bool _prepareRoundEnd;
        private float _enemyAlpha;

        private int _enemiesSpawned;
        private float _deltas;
        private float _timeBetweeenSpawn;
        private int _enemiesToSpawnPerTime;
        private int _totalEnemiesToSpawn;
        private float _goblinHP;
        private float _goblinHP_inc;
        private int _enemy_inc;
        private float _cleanupTime;
        private int _level;
        private static Music _bgmusic;
        
        private static List<Entity> _entities = new List<Entity>();
        private static List<Entity> _enemies = new List<Entity>();
        private static List<Entity> _dead = new List<Entity>();
        private static ObjectPool<Entity> _goblins;

        private static Queue<Entity> _toBeAdded = new Queue<Entity>();
        private static Queue<Entity> _toBeRemoved = new Queue<Entity>();

        public Game(Map map)
        {
            Level = 0;
            
            Map = map;
            
            Random = new Random(4);

            _timeBetweeenSpawn = 2f;
            _enemiesToSpawnPerTime = 4;
            _totalEnemiesToSpawn = 2;
            _goblinHP = 100f;
            _goblinHP_inc = 15f;
            _cleanupTime = 1.5f;
            _enemy_inc = 4;

            _prepareRoundEnd = false;
            _deltas = 0;
            _enemiesSpawned = 0;
            _enemyAlpha = 255;

            IsGameOver = false;
            IsSpawning = false;
            HasGameStarted = true;
            HasRoundStarted = false;

            Player = EntityFactory.CreatePlayer(Map.PlayerSpawn);
            Player.Notify(this, new SpawnedEventArgs());
            Player.GetComponent<IKillable>().Died += PlayerDied;

            foreach (ISkill s in Player.GetAllComponents<ISkill>())
            {
                s.Upgraded += (sender, e) => StartRound();
            }

            _goblins = new ObjectPool<Entity>(() => EntityFactory.CreateGoblin(new Vector2f(), Player));

            _bgmusic = new Music("Assets/Sounds/bg_music.wav");
        }

        private void PlayerDied(object sender, DieEventArgs e)
        {
            Player.DetachComponent(Player.GetComponent<IBehaviour>());
            Player.GetComponent<IBody>().Active = false;
            GameOver();
        }

        public void StartRound()
        {
            if (Level == 0)
            {
                _bgmusic.Play();
                _bgmusic.Loop = true;
                _bgmusic.Volume = 40;
            }
            _enemiesSpawned = 0;
            HasRoundStarted = true;
            IsSpawning = true;
            Level++;

            RoundStartedEventArgs started = new RoundStartedEventArgs(Level);
            Player.Notify(this, started);
            OnRoundStarted?.Invoke(this, started);
            Console.WriteLine("Round started");
        }

        public void EndRound()
        {
            _prepareRoundEnd = false;
            HasRoundStarted = false;
            _enemyAlpha = 255;

            _enemies = new List<Entity>();
            _dead = new List<Entity>();

            RoundEndedEventArgs ended = new RoundEndedEventArgs(Level);
            Player.Notify(this, new UpgradeSkillEventArgs());
            Player.Notify(this, ended);
            OnRoundEnded?.Invoke(this, ended);
            Console.WriteLine("Round Ended");
        }

        public void GameOver()
        {
            Sound sound = AudioLoader.GetSound("Assets/Sounds/gameoversound.wav");
            IsSpawning = false;
            foreach (Entity e in _enemies)
            {
                foreach (ISkill s in e.GetAllComponents<ISkill>())
                {
                    s.Active = false;
                }
            }
            GameLoop.Level = Level;
            sound.Play();
            _bgmusic.Stop();
            IsGameOver = true;
            OnGameOver?.Invoke(this, new GameOverEventArgs());
        }

        public static void DeInit()
        {
            World = new World(new Microsoft.Xna.Framework.Vector2());
            _entities = new List<Entity>();
            _enemies = new List<Entity>();
            _dead = new List<Entity>();

            _bgmusic.Stop();

            _toBeAdded = new Queue<Entity>();
            _toBeRemoved = new Queue<Entity>();
        }

        public void SpawnWave()
        {
            for(int i = 0; i < _enemiesToSpawnPerTime; i++)
            {
                if (_enemiesSpawned < _totalEnemiesToSpawn)
                {
                    _enemiesSpawned++;
                    int random = Random.Next(EnemySpawns.Count);

                    Entity goblin = _goblins.GetObject();
                    IKillable healthComponent = goblin.GetComponent<IKillable>();
                    healthComponent.MaxHP = _goblinHP + (Level-1) * _goblinHP_inc;
                    foreach(ISkill s in goblin.GetAllComponents<ISkill>())
                    {
                        s.Level = Level;
                    }
                    goblin.Position = Map.EnemySpawns[random];
                    goblin.Notify(this, new SpawnedEventArgs());
                    
                    goblin.GetComponent<IKillable>().Died += EnemyDied;
                    goblin.Spritesheet.Color = SFML.Graphics.Color.White;

                    _enemies.Add(goblin);
                }
                else
                {
                    IsSpawning = false;
                    break;
                }
            }
        }

        private void EnemyDied(object sender, DieEventArgs e)
        {
            _goblins.PutObject(e.Entity);
            e.Entity.GetComponent<IKillable>().Died -= EnemyDied;
            _enemies.Remove(e.Entity);
            _dead.Add(e.Entity);
            GameLoop.KilledEnemies++;
        }

        private void Cleanup()
        {
            _prepareRoundEnd = true;
            AudioLoader.GetSound("Assets/Sounds/wavesurvived.wav").Play();
            CleanupEventArgs clean = new CleanupEventArgs();
            Player.Notify(this, clean);
            OnCleanUp?.Invoke(this, clean);
        }

        public void Update(float delta)
        {
            if (IsSpawning)
            {
                _deltas += delta;
                if (_deltas >= _timeBetweeenSpawn)
                {
                    _deltas = 0;
                    SpawnWave();
                }
            }

            if (_prepareRoundEnd)
            {
                _deltas += delta;
                if(_deltas < _cleanupTime)
                {
                    float ds = _cleanupTime / delta;
                    float delta_alpha = 255 / ds;
                    _enemyAlpha -= delta_alpha;
                    if (_enemyAlpha < 0) _enemyAlpha = 0;
                    byte rounded = (byte)Math.Round(_enemyAlpha);
                    foreach (Entity e in _dead)
                    {
                        e.Spritesheet.Color = new SFML.Graphics.Color(150, 150, 150, rounded);
                    }
                }
                else
                {
                    _deltas = 0;
                    EndRound();
                }
            }

            if(_enemies.Count <= 0 && HasRoundStarted && !IsSpawning)
            {
                if (!_prepareRoundEnd)
                {
                    Cleanup();
                }
            }
            
            for (int i = 0; i < _toBeAdded.Count; i++)
            {
                _entities.Add(_toBeAdded.Dequeue());
            }

            for(int i = 0; i < _toBeRemoved.Count; i++)
            {
                _entities.Remove(_toBeRemoved.Dequeue());
            }

            World.Step(delta);
            foreach (Entity e in _entities)
            {
                e.Update(delta);
            }
        }

        public void Draw(Renderer renderer)
        {
            foreach (Entity e in _entities)
            {
                e.Draw(renderer);
            }
        }

        public static void RemoveEntity(Entity e)
        {
            _toBeRemoved.Enqueue(e);
        }

        public static void AddEntity(Entity e)
        {
            _toBeAdded.Enqueue(e);
        }

        public static Entity GetEntityOfFixture(Fixture fixture)
        {
            foreach (Entity e in _entities)
            {
                if (e.GetComponent<IBody>().Fixture.Equals(fixture))
                {
                    return e;
                }
            }
            return null;
        }
    }
}
