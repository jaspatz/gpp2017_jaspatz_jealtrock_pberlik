﻿using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest
{
    class Tile
    {
        private int _tilenumber;
        private int _tilesize;

        private int _map_row;
        private int _map_col;
        private int _txt_row;
        private int _txt_col;

        private Vertex[] _quad;

        public Tile(int tilenumber, int tilesize, int map_row, int map_col, int txt_row, int txt_col)
        {
            _tilenumber = tilenumber;
            _tilesize = tilesize;

            _map_row = map_row;
            _map_col = map_col;
            _txt_row = txt_row;
            _txt_col = txt_col;

            _quad = new Vertex[4];

            float offset = 0; //-0.0075f

            _quad[0] = new Vertex(new Vector2f(map_row * _tilesize, map_col * _tilesize), new Vector2f(txt_row * _tilesize, txt_col * _tilesize));
            _quad[1] = new Vertex(new Vector2f((map_row + 1) * _tilesize, map_col * _tilesize), new Vector2f((txt_row + 1) * _tilesize + offset, txt_col * _tilesize));
            _quad[2] = new Vertex(new Vector2f((map_row + 1) * _tilesize, (map_col + 1) * _tilesize), new Vector2f((txt_row + 1) * _tilesize + offset, (txt_col + 1) * _tilesize + offset));
            _quad[3] = new Vertex(new Vector2f(map_row * _tilesize, (map_col + 1) * _tilesize), new Vector2f(txt_row * _tilesize, (txt_col + 1) * _tilesize + offset));
        }

        public void AppendTo(VertexArray vertices)
        {
            foreach (Vertex v in _quad)
            {
                vertices.Append(v);
            }
        }
    }
}
