﻿using DarkForest.Loader;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Teilweise entnommen aus der SFML Doku (https://www.sfml-dev.org/tutorials/2.0/graphics-vertex-array.php)


namespace DarkForest.TileMap
{
    class Map : Transformable, Drawable
    {
        public Vector2f Boundarys { get { return _bounds; } }
        public int Tilesize { get { return _tilesize; } }
        public int Width { get { return _width; } }
        public int Height { get { return _height; } }

        public Vector2f PlayerSpawn { get; set; }
        public List<Vector2f> EnemySpawns { get; set; }

        private Texture _tileset;
        private Vector2f _bounds;
        private int _tilesize;
        private int _width;
        private int _height;

        private VertexArray _vertices;

        public Map(string filename, int tilesize, int width, int height)
        {
            _tileset = GraphicsLoader.GetTexture(filename);

            _tilesize = tilesize;

            _vertices = new VertexArray(PrimitiveType.Quads);

            _width = width;
            _height = height;
            
            //Generate the World boundarys

            _bounds = new Vector2f(_width * _tilesize, _height * _tilesize);
            Vector2 worldBounds = new Vector2(ConvertUnits.ToSimUnits(Boundarys.X), ConvertUnits.ToSimUnits(Boundarys.Y));

            var topEdge = BodyFactory.CreateEdge(Game.World, new Vector2(0, 0), new Vector2(worldBounds.X, 0));
            var rightEdge = BodyFactory.CreateEdge(Game.World, new Vector2(worldBounds.X, 0), new Vector2(worldBounds.X, worldBounds.Y));
            var bottomEdge = BodyFactory.CreateEdge(Game.World, new Vector2(worldBounds.X, worldBounds.Y), new Vector2(0, worldBounds.Y));
            var leftEdge = BodyFactory.CreateEdge(Game.World, new Vector2(0, worldBounds.Y), new Vector2(0, 0));
            topEdge.CollisionCategories = Category.Cat10;
            rightEdge.CollisionCategories = Category.Cat10;
            bottomEdge.CollisionCategories = Category.Cat10;
            leftEdge.CollisionCategories = Category.Cat10;

            //Miscellaneous

            PlayerSpawn = new Vector2f(_bounds.X / 2 - 550, _bounds.Y / 2 + 100);
            EnemySpawns = new List<Vector2f>();
        }

        public void AddLayer(int[] tiles)
        {
            for (int map_row = 0; map_row < _width; map_row++)
            {
                for (int map_col = 0; map_col < _height; map_col++)
                {
                    int tilenumber = tiles[map_row + map_col * _width] - 1;
                    
                    switch (tilenumber)
                    {
                        case -1:
                            break;
                        case 924:
                            Body body = BodyFactory.CreateBody(Game.World, new Vector2(ConvertUnits.ToSimUnits(map_row * _tilesize + _tilesize/2), ConvertUnits.ToSimUnits(map_col * _tilesize + _tilesize / 2)));
                            float size = ConvertUnits.ToSimUnits(_tilesize);
                            Fixture fixture = FixtureFactory.AttachRectangle(size, size, 0.5f, new Vector2(), body);
                            fixture.CollisionCategories = Category.Cat9;
                            break;
                        case 925:
                            PlayerSpawn = new Vector2f(map_row * _tilesize + _tilesize / 2, map_col * _tilesize + _tilesize / 2);
                            break;
                        case 926:
                            //NoCreatures
                            break;
                        case 927:
                            //Take Damage on hit
                            body = BodyFactory.CreateBody(Game.World, new Vector2(ConvertUnits.ToSimUnits(map_row * _tilesize + _tilesize / 2), ConvertUnits.ToSimUnits(map_col * _tilesize + _tilesize / 2)));
                            size = ConvertUnits.ToSimUnits(_tilesize);
                            fixture = FixtureFactory.AttachRectangle(size, size, 0.5f, new Vector2(), body);
                            fixture.CollisionCategories = Category.Cat8;
                            fixture.IsSensor = true;
                            break;
                        case 956:
                            EnemySpawns.Add(new Vector2f(map_row * _tilesize + _tilesize / 2, map_col * _tilesize + _tilesize / 2));
                            break;
                        default:
                            //texture position of tilenumber in tileset (by dividing through the number of tiles in a row of the tileset)
                            int txt_row = tilenumber % ((int)_tileset.Size.X / _tilesize);
                            int txt_col = tilenumber / ((int)_tileset.Size.X / _tilesize);
                            
                            Tile tile = new Tile(tilenumber, _tilesize, map_row, map_col, txt_row, txt_col);
                            tile.AppendTo(_vertices);
                            break;
                    }
                }
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            // apply the transform
            states.Transform *= Transform;

            // apply the tileset texture
            states.Texture = _tileset;

            // draw the vertex array
            target.Draw(_vertices, states);
        }
    }
}
