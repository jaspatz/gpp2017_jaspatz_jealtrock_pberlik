﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Menus
{
    class MenuColor
    {
        public static Color Selected = new Color(200, 70, 60);
        public static Color Unselected = new Color(190, 190, 190);
        public static Color Inactive = new Color(90, 90, 90);
        public static Color Text = new Color(190, 190, 190);
    }
}
