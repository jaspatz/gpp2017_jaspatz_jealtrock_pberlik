﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Menus
{
    interface IMenuItem
    {
        void Select();
        void Deselect();
        void HandleEvent(KeyEventArgs e);
        void Draw(Renderer renderer);
    }
}
