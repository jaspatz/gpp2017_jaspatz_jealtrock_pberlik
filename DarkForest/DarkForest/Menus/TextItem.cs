﻿using DarkForest.Loader;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Menus
{
    class TextItem : IMenuItem
    {
        public event EventHandler Activated;

        private Text _text;

        public TextItem(Vector2f position, string text, Font font, uint charsize)
        {
            _text = new Text(text, font, charsize);
            _text.FillColor = MenuColor.Unselected;
            _text.Position = position;
        }

        public void Select()
        {
            AudioLoader.GetSound("Assets/Sounds/select.wav").Play();
            _text.FillColor = MenuColor.Selected;
        }

        public void Deselect()
        {
            _text.FillColor = MenuColor.Unselected;
        }

        public void HandleEvent(KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Return:
                    Activated?.Invoke(this, EventArgs.Empty);
                    AudioLoader.GetSound("Assets/Sounds/activate.wav").Play();
                    break;
            }
        }

        public void Draw(Renderer renderer)
        {
            renderer.DrawOnScreen(_text);
        }
    }
}
