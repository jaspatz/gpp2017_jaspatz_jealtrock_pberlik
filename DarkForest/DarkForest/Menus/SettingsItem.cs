﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using SFML.System;
using DarkForest.Loader;

namespace DarkForest.Menus
{
    class SettingsItem<T> : IMenuItem
    {
        public T Value { get { return _options.ElementAt(_selectedIndex).Value; } }

        private Dictionary<string, T> _options;
        
        private Text _label;
        private Text _settings;

        private Text _rightArrow;
        private Text _leftArrow;

        private int _selectedIndex;

        public SettingsItem(Vector2f position, string text, Font font, uint charsize)
        {
            _label = new Text(text, font, charsize);
            _label.FillColor = MenuColor.Text;
            _label.Position = position;

            _settings = new Text("blank", font, charsize);
            _settings.Position = position + new Vector2f(550, 0);

            _rightArrow = new Text(">", font, charsize + 5);
            _rightArrow.Position = _settings.Position + new Vector2f(150, 0);

            _leftArrow = new Text("<", font, charsize + 5);
            _leftArrow.Position = _settings.Position - new Vector2f(25, 0);
            
            _options = new Dictionary<string, T>();
            _selectedIndex = 0;

        }

        public void AddOption(string key, T value)
        {
            _options.Add(key, value);
            SetCurrentOption();
        }

        private void SetCurrentOption()
        {
            //AudioLoader.GetSound("Assets/Sounds/select.wav").Play();
            _settings.DisplayedString = _options.ElementAt(_selectedIndex).Key;
            _rightArrow.Position = _settings.Position + new Vector2f(_settings.GetGlobalBounds().Width + 10, 0);
            _leftArrow.Position = _settings.Position - new Vector2f( 30, 0);
        }

        public void Select()
        {
            AudioLoader.GetSound("Assets/Sounds/select.wav").Play();
            _settings.FillColor = MenuColor.Selected;
        }

        public void Deselect()
        {
            _settings.FillColor = MenuColor.Unselected;
        }

        public void HandleEvent(KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Right:
                    if (_selectedIndex < _options.Count - 1)
                    {
                        _selectedIndex++;
                    }
                    else
                    {
                        _selectedIndex = 0;
                    }
                    SetCurrentOption();
                    break;
                case Keyboard.Key.Left:
                    if(_selectedIndex > 0)
                    {
                        _selectedIndex--;
                    }
                    else
                    {
                        _selectedIndex = _options.Count - 1;
                    }
                    SetCurrentOption();
                    break;
            }
        }

        public void Draw(Renderer renderer)
        {
            renderer.DrawOnScreen(_label);
            renderer.DrawOnScreen(_settings);

            renderer.DrawOnScreen(_leftArrow);
            renderer.DrawOnScreen(_rightArrow);
        }
    }
}
