﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest
{
    class Animation
    {
        public event EventHandler Stopped;

        public bool IsPlaying { get; set; }
        public bool IsLooping { get; set; }

        public float Duration { get { return _duration; } set { _duration = value; } }
        public float FrameDuration { get { return _duration / Count; } set { _duration = value * Count; } }

        public int CurrentFrame { get; private set; }
        public int Count { get; private set; }
        public IntRect CurrentRect { get { return _rects[_spritenumbers[CurrentFrame]]; } }

		private int[] _spritenumbers;
		private Sprite _spritesheet;
		private Vector2i _size;

		private IntRect[] _rects;
        private float _duration;

        private float _deltas;
        
        public Animation(Sprite spritesheet, Vector2i size, int[] spritenumbers, bool looping)
        {
            _spritesheet = spritesheet;
			_size = size;
            _spritenumbers = spritenumbers;
            IsLooping = looping;
            
			Texture texture = spritesheet.Texture;
			int width = (int)texture.Size.X / size.X;
			int height = (int)texture.Size.Y / size.Y;

            //fill _rects with every animationsprite
            _rects = new IntRect[width * height];
            for (int map_row = 0; map_row < width; map_row++)
			{
				for (int map_col = 0; map_col < height; map_col++)
				{
					_rects[map_row + map_col * width] = new IntRect(map_row * size.X, map_col * size.Y, size.X, size.Y);
				}
			}

			IsPlaying = false;
            Duration = 0.2f;
            Count = _spritenumbers.Length;
            CurrentFrame = 0;
        }

        public void Play()
        {
            CurrentFrame = 0;
            _spritesheet.TextureRect = CurrentRect;
            IsPlaying = true;
        }

        public void Pause()
        {
            IsPlaying = false;
        }

        public void Resume()
        {
            IsPlaying = true;
        }

        public void Stop()
        {
            Stopped?.Invoke(this, EventArgs.Empty);
            CurrentFrame = 0;
            IsPlaying = false;
        }

        public void Update(float delta)
        {
            _deltas += delta;
            if (_deltas >= FrameDuration)
            {
                _deltas = 0;
                CurrentFrame++;
                if (CurrentFrame >= Count)
                {
                    if (IsLooping)
                    {
                        CurrentFrame = 0;
                    }
                    else
                    {
                        Stop();
                        return;
                    }
                }
                _spritesheet.TextureRect = CurrentRect;
            }
        }
    }
}
