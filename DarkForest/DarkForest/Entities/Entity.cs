﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Entities
{
    class Entity
    {
        public Vector2f Position { get { return Spritesheet.Position; } set { Spritesheet.Position = value; Notify(this, new PositionedEventArgs(value)); } }
        public float Rotation { get { return Spritesheet.Rotation; } set { Notify(this, new RotatedEventArgs(value - Spritesheet.Rotation, value)); Spritesheet.Rotation = value; } }
        public Vector2f Scale { get { return Spritesheet.Scale; } set { Spritesheet.Scale = value; Notify(this, new ScaledEventArgs(value)); } }
        public Vector2f FacingDirection { get; set; }
        
        public String Description { get; set; }
        public Sprite Spritesheet { get; set; }
        public Vector2i SpriteSize { get; set; }
        
        public bool IsEnabled { get; set; }
        public bool DisableButRender { get; set; }

        public int ZIndex { get; set; }

        private List<IComponent> _components;
        private Queue<IComponent> _toBeDetached;

        public Entity(Vector2f position, Sprite spritesheet, Vector2i spritesize)
        {
            _components = new List<IComponent>();
            _toBeDetached = new Queue<IComponent>();

            SpriteSize = spritesize;
            Spritesheet = spritesheet;
            Spritesheet.Origin = new Vector2f(SpriteSize.X / 2, SpriteSize.Y / 2);
            Position = position;

            FacingDirection = new Vector2f(0, 1); //Facing Down

            ZIndex = 10;
            IsEnabled = true;
        }

        public void Update(float delta)
        {
            for (int i = 0; i < _toBeDetached.Count; i++)
            {
                _components.Remove(_toBeDetached.Dequeue());
            }
            if (IsEnabled && !DisableButRender)
            {
                Notify(this, new UpdatedEventArgs(delta));
            }
        }

        public void Draw(Renderer renderer)
        {
            if (IsEnabled)
            {
                renderer.Draw(Spritesheet, ZIndex);
            }
            else
            {
                if (DisableButRender)
                {
                    renderer.Draw(Spritesheet, ZIndex);
                }
            }
            Notify(this, new RenderedEventArgs(renderer));
        }

        public void Notify(object sender, EventArgs args)
        {
            foreach (IComponent c in _components)
            {
                c.HandleEvent(sender, args);
            }
        }

        public T GetComponent<T>() where T : IComponent
        {
            foreach (IComponent c in _components)
            {
                if (c is T)
                {
                    return (T)c;
                }
            }
            return default(T);
        }

        public List<T> GetAllComponents<T>() where T : IComponent
        {
            List<T> output = new List<T>();
            foreach (IComponent c in _components)
            {
                if (c is T)
                {
                    output.Add((T)c);
                }
            }
            return output;
        }

        public void AttachComponent(IComponent component)
        {
            _components.Add(component);
            component.Parent = this;
        }

        public void DetachComponent(IComponent component)
        {
            _toBeDetached.Enqueue(component);
        }

        public string GetOrientation()
        {
            if (FacingDirection.Equals(new Vector2f(-1, 0)))
                return "left";
            else if (FacingDirection.Equals(new Vector2f(0, -1)))
                return "up";
            else if (FacingDirection.Equals(new Vector2f(1, 0)))
                return "right";
            else if (FacingDirection.Equals(new Vector2f(0, 1)))
                return "down";
            return "(no direction)";
        }
    }
}
