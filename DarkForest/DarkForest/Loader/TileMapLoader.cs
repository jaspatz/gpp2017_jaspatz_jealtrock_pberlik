﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using DarkForest.TileMap;

namespace DarkForest.Loader
{
    static class TileMapLoader
    {
        
        private static void Skip(JsonTextReader reader)
        {
            if (reader.TokenType == JsonToken.StartArray)
            {
                while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                {
                    Skip(reader);
                }
            }
        }

        public static Map LoadFromJson(string mapname, string texturename)
        {

            int height = 0;
            int width = 0;

            int tileheight = 0;
            int tilewidth = 0;

            List<int[]> layers = new List<int[]>();

            string json = System.IO.File.ReadAllText(mapname);
            JsonTextReader reader = new JsonTextReader(new System.IO.StringReader(json));
            while (reader.Read())
            {
                if(reader.TokenType == JsonToken.StartArray)
                {
                    //Skip unneccasery code that isnt used...
                    Skip(reader);
                }

                if(reader.TokenType == JsonToken.PropertyName)
                {
                    if(reader.Value.ToString() == "height")
                    {
                        reader.Read();
                        height = int.Parse(reader.Value.ToString());
                    }
                    if (reader.Value.ToString() == "width")
                    {
                        reader.Read();
                        width = int.Parse(reader.Value.ToString());
                    }
                    if (reader.Value.ToString() == "tileheight")
                    {
                        reader.Read();
                        tileheight = int.Parse(reader.Value.ToString());
                    }
                    if (reader.Value.ToString() == "tilewidth")
                    {
                        reader.Read();
                        tilewidth = int.Parse(reader.Value.ToString());
                    }
                    if (reader.Value.ToString() == "layers")
                    {
                        while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                        {
                            if(reader.TokenType == JsonToken.StartObject)
                            {
                                List<int> data = new List<int>();
                                string name = "";

                                while (reader.Read() && reader.TokenType != JsonToken.EndObject)
                                {
                                    if(reader.TokenType == JsonToken.PropertyName)
                                    {
                                        if (reader.Value.ToString() == "data")
                                        {
                                            while (reader.Read() && reader.TokenType != JsonToken.EndArray)
                                            {
                                                if (reader.TokenType == JsonToken.Integer)
                                                {
                                                    data.Add(int.Parse(reader.Value.ToString()));
                                                }
                                            }
                                            continue;
                                        }

                                        if (reader.Value.ToString() == "name")
                                        {
                                            reader.Read();
                                            name = reader.Value.ToString();
                                        }
                                    }
                                }
                                layers.Add(data.ToArray());
                            }
                        }
                    }
                }
            }

            Map map = new Map(texturename, tileheight, width, height);

            foreach(int[] layer in layers)
            {
                map.AddLayer(layer);
            }

            return map;
        }
    }
}
