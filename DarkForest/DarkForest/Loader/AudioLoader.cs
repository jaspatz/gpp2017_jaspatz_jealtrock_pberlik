﻿using SFML.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Loader
{
    static class AudioLoader
    {
        private static Dictionary<string, Sound> _sounds = new Dictionary<string, Sound>();

        public static Sound GetSound(string filename)
        {
            foreach (KeyValuePair<string, Sound> entry in _sounds)
            {
                if (entry.Key.Equals(filename))
                {
                    return entry.Value;
                }
            }

            //there is no entry for the file so new entry gets created
            SoundBuffer buffer = new SoundBuffer(filename);
            Sound sound = new Sound(buffer);
            _sounds.Add(filename, sound);
            return sound;
        }
    }
}
