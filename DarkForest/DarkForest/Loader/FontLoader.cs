﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Loader
{
    static class FontLoader
    {
        private static Dictionary<string, Font> _fonts = new Dictionary<string, Font>();

        public static Font GetFont(string filename)
        {
            foreach (KeyValuePair<string, Font> entry in _fonts)
            {
                if (entry.Key.Equals(filename))
                {
                    return entry.Value;
                }
            }
            //there is no entry for the file so new entry gets created
            Font font = new Font(filename);
            _fonts.Add(filename, font);
            return font;
        }
    }
}
