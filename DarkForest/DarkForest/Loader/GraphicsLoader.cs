﻿using DarkForest.Components.Components;
using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Loader
{
    static class GraphicsLoader
    {
        private static Dictionary<string, Texture> _textures = new Dictionary<string, Texture>();
        
        public static Texture GetTexture(string filename)
        {
            foreach(KeyValuePair<string, Texture> entry in _textures)
            {
                if (entry.Key.Equals(filename))
                {
                    return entry.Value;
                }
            }
            //there is no entry for the file so new entry gets created
            Texture texture = new Texture(filename);
            _textures.Add(filename, texture);
            return texture;
        }
    }
}
