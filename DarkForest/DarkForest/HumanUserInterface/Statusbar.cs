﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.HumanUserInterface
{
    class Statusbar : Drawable
    {
        public float Value { get { return _value; } set
            {
                if (value < 0)
                    _value = 0;
                else if (value > _maxValue)
                    _value = _maxValue;
                else
                    _value = value;
                float factor = _value / _maxValue;
                int rounded = (int)Math.Ceiling(_value);
                if (rounded >= _maxValue)
                    rounded = (int)_maxValue;
                _text.DisplayedString = rounded + " / " + Math.Round(_maxValue);
                CenterText(_text);
                _bar.Size = new Vector2f(factor * _maxSize, _bar.Size.Y);
            }
        }

        private RectangleShape _bar;
        private RectangleShape _bgBar;
        private Text _text;
        private Font _font;

        private float _maxSize;
        private float _maxValue;
        private float _value;

        public Statusbar(Vector2f position, Vector2f size, Font font, Color color, float maxValue = 100f)
        {
            _font = font;
            _maxSize = size.X;

            _bar = new RectangleShape(size);
            _bar.Origin = new Vector2f(0, size.Y / 2);
            _bar.Position = position;
            _bar.FillColor = color;

            _bgBar = new RectangleShape(_bar);
            _bgBar.FillColor = new Color(color.R, color.G, color.B, 155);

            _text = new Text("100 / 100", _font);
            _text.CharacterSize = 15;
            _text.Origin = new Vector2f(_text.GetGlobalBounds().Width / 2, _text.GetGlobalBounds().Height / 2);
            _text.Position = new Vector2f(_bar.Position.X + _bar.Size.X / 2, _bar.Position.Y - 4);

            _maxValue = maxValue;
            _value = maxValue;
        }

        private void CenterText(Text text)
        {
            text.Origin = new Vector2f(text.GetGlobalBounds().Width / 2, text.GetGlobalBounds().Height / 2);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(_bgBar);
            target.Draw(_bar);
            target.Draw(_text);
        }
    }
}
