﻿using DarkForest.Components.Interfaces;
using DarkForest.Loader;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.HumanUserInterface
{
    class Skill : Drawable
    {
        private ISkill _skill;

        private float _cd_counter;

        private Text _lvl;
        private Text _key;
        private Text _mana;

        private Text _cd;
        private RectangleShape _cdbg;

        private Font _font_key;
        private Font _font_misc;
        private uint _charsize;

        private bool _onCooldown;

        public Skill(Vector2f position, ISkill skill)
        {
            _skill = skill;

            _font_misc = FontLoader.GetFont("Assets/Fonts/roboto.ttf");
            _font_key = FontLoader.GetFont("Assets/Fonts/darkforest.ttf");
            _charsize = 11;

            _lvl = new Text(skill.Level + "", _font_misc);
            _lvl.CharacterSize = _charsize;
            _lvl.Position = position;
            CenterText(_lvl);

            _key = new Text((char)skill.Key + "", _font_key);
            _key.CharacterSize = _charsize - 2;
            _key.Position = position + new Vector2f(27, 26);
            CenterText(_key);
            
            _cd = new Text("", _font_misc, _charsize + 10);
            _cd.Position = position + new Vector2f(26, -2);
            CenterText(_cd);

            _cdbg = new RectangleShape(new Vector2f(38, 38));
            _cdbg.Position = _cd.Position + new Vector2f(0, 5);
            _cdbg.Origin = new Vector2f(_cdbg.Size.X/2, _cdbg.Size.Y/2);
            _cdbg.FillColor = new Color(30, 30, 30, 200);

            _mana = new Text(Round(skill.Mana) + "", _font_misc);
            _mana.CharacterSize = _charsize;
            _mana.Position = position + new Vector2f(44, -19);
            _mana.FillColor = new Color(0, 150, 220);
            CenterText(_mana);

            skill.Upgraded += OnUpgrade;
            skill.Used += OnUse;

            _onCooldown = false;
        }

        private void OnUse(object sender, CustomEventArgs.AttackedEventArgs e)
        {
            _cd_counter = _skill.Cooldown;
            _onCooldown = true;
            _mana.FillColor = new Color(255, 0, 0);
        }

        private void OnUpgrade(object sender, CustomEventArgs.UpgradeSkillEventArgs e)
        {
            _lvl.DisplayedString = _skill.Level + "";
            CenterText(_lvl);
            
            _mana.DisplayedString = Round(_skill.Mana) + "";
            CenterText(_mana);
        }

        public void Update(float delta)
        {
            if (_onCooldown)
            {
                if (_cd_counter > 0)
                {
                    _cd_counter -= delta;
                    double rounded = (int) Math.Ceiling(_cd_counter);
                    if (rounded <= 1)
                    {
                        rounded = Math.Round(_cd_counter, 1);
                    }
                    _cd.DisplayedString = rounded + "";
                    CenterText(_cd);
                }
                else
                {
                    _onCooldown = false;
                    _mana.FillColor = new Color(0, 150, 220);
                }
            }
        }

        private float Round(float f)
        {
            float rounded;
            if (f >= 1)
            {
                rounded = (int)Math.Ceiling(f);
            }
            else
            {
                rounded = (float)Math.Round(f, 1);
            }
            return rounded;
        }

        private void CenterText(Text text)
        {
            text.Origin = new Vector2f(text.GetGlobalBounds().Width / 2, text.GetGlobalBounds().Height / 2);
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(_lvl);
            target.Draw(_key);

            if (_onCooldown)
            {
                target.Draw(_cdbg);
                target.Draw(_cd);
            }
            target.Draw(_mana);
        }
    }
}
