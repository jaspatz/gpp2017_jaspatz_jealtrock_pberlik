﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using SFML.Graphics;
using DarkForest.Loader;
using SFML.System;
using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Enums;

namespace DarkForest.HumanUserInterface
{
    class HUD : Drawable
    {
        private Entity _player;
        private Game _game;
        private int _level;

        private bool _showUpgradeOverlay;
        private bool _showGameOverOverlay;

        private Sprite _background;
        private Texture _backgroundTexture;

        private Statusbar _lifebar;
        private Statusbar _manabar;

        private List<Skill> _skills;

        private Font _font_text;
        private Font _font_misc;

        private Text _text_lvlup;

        public HUD(Game game, Vector2i size)
        {
            _game = game;
            _player = game.Player;

            _font_text = new Font("Assets/Fonts/darkforest.ttf");
            _font_misc = new Font("Assets/Fonts/roboto.ttf");

            _backgroundTexture = GraphicsLoader.GetTexture("Assets/hud.png");
            _background = new Sprite(_backgroundTexture);
            _background.Origin = new Vector2f(0, _background.Texture.Size.Y);
            _background.Position = new Vector2f(0, size.Y);

            _text_lvlup = new Text("Wave Survived!\nUse the Skill that you want to Upgrade.", _font_text);
            _text_lvlup.CharacterSize = 64;
            _text_lvlup.Origin = new Vector2f(_text_lvlup.GetGlobalBounds().Width / 2, _text_lvlup.GetGlobalBounds().Height / 2);
            _text_lvlup.Position = new Vector2f(size.X / 2, size.Y / 2 - 200);
            _text_lvlup.FillColor = Color.White;
            _text_lvlup.OutlineColor = Color.Black;
            _text_lvlup.OutlineThickness = 3;

            float health = _player.GetComponent<IKillable>().MaxHP;
            float mana = 200;

            _lifebar = new Statusbar(new Vector2f(525, 742), new Vector2f(235, 18), _font_misc, new Color(170, 30, 30), health);
            _manabar = new Statusbar(new Vector2f(525, 773), new Vector2f(235, 18), _font_misc, new Color(30, 30, 170), mana);

            _skills = new List<Skill>();
            List<ISkill> playerSkills = _player.GetAllComponents<ISkill>();
            for (int i = 0; i < playerSkills.Count; i++)
            {
                _skills.Add(new Skill(new Vector2f(82 + 77 * i, 762), playerSkills[i]));
            }

            _game.OnCleanUp += UpgradeOverlay;
            _game.OnGameOver += OnGameOver;
            _game.OnRoundStarted += (sender, args) => { _level++; _showUpgradeOverlay = false; _text_lvlup.DisplayedString = "Wave " + _level + " Survived!\nUse the Skill that you want to Upgrade."; };

            _player.GetComponent<IKillable>().GotHit += LoseHealth;
            _player.GetComponent<IKillable>().Refill += RefillHealth;
            _player.GetComponent<IResource>().UseResource += LoseMana;
            _player.GetComponent<IResource>().Refill += RefillMana;
        }

        private void OnGameOver(object sender, GameOverEventArgs e)
        {

        }

        private void UpgradeOverlay(object sender, EventArgs e)
        {
            _showUpgradeOverlay = true;
        }

        private void RefillHealth(object sender, RefillingEventArgs e)
        {
            _lifebar.Value += e.Amount;
        }

        private void LoseHealth(object sender, HitEventArgs e)
        {
            _lifebar.Value -= e.Damage;
        }

        private void RefillMana(object sender, RefillingEventArgs e)
        {
            _manabar.Value += e.Amount;
        }

        private void LoseMana(object sender, UseManaEventArgs e)
        {
            _manabar.Value += e.Mana;
        }

        public void Update(float delta)
        {
            foreach(Skill s in _skills)
            {
                s.Update(delta);
            }
        }

        public void Draw(RenderTarget target, RenderStates states)
        {
            target.Draw(_lifebar, states);
            target.Draw(_manabar, states);

            target.Draw(_background, states);

            foreach(Skill s in _skills)
            {
                target.Draw(s);
            }

            if(_showUpgradeOverlay)
                target.Draw(_text_lvlup);
        }
    }
}
