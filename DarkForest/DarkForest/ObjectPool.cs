﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest
{
    class ObjectPool<T>
    {
        private Func<T> _produce;
        private ConcurrentBag<T> _objects;

        private int numberOfProduced;
        
        public ObjectPool(Func<T> factoryMethod)
        {
            _objects = new ConcurrentBag<T>();
            _produce = factoryMethod;
            numberOfProduced = 0;
        }

        public T GetObject()
        {
            T item;
            if (_objects.TryTake(out item)) return item;
            numberOfProduced++;
            return _produce();
        }

        public void PutObject(T obj)
        {
            _objects.Add(obj);
        }
    }
}
