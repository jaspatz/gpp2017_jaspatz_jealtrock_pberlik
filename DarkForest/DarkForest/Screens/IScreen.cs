﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Screens
{
    interface IScreen
    {
        void Update(float delta);
        void HandleEvent(KeyEventArgs e);
        void Render(Renderer renderer);
    }
}
