﻿using DarkForest.Menus;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Screens
{
    class PauseMenuScreen : Screen
    {
        public PauseMenuScreen(GameLoop game) : base(game)
        {
            int xPos = 100;

            TextItem play = new TextItem(new Vector2f(xPos, 300), "Resume", Font, Charsize);
            play.Activated += (sender, args) => Game.PopScreen();
            AddMenuItem(play);

            TextItem options = new TextItem(new Vector2f(xPos, 400), "Exit to Mainmenu", Font, Charsize);
            options.Activated += (sender, args) => { Game.PopScreen(); Game.PopScreen(); Game.PushScreen(new MainMenuScreen(Game)); DarkForest.Game.DeInit(); };
            AddMenuItem(options);

            TextItem exit = new TextItem(new Vector2f(xPos, 500), "Exit Game", Font, Charsize);
            exit.Activated += (sender, e) => Game.Running = false;
            AddMenuItem(exit);
        }
    }
}
