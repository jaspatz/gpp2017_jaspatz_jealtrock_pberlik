﻿using DarkForest.Loader;
using DarkForest.Menus;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Screens
{
    class GameOverScreen : Screen
    {

        Text text1;
        Text text2;

        public GameOverScreen(GameLoop game) : base(game)
        {
            int xPos = 100;

            TextItem play = new TextItem(new Vector2f(xPos, 300), "Retry", Font, Charsize);
            play.Activated += (sender, args) => { Game.PopScreen(); Game.PopScreen(); DarkForest.Game.DeInit(); Game.PushScreen(new PlayScreen(Game)); };
            AddMenuItem(play);

            TextItem options = new TextItem(new Vector2f(xPos, 400), "Exit to Mainmenu", Font, Charsize);
            options.Activated += (sender, args) => { Game.PopScreen(); Game.PopScreen(); Game.PushScreen(new MainMenuScreen(Game)); DarkForest.Game.DeInit(); };
            AddMenuItem(options);

            TextItem exit = new TextItem(new Vector2f(xPos, 500), "Exit Game", Font, Charsize);
            exit.Activated += (sender, e) => Game.Running = false;
            AddMenuItem(exit);

            _darkforest.DisplayedString = "Game Over";

            text1 = new Text("You Died at Round " + GameLoop.Level + "!", FontLoader.GetFont("Assets/Fonts/darkforest.ttf"), 45);
            text1.Position = new Vector2f(Game.Width / 2, 190);
            text1.Origin = new Vector2f(text1.GetGlobalBounds().Width / 2, text1.GetGlobalBounds().Height / 2);

            text2 = new Text("Killed Enemies: " + GameLoop.KilledEnemies, FontLoader.GetFont("Assets/Fonts/darkforest.ttf"), 45);
            text2.Position = new Vector2f(Game.Width / 2, 230);
            text2.Origin = new Vector2f(text2.GetGlobalBounds().Width / 2, text2.GetGlobalBounds().Height / 2);

            GameLoop.KilledEnemies = 0;
        }

        public override void Render(Renderer renderer)
        {
            base.Render(renderer);
            renderer.DrawOnScreen(text1);
            renderer.DrawOnScreen(text2);
        }
    }
}
