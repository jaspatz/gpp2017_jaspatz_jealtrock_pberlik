﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using DarkForest.Menus;
using SFML.System;
using DarkForest.Loader;

namespace DarkForest.Screens
{
    abstract class Screen : IScreen
    {
        protected GameLoop Game { get; set; }

        protected uint Charsize { get; set; }
        protected Font Font { get; set; }

        protected Text _darkforest;
        private Sprite _background;
        protected List<IMenuItem> _items;
        private int _selectedIndex;

        public Screen(GameLoop game)
        {
            Game = game;

            Charsize = 60;
            Font = new Font("Assets/Fonts/darkforest.ttf");

            _background = new Sprite(GraphicsLoader.GetTexture("Assets/background.jpg"));

            _darkforest = new Text("Dark Forest", Font, 102);
            _darkforest.Style = Text.Styles.Bold;
            _darkforest.FillColor = new Color(13, 70, 12);
            _darkforest.OutlineColor = new Color(200, 70, 60);
            _darkforest.OutlineThickness = 1;
            _darkforest.Position = new Vector2f(Game.Width / 2, 90);
            _darkforest.Origin = new Vector2f(_darkforest.GetGlobalBounds().Width/2, _darkforest.GetGlobalBounds().Height/2);

            _items = new List<IMenuItem>();

            _selectedIndex = 0;
        }

        protected void AddMenuItem(IMenuItem item)
        {
            _items.Add(item);
            _items[_selectedIndex].Select();
        }

        public virtual void Update(float delta)
        {

        }

        public virtual void HandleEvent(KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Up:
                    if (_selectedIndex > 0)
                    {
                        _items[_selectedIndex].Deselect();
                        _selectedIndex--;
                        _items[_selectedIndex].Select();
                    }
                    break;
                case Keyboard.Key.Down:
                    if (_selectedIndex < _items.Count - 1)
                    {
                        _items[_selectedIndex].Deselect();
                        _selectedIndex++;
                        _items[_selectedIndex].Select();
                    }
                    break;
                default:
                    _items[_selectedIndex].HandleEvent(e);
                    break;
            }
        }

        public virtual void Render(Renderer renderer)
        {
            renderer.DrawOnScreen(_background);
            renderer.DrawOnScreen(_darkforest);
            foreach (IMenuItem item in _items)
                item.Draw(renderer);
        }
    }
}
