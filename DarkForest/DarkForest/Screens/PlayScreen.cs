﻿using DarkForest.HumanUserInterface;
using DarkForest.Loader;
using DarkForest.Menus;
using FarseerPhysics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Audio;

namespace DarkForest.Screens
{
    class PlayScreen : Screen
    {
        private Game _darkforest;
        private Camera _camera;
        private HUD _hud;

        private RectangleShape _overlay;
        private float _alpha;

        private float _gameovertime;
        private bool _gameOver;
        private bool _started;
        private float _deltas;

        public PlayScreen(GameLoop game) : base(game)
        {
            AudioLoader.GetSound("Assets/Sounds/gameoversound.wav");
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);

            string mapname;
            if (Options.Mapname == null)
            {
                mapname = "winterwonder";   //winterwonder
            }
            else
            {
                mapname = Options.Mapname;
            }
            _darkforest = new Game(TileMapLoader.LoadFromJson("Maps/" + mapname + ".json", "Assets/terrain.png"));
            _darkforest.OnGameOver += (sender, args) =>{ _gameOver = true; Console.WriteLine("game overs"); };
            _hud = new HUD(_darkforest, new Vector2i(game.Width, game.Height));
            _camera = new Camera(_darkforest.Player, _darkforest.Map.Boundarys, game.Renderer);
            _camera.SetDeadZone(240, 100, new Vector2f(0, -65));
            Game.Renderer.CameraView = _camera.View;


            _overlay = new RectangleShape(new Vector2f(Game.Width, Game.Height));
            _overlay.FillColor = new Color(0, 0, 0, 0);
            _alpha = 0;

            _gameovertime = 5f;
            _started = false;
            _gameOver = false;
        }

        private void GameOver()
        {
            Game.PushScreen(new GameOverScreen(Game));
        }

        public override void Update(float delta)
        {
            if (!_started)
            {
                _deltas += delta;
                if(_deltas > 2.5f)
                {
                    _darkforest.StartRound();
                    _started = true;
                    _deltas = 0;
                }
            }
            if (_gameOver)
            {
                _deltas += delta;
                if(_deltas > _gameovertime)
                {
                    GameOver();
                    _deltas = 0;
                }
                else
                {
                    float ds = _gameovertime / delta;
                    float delta_alpha = 255 / ds;
                    _alpha += delta_alpha;
                    if (_alpha > 255) _alpha = 255;
                    byte rounded = (byte)Math.Round(_alpha);
                    _overlay.FillColor = new SFML.Graphics.Color(0, 0, 0, rounded);
                }
            }
            _darkforest.Update(delta);
            _hud.Update(delta);
            _camera.Update();
        }

        public override void Render(Renderer renderer)
        {
            //Draw the Map
            renderer.Draw(_darkforest.Map, (int)Enums.Zindex.Background);

            //Draw world (Dynamic objects)
            _darkforest.Draw(renderer);

            //Draw HUD
            renderer.DrawOnScreen(_hud);

            renderer.DrawOnScreen(_overlay);
        }

        public override void HandleEvent(KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Escape:
                    Game.PushScreen(new PauseMenuScreen(Game));
                    break;
            }
        }
    }
}
