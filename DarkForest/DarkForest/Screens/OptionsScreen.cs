﻿using DarkForest.Menus;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Screens
{
    class OptionsScreen : Screen
    {
        SettingsItem<string> levelselect;
        SettingsItem<bool> fullscreen;

        public OptionsScreen(GameLoop game) : base(game)
        {
            fullscreen = new SettingsItem<bool>(new Vector2f(100, 260), "Fullscreen", Font, Charsize);
            fullscreen.AddOption("Yes", true);
            fullscreen.AddOption("No", false);
            AddMenuItem(fullscreen);

            levelselect = new SettingsItem<string>(new Vector2f(100, 340), "Select the Level", Font, Charsize);
            levelselect.AddOption("Winter Wonderworld", "winterwonder");
            levelselect.AddOption("Savana", "savana");
            levelselect.AddOption("Savage", "savage");
            levelselect.AddOption("Funky Dirt", "funkyfun");
            levelselect.AddOption("Lava Maze", "lavamaze");
            AddMenuItem(levelselect);

            TextItem exit = new TextItem(new Vector2f(200, 450), "Save & Back", Font, Charsize);
            exit.Activated += ExitScreen;
            AddMenuItem(exit);
        }

        private void ExitScreen(object sender, EventArgs e)
        {
            Options.Mapname = levelselect.Value;
            Options.Fullscreen = fullscreen.Value;
            Game.PopScreen();
            Game.Renderer.ChangeFullscreen(fullscreen.Value);
        }
    }
}
