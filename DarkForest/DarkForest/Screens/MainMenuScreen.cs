﻿using DarkForest.Loader;
using DarkForest.Menus;
using SFML.Audio;
using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Screens
{
    class MainMenuScreen : Screen
    {
        Music _music;

        Sprite _howtoplay;

        public MainMenuScreen(GameLoop game) : base(game)
        {
            int xPos = 100;

            TextItem play = new TextItem(new Vector2f(xPos, 300), "Play Game", Font, Charsize);
            play.Activated += StartGame;
            AddMenuItem(play);

            TextItem options = new TextItem(new Vector2f(xPos, 400), "Options", Font, Charsize);
            options.Activated += OpenOptions;
            AddMenuItem(options);

            TextItem exit = new TextItem(new Vector2f(xPos, 500), "Exit Game", Font, Charsize);
            exit.Activated += (sender, e) => Game.Running = false;
            AddMenuItem(exit);

            _howtoplay = new Sprite(GraphicsLoader.GetTexture("Assets/howtoplay.png"));
            //_howtoplay.Scale = new Vector2f(0.9f, 0.9f);
            _howtoplay.Position = new Vector2f(game.Width/2 - 150, 180);

            _music = new Music("Assets/Sounds/mainmenu.wav");
            _music.Play();
        }

        private void OpenOptions(object sender, EventArgs e)
        {
            Game.PushScreen(new OptionsScreen(Game));
        }

        private void OpenHowToPlay(object sender, EventArgs e)
        {
            Game.PushScreen(new OptionsScreen(Game));
        }

        private void StartGame(object sender, EventArgs e)
        {
            _music.Stop();
            Game.PopScreen();
            Game.PushScreen(new PlayScreen(Game));
        }

        public override void Render(Renderer renderer)
        {
            base.Render(renderer);
            renderer.DrawOnScreen(_howtoplay);
        }
    }
}
