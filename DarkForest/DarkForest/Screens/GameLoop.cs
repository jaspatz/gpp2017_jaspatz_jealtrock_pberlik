﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Screens
{
    class GameLoop
    {
        public const int FPS = 60;
        public const float TICK = 1 / (float)FPS;

        public static int Level;
        public static int KilledEnemies;

        public int Width { get; set; }
        public int Height { get; set; }
        public bool Running { get; set; }
        public Renderer Renderer { get; set; }
        
        private bool _fullscreen;
        private RenderWindow _window;

        private List<IScreen> _screens;

        public GameLoop()
        {
            Width = 1280;
            Height = 800;
            _fullscreen = false;
            Renderer = new Renderer(Width, Height, _fullscreen, "DarkForest");
            _window = Renderer.Window;

            _screens = new List<IScreen>();

            _screens.Add(new MainMenuScreen(this));

            Run();
        }

        public void Update(float delta)
        {
            if (_screens.Count > 0)
                _screens.Last().Update(delta);
        }

        public void Render()
        {
            _window.Clear();

            if (_screens.Count > 0)
                _screens.Last().Render(Renderer);
            
            Renderer.Render();

            _window.Display();
        }

        public void PushScreen(IScreen screen)
        {
            _screens.Add(screen);
        }

        public void PopScreen()
        {
            _screens.Remove(_screens.Last());
        }

        private void KeyPressed(object sender, KeyEventArgs e)
        {
            if (_screens.Count > 0)
                _screens.Last().HandleEvent(e);
        }

        public void Run()
        {
            Running = true;
            Renderer.Closed += (sender, e) => Running = false;
            Renderer.KeyPressed += KeyPressed;
            float accumulator = 0;
            float delta = 0;
            Stopwatch clock = new Stopwatch();
            clock.Start();

            while (Running)
            {
                delta = clock.ElapsedTicks / (float)Stopwatch.Frequency;
                accumulator += delta;
                clock.Restart();

                if (accumulator > 0.2f)
                    accumulator = 0.2f;

                while (accumulator >= TICK)
                {
                    Renderer.DispatchEvents();
                    Update(TICK);
                    accumulator -= TICK;
                }
                Render();
            }
        }
    }
}
