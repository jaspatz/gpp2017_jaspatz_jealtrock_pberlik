﻿using DarkForest.Components.Components.Animators;
using DarkForest.Components.Components.Behaviours;
using DarkForest.Components.Components.Physicals;
using DarkForest.Components.Interfaces;
using DarkForest.Entities;
using DarkForest.Loader;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Factories
{
    static class ProjectileFactory
    {
        private static int fireball_counter = 0;
        private static int thunderstrike_counter = 0;

        public static Entity CreateFireball(Entity attacker, bool horizontal)
        {
            Sprite sprite;
            Vector2i size;
            IComponent animator;
            if (horizontal)
            {
                sprite = new Sprite(GraphicsLoader.GetTexture("Assets/Attacks/fireballrl.png"));
                size = new Vector2i(29, 19);
                animator = new FireballHorizontalAnimator(sprite, size);
            }
            else
            {
                sprite = new Sprite(GraphicsLoader.GetTexture("Assets/Attacks/fireballver.png"));
                size = new Vector2i(19, 29);
                animator = new FireballVerticalAnimator(sprite, size);
            }
            Entity fireball = new Entity(new Vector2f(0, 0), sprite, size);
            fireball.Description = "Fireball " + fireball_counter;
            fireball.ZIndex = (int)Enums.Zindex.Projectile;

            fireball.AttachComponent(new ProjectileBody(attacker, fireball.SpriteSize, fireball.Position, Game.World));
            fireball.AttachComponent(new StraightMover(5f));
            fireball.AttachComponent(animator);

            fireball_counter++;
            return fireball;
        }

        public static Entity CreateThunderStrike(Entity attacker, bool horizontal)
        {
            Sprite sprite;
            Vector2i size;
            IComponent animator;
            if (horizontal)
            {
                sprite = new Sprite(GraphicsLoader.GetTexture("Assets/Attacks/blitzstrahlrl.png"));
                size = new Vector2i(255, 90);
                animator = new ThunderHorizontalAnimator(sprite, size);
            }
            else
            {
                sprite = new Sprite(GraphicsLoader.GetTexture("Assets/Attacks/blitzstrahlver.png"));
                size = new Vector2i(90, 254);
                animator = new ThunderVerticalAnimator(sprite, size);
            }
            Entity thunderstrike = new Entity(new Vector2f(0, 0), sprite, size);
            thunderstrike.Description = "ThunderStrike " + thunderstrike_counter;
            thunderstrike.ZIndex = (int)Enums.Zindex.Projectile;

            thunderstrike.AttachComponent(new ProjectileBody(attacker, thunderstrike.SpriteSize, thunderstrike.Position, Game.World));
            thunderstrike.AttachComponent(animator);

            thunderstrike_counter++;
            return thunderstrike;
        }

        public static Entity CreateFreezeStrike(Entity attacker)
        {
            Entity iceblock = new Entity(new Vector2f(0, 0), new Sprite(GraphicsLoader.GetTexture("Assets/Attacks/ice-dust.png")), new Vector2i(359, 403));
            iceblock.Description = "FreezeStrike";
            iceblock.ZIndex = (int)Enums.Zindex.Projectile;

            iceblock.AttachComponent(new ProjectileBody(attacker, iceblock.SpriteSize - new Vector2i(40, 40), iceblock.Position, Game.World));
            iceblock.AttachComponent(new FreezeAnimator(iceblock.Spritesheet, iceblock.SpriteSize));
            
            return iceblock;
        }

        public static Entity CreateShield()
        {
            Entity shield = new Entity(new Vector2f(0, 0), new Sprite(GraphicsLoader.GetTexture("Assets/Attacks/earthshield.png")), new Vector2i(111, 109));
            shield.Description = "Earthshield";
            shield.ZIndex = (int)Enums.Zindex.Projectile;

            //shield.AttachComponent(new HitRegistrationBody(shield.SpriteSize, shield.Position, Game.World));
            shield.AttachComponent(new ShieldAnimator(shield.Spritesheet, shield.SpriteSize));
            
            return shield;
        }
    }
}
