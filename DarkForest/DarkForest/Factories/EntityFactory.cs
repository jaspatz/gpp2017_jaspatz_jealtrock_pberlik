﻿using DarkForest.Components.Components.Animators;
using DarkForest.Components.Components.Behaviours;
using DarkForest.Components.Components.Dynamics;
using DarkForest.Components.Components.Skills;
using DarkForest.Components.Interfaces;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using DarkForest.Components.Components.Physicals;
using DarkForest.Loader;
using DarkForest.Components.Components.Audio;

namespace DarkForest.Factories
{
    static class EntityFactory
    {
        private static int goblin_counter = 0;

        public static Entity CreatePlayer(Vector2f position)
        {
            Entity player = new Entity(position, new Sprite(GraphicsLoader.GetTexture("Assets/hero.png")), new Vector2i(36, 65));
            player.Description = "Player";
            player.ZIndex = (int) Enums.Zindex.Player;

            player.AttachComponent(new CharacterBody(player.SpriteSize, position, Game.World));
            player.AttachComponent(new PlayerControlled(1.5f));
            player.AttachComponent(new PlayerAnimator(player.Spritesheet, player.SpriteSize));
            player.AttachComponent(new FireballSkill());
            player.AttachComponent(new ThunderSkill());
            player.AttachComponent(new FreezeSkill());
            player.AttachComponent(new ShieldSkill());
            player.AttachComponent(new PlayerAudio());
            player.AttachComponent(new Killable(100.0f));
            player.AttachComponent(new ManaResource(200.0f));

            Game.AddEntity(player);
            return player;
        }

        public static Entity CreateGoblin(Vector2f position, Entity followed)
        {
            Entity goblin = new Entity(position, new Sprite(GraphicsLoader.GetTexture("Assets/Creatures/goblin.png")), new Vector2i(39, 37));
            goblin.Description = "Goblin " + goblin_counter;
            goblin.ZIndex = (int)Enums.Zindex.Entity;
            goblin.Scale = new Vector2f(0, 0);

            goblin.AttachComponent(new CharacterBody(goblin.SpriteSize, position, Game.World));
            goblin.AttachComponent(new CreatureSpawn());
            goblin.AttachComponent(new FollowPlayer(followed, 0.45f, 0.8f));
            goblin.AttachComponent(new GoblinAnimator(goblin.Spritesheet, goblin.SpriteSize));
            goblin.AttachComponent(new MeleeAttack(followed));
            goblin.AttachComponent(new Killable(100.0f));

            Game.AddEntity(goblin);
            goblin_counter++;
            return goblin;
        }
    }
}
