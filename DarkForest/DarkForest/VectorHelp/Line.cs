﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;

namespace DarkForest.VectorHelp
{
	class Line
	{
        private Vector2f start;
        private Vector2f end;

		public Line(Vector2f start, Vector2f end)
		{
            this.start = start;
            this.end = end;
		}

        public Vector2f ProjectPointOnLine(Vector2f point)
        {
            Vector2f normal = Vector.Normal(end - start);
            Line pointToLine = new Line(point, point + normal);

            float t = pointToLine.IntersectionWithLine(this) * 1.0001f; //aus rechnerischer ungenauigkeit 0.01% (1.0001f)draufgerechnet werden

            Vector2f xPoint = pointToLine.Start + t * (pointToLine.End - pointToLine.Start);
            return xPoint;
        }
		public float DistanceToPoint(Vector2f point)
		{
            Vector2f normal = Vector.Normal(end - start);
            Line pointToLine = new Line(point, point + normal);

            float t = pointToLine.IntersectionWithLine(this);

            Vector2f xPoint = pointToLine.Start + t * (pointToLine.End - pointToLine.Start);

            float distance = new Line(point, xPoint).Length;
            return distance;
		}
        public Vector2f CrossPointWithLine(Line line)
        {
            float t = IntersectionWithLine(line);
            return start + t * (end - start);
        }
		public float IntersectionWithLine(Line line)
		{
            Vector2f direction = end - start;

            Vector2f linePosition = line.Start;
            Vector2f lineDirection = line.End - line.Start;

			float zaehler = lineDirection.X * linePosition.Y - lineDirection.X * start.Y + start.X * lineDirection.Y - linePosition.X * lineDirection.Y;
			float nenner = direction.Y * lineDirection.X - direction.X * lineDirection.Y;
            float t = zaehler / nenner;

            return t;
		}
        public bool IsPointOnLineSegment(Vector2f point)
        {
            float minX = Min(start.X, end.X);
            float maxX = Max(start.X, end.X);
            float minY = Min(start.Y, end.Y);
            float maxY = Max(start.Y, end.Y);
            if (point.X >= minX && point.X <= maxX || point.Y >= minY && point.Y <= maxY)
            {
                return true;
            }
            return false;
        }
        public float Min(float number1, float number2)
        {
            if(number1 < number2)
            {
                return number1;
            }
            else
            {
                return number2;
            }
        }
        public float Max(float number1, float number2)
        {
            if (number1 > number2)
            {
                return number1;
            }
            else
            {
                return number2;
            }
        }
		public Vector2f Start
		{
			get { return start; }
			set { start = value; }
		}
		public Vector2f End
		{
			get { return end; }
			set { end = value; }
		}
        public float Length
        {
            get { return Vector.Length(end - start); }
        }
        public Vector2f Normal
        {
            get { return Vector.Normal(end - start); }
        }
	}
}
