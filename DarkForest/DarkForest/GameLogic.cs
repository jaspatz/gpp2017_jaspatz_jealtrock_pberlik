﻿using DarkForest.Components.Components;
using DarkForest.Components.Interfaces;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using DarkForest.Loader;
using DarkForest.HumanUserInterface;

namespace DarkForest
{
	class GameLogic
	{
		public const int FPS = 60;
		public const float TICK = 1 / (float)FPS;

        private int width;
        private int height;
        private bool fullscreen;

        private Renderer _renderer;
        private Camera _camera;

        //private GameObjects _gameobjects;
        private Game _game;
        private HUD _hud;

		public GameLogic()
		{

            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            //All not used -----------------------------------------------
            AudioLoader.GetSound("Assets/Sounds/gameoversound.wav");

            width = 1280; //
            height = 800; //
            fullscreen = false;
            _renderer = new Renderer(width, height, fullscreen, "DarkForest");
            
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64f);//---------------------------------------------------------------------------------------------

            string mapname = "winterwonder";   //winterwonder
            _game = new Game(TileMapLoader.LoadFromJson("Maps/" + mapname + ".json", "Assets/terrain.png"));
            _hud = new HUD(_game, new Vector2i(width, height));
            _camera = new Camera(_game.Player, _game.Map.Boundarys, _renderer);
            _camera.SetDeadZone(240, 100, new Vector2f(0, -65));
            
            _renderer.CameraView = _camera.View;

            Run();
		}

        public void Update(float delta)
        {
            if(!_game.HasRoundStarted && _game.Level == 0) _game.StartRound();
            _game.Update(delta);
            //_hud.Update(delta);
            _camera.Update();
        }

        public void Render()
        {
            //Draw the Map
            _renderer.Draw(_game.Map, (int) Enums.Zindex.Background);

            //Draw world (Dynamic objects)
            _game.Draw(_renderer);

            //Draw HUD
            _renderer.DrawOnScreen(_hud);

            _renderer.Render();
        }
        
        public void Run()
        {
            bool running = true;
            _renderer.Closed += (sender, e) => running = false;
            float accumulator = 0;
            float delta = 0;
            Stopwatch clock = new Stopwatch();
            clock.Start();

            while (running)
            {
                delta = clock.ElapsedTicks / (float)Stopwatch.Frequency;
                accumulator += delta;
                clock.Restart();

                if (accumulator > 0.2f)
                    accumulator = 0.2f;

                while (accumulator >= TICK)
                {
                    _renderer.DispatchEvents();
                    Update(TICK);
                    accumulator -= TICK;
                }
                Render();
            }
        }
    }
}
