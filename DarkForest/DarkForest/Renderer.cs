﻿using DarkForest.Components.Interfaces;
using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest
{
	class Renderer
    {
        public event EventHandler Closed;
        public event EventHandler<KeyEventArgs> KeyPressed;

        public View CameraView { get; set; }
        public View DefaultView { get { return _window.DefaultView; } }
        public RenderWindow Window { get { return _window; } }

		private RenderWindow _window;
        private Styles _style;
        private ContextSettings _context;
        private VideoMode _videomode;
        private bool _fullscreen;
        private string _title;

		private Dictionary<Drawable, int> _world;
		private List<Drawable> _screen;

		public Renderer(int width, int height, bool fullscreen, string title)
		{
            _fullscreen = fullscreen;
			_videomode = new VideoMode((uint)width, (uint)height);
			_context = new ContextSettings(24, 8, 8);
            _style = fullscreen ? Styles.Fullscreen : Styles.Default;
            _title = title;

            _world = new Dictionary<Drawable, int>();
			_screen = new List<Drawable>();
            
			_window = new RenderWindow(_videomode, title, _style, _context);
			_window.SetVerticalSyncEnabled(true);
            _window.Closed += WindowClosed;
            _window.KeyPressed += WindowKeyPressed;

            _window.SetMouseCursorVisible(!fullscreen);
        }

        public void ChangeFullscreen(bool fullscreen)
        {
            if (_fullscreen != fullscreen)
            {
                _fullscreen = fullscreen;
                _window.Close();
                _style = fullscreen ? Styles.Fullscreen : Styles.Default;
                _window = new RenderWindow(_videomode, _title, _style, _context);
                _window.SetMouseCursorVisible(!fullscreen);
                _window.Closed += WindowClosed;
                _window.KeyPressed += WindowKeyPressed;
                _window.SetVerticalSyncEnabled(true);
            }
        }

        private void WindowKeyPressed(object sender, KeyEventArgs e)
        {
            KeyPressed?.Invoke(this, e);
        }

        public void WindowClosed(object sender, EventArgs e)
        {
            _window.Close();
            Closed?.Invoke(this, e);
        }

		public void DispatchEvents()
		{
			_window.DispatchEvents();
		}

		public void Draw(Drawable drawable, int zIndex = 10)
		{
			_world.Add(drawable, zIndex);
		}

		public void DrawOnScreen(Drawable drawable)
		{
			_screen.Add(drawable);
		}

		public void Render()
        {
            _window.Clear();
            if (CameraView != null)
            {
                //var myList = _world.ToList();
                //myList.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));
                var sortedDict = from entry in _world orderby entry.Value descending select entry;

                _window.SetView(CameraView);
                foreach (KeyValuePair<Drawable, int> pair in sortedDict)
                {
                    _window.Draw(pair.Key);
                }
                _world = new Dictionary<Drawable, int>();
            }
			_window.SetView(DefaultView);
			foreach (Drawable d in _screen)
			{
				_window.Draw(d);
			}
            _screen = new List<Drawable>();

			_window.Display();
        }
	}
}
