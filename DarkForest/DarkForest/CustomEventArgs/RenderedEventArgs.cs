﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class RenderedEventArgs : EventArgs
    {
        public Renderer Renderer { get; private set; }

        public RenderedEventArgs(Renderer renderer)
        {
            Renderer = renderer;
        }
    }
}
