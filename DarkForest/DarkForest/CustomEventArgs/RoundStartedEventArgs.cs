﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class RoundStartedEventArgs : EventArgs
    {
        public int LevelOnStart { get; private set; }

        public RoundStartedEventArgs(int level)
        {
            LevelOnStart = level;
        }
    }
}
