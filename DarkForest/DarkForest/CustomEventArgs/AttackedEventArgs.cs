﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class AttackedEventArgs : EventArgs
    {
        public ISkill Skill { get; private set; }
        
        public AttackedEventArgs(ISkill skill)
        {
            Skill = skill;
        }
    }
}
