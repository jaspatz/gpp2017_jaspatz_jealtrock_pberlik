﻿using DarkForest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class DieEventArgs : EventArgs
    {
        public Entity Entity { get; set; }

        public DieEventArgs(Entity e)
        {
            Entity = e;
        }
    }
}
