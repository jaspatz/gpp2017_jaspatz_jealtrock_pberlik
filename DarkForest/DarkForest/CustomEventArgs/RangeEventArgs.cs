﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class RangeEventArgs : EventArgs
    {
        public enum EventType
        {
            Entered,
            Left,
        }
        public float Distance { get; private set; }
        public EventType Type { get; private set; }

        public RangeEventArgs(EventType type, float distance)
        {
            Type = type;
            Distance = distance;
        }
    }
}
