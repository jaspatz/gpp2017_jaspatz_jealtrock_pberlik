﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class RotatedEventArgs : EventArgs
    {
        public float DeltaRotation { get; private set; }
        public float Rotation { get; private set; }

        public RotatedEventArgs(float delta, float rotation)
        {
            DeltaRotation = delta;
            Rotation = rotation;
        }
    }
}
