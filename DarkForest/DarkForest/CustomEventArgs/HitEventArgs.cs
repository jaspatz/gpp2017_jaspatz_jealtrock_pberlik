﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;

namespace DarkForest.CustomEventArgs
{
    class HitEventArgs : EventArgs
    {
        public Entity Offender { get; private set; }
        public Entity Victim { get; private set; }
        public float Damage { get; private set; }
        //public ISkill Skill { get; private set; }

        public HitEventArgs(Entity offender, Entity victim, float damage)
        {
            Offender = offender;
            Victim = victim;
            Damage = damage;
            //Skill = skill;
        }
    }
}
