﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class RoundEndedEventArgs : EventArgs
    {
        public int LevelOnEnd { get; private set; }

        public RoundEndedEventArgs(int level)
        {
            LevelOnEnd = level;
        }
    }
}
