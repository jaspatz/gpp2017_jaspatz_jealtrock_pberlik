﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class MovedEventArgs : EventArgs
    {
        public Vector2f OldPosition { get; private set; }
        public Vector2f NewPosition { get; private set; }
        public float Velocity { get; private set; }
        public Vector2f Direction { get; private set; }

        public MovedEventArgs(Vector2f oldpos, Vector2f newpos, Vector2f direction, float velocity)
        {
            OldPosition = oldpos;
            NewPosition = newpos;
            Direction = direction;
            Velocity = velocity;
        }
    }
}
