﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class AttackInterruptedEventArgs : EventArgs
    {
        public ISkill Skill { get; private set; }

        public AttackInterruptedEventArgs(ISkill skill)
        {
            Skill = skill;
        }
    }
}
