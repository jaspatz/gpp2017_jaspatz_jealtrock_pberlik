﻿using FarseerPhysics.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;

namespace DarkForest.CustomEventArgs
{
    class CollidedEventArgs : EventArgs
    {
        public Entity Entity { get; private set; }
        public Fixture Fixture { get; private set; }

        public CollidedEventArgs(Entity entity, Fixture fixture)
        {
            Entity = entity;
            Fixture = fixture;
        }
    }
}
