﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class UseManaEventArgs : EventArgs
    {
        public float Mana { get; private set; }

        public UseManaEventArgs(float mana)
        {
            Mana = mana;
        }
    }
}
