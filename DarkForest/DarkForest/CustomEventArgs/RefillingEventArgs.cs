﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class RefillingEventArgs : EventArgs
    {
        public float Amount { get; set; }
        public RefillingEventArgs(float amount)
        {
            Amount = amount;
        }
    }
}
