﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class ScaledEventArgs : EventArgs
    {
        public Vector2f Scale { get; private set; }

        public ScaledEventArgs(Vector2f scale)
        {
            Scale = scale;
        }
    }
}
