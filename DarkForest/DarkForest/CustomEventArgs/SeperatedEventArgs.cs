﻿using DarkForest.Entities;
using FarseerPhysics.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class SeperatedEventArgs : EventArgs
    {
        public Entity Entity { get; private set; }
        public Fixture Fixture { get; private set; }

        public SeperatedEventArgs(Entity entity, Fixture fixture)
        {
            Entity = entity;
            Fixture = fixture;
        }
    }
}
