﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.CustomEventArgs
{
    class LavaEventArgs : EventArgs
    {
        public bool InLava { get; set; }
        public LavaEventArgs(bool inLava)
        {
            InLava = inLava;
        }
    }
}
