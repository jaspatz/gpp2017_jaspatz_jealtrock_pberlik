﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Entities;
using DarkForest.VectorHelp;
using FarseerPhysics;
using Microsoft.Xna.Framework;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Behaviours
{
    class FollowPlayer : IBehaviour
    {
        public Entity Parent { get; set; }
        public float Velocity { get; set; }
        public Vector2f Direction { get; set; }

        public bool Active { get; set; }

        private Entity _player;
        private float _distance;
        private bool _inRange;

        public FollowPlayer(Entity Player, float velocity, float distance)
        {
            _player = Player;
            _distance = distance;
            Velocity = velocity;

            Active = false;
        }
        
        public void Move()
        {
            Vector2f parentOldpos = Parent.Position;
            Vector2f playerOldpos = _player.Position;

            Direction = playerOldpos - parentOldpos;
            if (!Direction.Equals(new Vector2f(0, 0)))
            {
                if (Direction.X > 0)
                {
                    //Facing Right
                    Parent.FacingDirection = new Vector2f(1, 0);
                }
                else
                {
                    //facing left
                    Parent.FacingDirection = new Vector2f(-1, 0);
                }
                Vector2 direction = new Vector2(ConvertUnits.ToSimUnits(Direction.X), ConvertUnits.ToSimUnits(Direction.Y));
                direction.Normalize();
                Parent.GetComponent<IBody>().Body.LinearVelocity += Velocity * direction;
                Parent.GetComponent<IBody>().Body.ApplyForce(direction);
            }
            Vector2f newpos = Parent.Position;

            float distance = ConvertUnits.ToSimUnits(Vector.Length(_player.Position - Parent.Position));
            if (distance <= _distance)
            {
                _inRange = true;
                PostEvent(new RangeEventArgs(RangeEventArgs.EventType.Entered, distance));
            }

            PostEvent(new MovedEventArgs(parentOldpos, newpos, Direction, Velocity));
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            if (!Active) return;

            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                if (!_inRange)
                {
                    Move();
                }
                else
                {
                    float distance = ConvertUnits.ToSimUnits(Vector.Length(_player.Position - Parent.Position));
                    if (distance > _distance)
                    {
                        _inRange = false;
                        PostEvent(new RangeEventArgs(RangeEventArgs.EventType.Left, distance));
                    }
                }
            }

            DieEventArgs died = args as DieEventArgs;
            if(died != null)
            {
                Active = false;
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
