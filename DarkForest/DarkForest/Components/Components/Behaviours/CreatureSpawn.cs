﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using DarkForest.Entities;
using DarkForest.CustomEventArgs;
using FarseerPhysics;
using DarkForest.Components.Components.Physicals;
using Microsoft.Xna.Framework;

namespace DarkForest.Components.Components.Behaviours
{
    class CreatureSpawn : IBehaviour
    {
        public float Velocity { get; set; }
        public Vector2f Direction { get; set; }
        public Entity Parent { get; set; }

        public bool Active { get; set; }

        private bool _jumpingUp;

        private float _spawntime;
        private float _deltascale;
        private float _maxScale;

        private float _delta;

        public CreatureSpawn()
        {
            _jumpingUp = true;

            _spawntime = 0.8f;
            _maxScale = 1.8f;

            _deltascale = (_maxScale + 0.8f) / (_spawntime / GameLogic.TICK);

            Velocity = 0.8f;
            
            float x = (float) Game.Random.NextDouble();
            if (Game.Random.Next(2) == 0)
            {
                x = -x;
            }
            float y = (float) Game.Random.NextDouble();
            if (Game.Random.Next(2) == 0)
            {
                y = -y;
            }
            Direction = new Vector2f(x, y);
            Direction = VectorHelp.Vector.Normal(Direction);

            Active = true;
        }

        public void Move()
        {
            Vector2f oldpos = Parent.Position;
            
            if (!Direction.Equals(new Vector2f(0, 0)))
            {
                if (Direction.X > 0)
                {
                    //Facing Right
                    Parent.FacingDirection = new Vector2f(1, 0);
                }
                else
                {
                    //facing left
                    Parent.FacingDirection = new Vector2f(-1, 0);
                }
                Vector2 direction = new Vector2(ConvertUnits.ToSimUnits(Direction.X), ConvertUnits.ToSimUnits(Direction.Y));
                direction.Normalize();
                Parent.GetComponent<IBody>().Body.LinearVelocity += Velocity * direction;
                Parent.GetComponent<IBody>().Body.ApplyForce(direction);
            }
            PostEvent(new MovedEventArgs(oldpos, Parent.Position, Direction, 0));
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if(spawned != null)
            {
                Active = true;
                Parent.GetComponent<CharacterBody>().Active = false;
            }

            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null && Active)
            {
                _delta = updated.Delta;
                Move();
;
                if(_jumpingUp)
                {
                    if(Parent.Scale.X + _deltascale < _maxScale && Parent.Scale.Y + _deltascale < _maxScale)
                    {
                        Parent.Scale += new Vector2f(_deltascale, _deltascale);
                    }
                    else
                    {
                        Parent.Scale = new Vector2f(_maxScale, _maxScale);
                        _jumpingUp = false;
                    }
                }
                else
                {
                    if (Parent.Scale.X - _deltascale > 1.0 && Parent.Scale.Y - _deltascale > 1.0f)
                    {
                        Parent.Scale -= new Vector2f(_deltascale, _deltascale);
                    }
                    else
                    {
                        Parent.Scale = new Vector2f(1.0f, 1.0f);
                        _jumpingUp = true;
                        Active = false;

                        Parent.GetComponent<CharacterBody>().Active = true;
                        List<IBehaviour> behaviours = Parent.GetAllComponents<IBehaviour>();
                        foreach(IBehaviour b in behaviours)
                        {
                            if(b != this)
                            {
                                b.Active = true;
                            }
                        }
                    }
                }
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
