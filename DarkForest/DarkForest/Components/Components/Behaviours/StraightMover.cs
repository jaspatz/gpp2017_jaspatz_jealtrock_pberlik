﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using DarkForest.CustomEventArgs;
using Microsoft.Xna.Framework;
using DarkForest.Entities;

namespace DarkForest.Components.Components.Behaviours
{
    class StraightMover : IBehaviour
    {
        public float Velocity { get; set; }
        public Vector2f Direction { get; set; }
        public Entity Parent { get; set; }

        public bool Active { get; set; }

        public StraightMover(float velocity)
        {
            Velocity = velocity;
        }

        public void Move()
        {
            Parent.GetComponent<IBody>().Body.LinearVelocity = new Vector2(Parent.FacingDirection.X, Parent.FacingDirection.Y) * Velocity;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                Move();
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
