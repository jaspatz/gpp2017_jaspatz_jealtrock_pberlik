﻿using DarkForest.Components.Components.Skills;
using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Entities;
using DarkForest.Enums;
using FarseerPhysics;
using Microsoft.Xna.Framework;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Behaviours
{
	class PlayerControlled : IBehaviour
	{
		public Entity Parent { get; set; }
        public float Velocity { get; set; }
        public Vector2f Direction { get; set; }

        public bool Active { get; set; }

        private float _deltas;
        private bool _directionChanged;

		public PlayerControlled(float velocity)
		{
            Velocity = velocity;
		}

        private void ProcessInput()
        {
            bool upPressed = Keyboard.IsKeyPressed(Keyboard.Key.Up);
            bool downPressed = Keyboard.IsKeyPressed(Keyboard.Key.Down);
            bool rightPressed = Keyboard.IsKeyPressed(Keyboard.Key.Right);
            bool leftPressed = Keyboard.IsKeyPressed(Keyboard.Key.Left);
            
            Vector2f direction = new Vector2f();

            if (upPressed)
                direction.Y -= 1f;
            if (downPressed)
                direction.Y += 1f;
            if (rightPressed)
                direction.X += 1f;
            if (leftPressed)
                direction.X -= 1f;
            
            if (!direction.Equals(Direction))
            {
                _directionChanged = true;
            }
            Direction = direction;
            Move();

            if (Keyboard.IsKeyPressed(Keyboard.Key.Q))
            {
                Parent.Notify(this, new CustomEventArgs.KeyEventArgs(KeyAction.UseFireball));
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.W))
            {
                Parent.Notify(this, new CustomEventArgs.KeyEventArgs(KeyAction.UseThunder));
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.E))
            {
                Parent.Notify(this, new CustomEventArgs.KeyEventArgs(KeyAction.UseFreeze));
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.R))
            {
                Parent.Notify(this, new CustomEventArgs.KeyEventArgs(KeyAction.UseShield));
            }
        }

        public void Move()
        {
            Vector2f oldpos = Parent.Position;
            
            if (!Direction.Equals(new Vector2f(0, 0)))
            {
                if (_directionChanged)
                {
                    if ((Parent.FacingDirection.Equals(new Vector2f(0, -1)) || Parent.FacingDirection.Equals(new Vector2f(0, 1))) && Direction.X != 0 || Direction.Y == 0)
                    {
                        Parent.FacingDirection = new Vector2f(Direction.X, 0);
                        _directionChanged = false;
                    }
                    else if ((Parent.FacingDirection.Equals(new Vector2f(-1, 0)) || Parent.FacingDirection.Equals(new Vector2f(1, 0))) && Direction.Y != 0 || Direction.X == 0)
                    {
                        Parent.FacingDirection = new Vector2f(0, Direction.Y);
                        _directionChanged = false;
                    }
                }

                Vector2 direction = new Vector2(ConvertUnits.ToSimUnits(Direction.X), ConvertUnits.ToSimUnits(Direction.Y));
                direction.Normalize();
                Parent.GetComponent<IBody>().Body.LinearVelocity += Velocity * direction;
                //Parent.Body.ApplyForce(direction);
            }
            Vector2f newpos = Parent.Position;
            PostEvent(new MovedEventArgs(oldpos, newpos, Direction, Velocity));
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                _deltas += updated.Delta;
                ProcessInput();
            }
        }

		public void PostEvent(EventArgs args)
		{
            Parent.Notify(this, args);
		}
	}
}
