﻿using DarkForest.Components.Interfaces;
using DarkForest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Behaviours
{
    class JumpAroundPlayer
    {
        public Entity Parent { get; set; }
        public float Velocity { get; set; }
        
        public void HandleEvent(object sender, EventArgs args)
        {
        }

        public void PostEvent(EventArgs args)
        {
        }
    }
}
