﻿using DarkForest.Components.Components.Behaviours;
using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Entities;
using DarkForest.Factories;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Skills
{
    class FreezeSkill : Skill
    {
        public override Entity Parent { get { return _parent; } set { _parent = value; _iceblock = ProjectileFactory.CreateFreezeStrike(Parent); } }
        private Entity _iceblock;
        private List<Entity> _frozen;
        private bool _attacking;
        private bool _melting;
        
        private float _deltas;
        private Entity _parent;

        public FreezeSkill() : base(0.08f, 0.12f, 7)
        {
            BaseDamage = 3;
            BaseMana = 70;

            _max_dmg = 6;
            _min_mana = 35;
            _min_cd = 2;

            _dmg_inc = 0.5f;
            _mana_dec = 5;
            _cd_dec = 0.5f;

            Level = 1;

            Key = Enums.KeyAction.UseFreeze;

            _frozen = new List<Entity>();

            _attacking = false;
            _melting = false;
        }

        public override void OnUse()
        {
            _iceblock.Position = new Vector2f(Parent.Position.X, Parent.Position.Y);
            _iceblock.GetComponent<IBody>().Active = true;
            _iceblock.Notify(this, new AttackedEventArgs(this));
            _attacking = true;
        }

        private void IceMelting()
        {
            _melting = true;
            _iceblock.GetComponent<IBody>().Active = false;
            foreach (Entity e in _frozen)
            {
                /*List<IBehaviour> behaviours = e.GetAllComponents<IBehaviour>();
                foreach (IBehaviour b in behaviours)
                {
                    if (b is FollowPlayer && !e.GetComponent<IKillable>().IsDead)
                    {
                        b.Active = true;
                    }
                }
                e.GetComponent<IAnimator>().Unpause();*/
                e.DisableButRender = false;
                e.Spritesheet.Color = Color.White;
            }
            _frozen = new List<Entity>();
            _iceblock.Notify(this, new AttackInterruptedEventArgs(this));
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            CollidedEventArgs collided = args as CollidedEventArgs;
            if (collided != null)
            {
                if (collided.Entity == _iceblock)
                {
                    Entity victim = Game.GetEntityOfFixture(collided.Fixture);
                    if (victim != Parent)
                    {
                        IBody body = victim?.GetComponent<IBody>();
                        if (body != null && body.Active)
                            _frozen.Add(victim);
                    }
                }
            }

            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                if (_attacking && !_melting)
                {
                    _deltas += updated.Delta;
                    if (_deltas > Damage)
                    {
                        _deltas = 0;
                        IceMelting();
                    }
                }

                if (_melting)
                {
                    _deltas += updated.Delta;
                    if(_deltas > Duration)
                    {
                        _melting = false;
                        _attacking = false;
                    }
                }

                foreach (Entity e in _frozen)
                {
                    //do something for all frozen enemys
                    /*List<IBehaviour> behaviours = e.GetAllComponents<IBehaviour>();
                    foreach(IBehaviour b in behaviours)
                    {
                        if(b is FollowPlayer)
                        {
                            b.Active = false;
                        }
                    }
                    e.GetComponent<IAnimator>().Pause();*/
                    e.DisableButRender = true;
                    e.Spritesheet.Color = Color.Blue;
                }

                if (_iceblock.GetComponent<IBody>().Active)
                    _iceblock.Update(updated.Delta);
            }

            RenderedEventArgs rendered = args as RenderedEventArgs;
            if (rendered != null)
            {
                if (_attacking)
                    _iceblock.Draw(rendered.Renderer);
            }
        }
    }
}
