﻿using DarkForest.CustomEventArgs;
using DarkForest.Factories;
using DarkForest.Entities;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Components.Interfaces;
using SFML.System;
using DarkForest.Enums;

namespace DarkForest.Components.Components.Skills
{
    class ThunderSkill : Skill
    {
        public override Entity Parent { get { return _parent; } set
            {
                _parent = value;
                _verThunderbolt = ProjectileFactory.CreateThunderStrike(_parent, false);
                _verThunderbolt.GetComponent<IBody>().Active = false;
                _horThunderbolt = ProjectileFactory.CreateThunderStrike(_parent, true);
                _horThunderbolt.GetComponent<IBody>().Active = false;

                _currentThunderbolt = _verThunderbolt;
            }
        }

        private List<Entity> _takingDamage;

        private Entity _horThunderbolt;
        private Entity _verThunderbolt;

        private Entity _currentThunderbolt;
        
        private Entity _parent;

        private bool _attacked;
        private float _deltas;
        
        private float _dmgPerTick;
        
        public ThunderSkill() : base(0.08f, 0.12f, 7f)
        {
            BaseDamage = 400;
            BaseMana = 80;

            _max_dmg = 10000;
            _min_mana = 34;
            _min_cd = 1f;

            _dmg_inc = 60;
            _mana_dec = 4;
            _cd_dec = 0.5f;

            Level = 1;

            _dmgPerTick = GameLogic.TICK * Damage;

            _attacked = false;
            _takingDamage = new List<Entity>();
            _deltas = 0;

            Key = KeyAction.UseThunder;
        }

        public override void OnUse()
        {
            Vector2f offset;
            if (Parent.GetOrientation().Equals("left") || Parent.GetOrientation().Equals("right"))
            {
                _currentThunderbolt = _horThunderbolt;
                offset = new Vector2f(126 * Parent.FacingDirection.X, Parent.FacingDirection.Y);
            }
            else
            {
                _currentThunderbolt = _verThunderbolt;
                if (Parent.GetOrientation().Equals("up"))
                    offset = new Vector2f(Parent.FacingDirection.X, 110f * Parent.FacingDirection.Y);
                else
                    offset = new Vector2f(Parent.FacingDirection.X, 130f * Parent.FacingDirection.Y);
            }

            _attacked = true;
            _currentThunderbolt.FacingDirection = Parent.FacingDirection;
            _currentThunderbolt.Position = new Vector2f(Parent.Position.X, Parent.Position.Y) + offset;
            _currentThunderbolt.GetComponent<IBody>().Active = true;
            _currentThunderbolt.Notify(this, new AttackedEventArgs(this));
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            CollidedEventArgs collided = args as CollidedEventArgs;
            if (collided != null)
            {
                if (collided.Entity.Description.Contains("ThunderStrike"))
                {
                    Entity victim = Game.GetEntityOfFixture(collided.Fixture);
                    if (victim != Parent)
                    {
                        Entity thunder = collided.Entity;

                        if (victim != null)
                        {
                            _takingDamage.Add(victim);
                        }
                    }
                }
            }

            SeperatedEventArgs seperated = args as SeperatedEventArgs;
            if (seperated != null)
            {
                if (seperated.Entity.Description.Contains("ThunderStrike"))
                {
                    Entity victim = Game.GetEntityOfFixture(seperated.Fixture);
                    if (victim != Parent)
                    {
                        Entity thunder = seperated.Entity;

                        if (victim != null)
                        {
                            _takingDamage.Remove(victim);
                        }
                    }
                }
            }

            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null && _parent != null)
            {
                if (_attacked)
                {
                    _deltas += updated.Delta;
                    if (_deltas > Duration + 0.1f)
                    {
                        _deltas = 0;
                        _attacked = false;
                        
                        _currentThunderbolt.GetComponent<IBody>().Active = false;
                        
                        _takingDamage = new List<Entity>();
                    }
                }

                foreach(Entity e in _takingDamage)
                {
                    e.Notify(this, new HitEventArgs(Parent, e, _dmgPerTick));
                }

                if(_currentThunderbolt.GetComponent<IBody>().Active)
                    _currentThunderbolt.Update(updated.Delta);
            }

            SkillUpgradedEventArgs upgraded = args as SkillUpgradedEventArgs;
            if(upgraded != null)
            {
                _dmgPerTick = GameLogic.TICK * Damage;
            }

            RenderedEventArgs rendered = args as RenderedEventArgs;
            if (rendered != null && _parent != null)
            {
                if (_currentThunderbolt.GetComponent<IBody>().Active)
                    _currentThunderbolt.Draw(rendered.Renderer);
            }
        }
    }
}
