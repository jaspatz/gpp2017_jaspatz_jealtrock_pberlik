﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using DarkForest.Enums;

namespace DarkForest.Components.Components.Skills
{
    abstract class Skill : ISkill
    {
        public event EventHandler<AttackedEventArgs> Used;
        public event EventHandler<UpgradeSkillEventArgs> Upgraded;

        public int Level { get { return _level; } set
            {
                _level = value;
                float cd = BaseCooldown - (Level - 1) * _cd_dec;
                if (cd > _min_cd)
                    Cooldown = cd;
                else
                    Cooldown = _min_cd;

                float mana = BaseMana - (Level - 1) * _mana_dec;
                if (mana > _min_mana)
                    Mana = mana;
                else
                    Mana = _min_mana;

                float dmg = BaseDamage + (Level - 1) * _dmg_inc;
                if (dmg < _max_dmg)
                    Damage = dmg;
                else
                    Damage = _max_dmg;
            }
        }
        public float ChannelAttack { get; set; }
		public float ReleaseAttack { get; set; }
		public float Duration { get { return ChannelAttack + ReleaseAttack; } }
        public float BaseCooldown { get; set; }
		public float Cooldown { get; private set; }
		public float CooldownCounter { get; set; }
		public bool Attacking { get; set; }
		public virtual Entity Parent { get; set; }
        public float BaseDamage { get; set; }
        public float Damage { get; private set; }
        public float BaseMana { get; set; }
        public float Mana { get; set; }
        public KeyAction Key { get; set; }

        public bool Active { get { return _active; } set { _active = value; PostEvent(new AttackInterruptedEventArgs(this)); } }

        private bool _active;

		private float _channel_counter;
		private float _release_counter;

        private int _level;

		private bool _channeling;

        private float _deltas;
        private bool _isUpgrading;
        private bool _upgraded;
        private bool _onCleanup;
        private bool _notified;

        //variables for increase and decrease per Level
        protected float _dmg_inc;
        protected float _mana_dec;
        protected float _cd_dec;

        protected float _max_dmg;
        protected float _min_cd;
        protected float _min_mana;

        public Skill(float channel, float release, float cooldown)
		{
			ChannelAttack = channel;
			ReleaseAttack = release;

			BaseCooldown = cooldown;

			Attacking = false;
			_channeling = false;

			_channel_counter = 0;
			_release_counter = 0;

            _isUpgrading = false;
            _onCleanup = false;
            _upgraded = false;
		}

		public abstract void OnUse();
		public abstract void HandleOtherEvent(object sender, EventArgs args);

        public void Reset()
        {
            Attacking = false;
            _channeling = true;
            _channel_counter = 0;
            _release_counter = 0;
        }

		public void Use()
		{
            if (!Active) return;

            if (_isUpgrading || _upgraded)
            {
                if (_upgraded)
                    return;
                Level++;
                Parent.Notify(this, new SkillUpgradedEventArgs());
                Upgraded?.Invoke(this, new UpgradeSkillEventArgs());
                _isUpgrading = false;
                _upgraded = true;
                return;
            }

            if (_onCleanup) return;

            if (CooldownCounter <= 0 && !Attacking)
			{
                UseSkill();
			}
		}

        protected void UseSkill()
        {
            if (Mana > 0) //If the spell costs mana, check if we have enough
            {
                float playerMana = Parent.GetComponent<IResource>().CurrentMana;
                if (playerMana < Mana)
                {
                    if (!_notified)
                    {
                        Parent.Notify(this, new NoManaEventArgs());
                        _notified = true;
                    }
                    return;
                }
            }
            AttackedEventArgs attacked = new AttackedEventArgs(this);
            PostEvent(attacked);
            Used?.Invoke(this, attacked);
            _channeling = true;
            Attacking = true;
        }
        
        public void HandleEvent(object sender, EventArgs args)
        {
			UpdatedEventArgs updated = args as UpdatedEventArgs;
			if (updated != null)
			{
                if (_upgraded)
                {
                    _deltas += updated.Delta;
                    if (_deltas >= 0.5)
                    {
                        _deltas = 0;
                        _upgraded = false;
                    }
                }

                if (_notified)
                {
                    _deltas += updated.Delta;
                    if (_deltas >= 0.5)
                    {
                        _deltas = 0;
                        _notified = false;
                    }
                }

                if (Attacking && Active)
				{
					if (_channeling)
					{
						_channel_counter += updated.Delta;
						if (_channel_counter >= ChannelAttack)
						{
							_channeling = false;
                            OnUse();
						}
					}
					else
					{
						_release_counter += updated.Delta;
						if (_release_counter >= ReleaseAttack)
						{
                            Reset();
							CooldownCounter = Cooldown;
						}
					}
				}
				else
				{
					CooldownCounter -= updated.Delta;
                    Reset();
				}
			}

            UpgradeSkillEventArgs upgraded = args as UpgradeSkillEventArgs;
            if (upgraded != null)
            {
                _isUpgrading = true;
            }

            CleanupEventArgs cleanup = args as CleanupEventArgs;
            if(cleanup != null)
            {
                _onCleanup = true;
            }

            RoundStartedEventArgs stoppedupgrade = args as RoundStartedEventArgs;
            if (stoppedupgrade != null)
            {
                _isUpgrading = false;
                _onCleanup = false;
            }

            KeyEventArgs keyArgs = args as KeyEventArgs;
            if (keyArgs != null)
            {
                if (keyArgs.Key == Key)
                {
                    Use();
                }
            }

            DieEventArgs died = args as DieEventArgs;
            if(died != null)
            {
                Active = false;
            }

            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if(spawned != null)
            {
                Active = true;
            }

            HandleOtherEvent(sender, args);
		}

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
