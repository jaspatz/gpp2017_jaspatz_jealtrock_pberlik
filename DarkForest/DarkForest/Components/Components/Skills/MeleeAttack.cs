﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;

namespace DarkForest.Components.Components.Skills
{
    class MeleeAttack : Skill
    {
        public Entity Victim { get; set; }
        
        private bool _inRange;

        public MeleeAttack(Entity victim) : base(0.55f/1.7f, 0.45f/1.7f, 0.5f)
        {
            Victim = victim;
            BaseDamage = 26.7f;

            _max_dmg = 44;
            _min_mana = 0;
            _min_cd = 0;

            _dmg_inc = 2.3f;
            _mana_dec = 0;
            _cd_dec = 0.08f;

            Level = 1;

            _inRange = false;
        }

        public override void OnUse()
        {
            HitEventArgs args = new HitEventArgs(Parent, Victim, Damage);

            PostEvent(args);
            Victim.Notify(this, args);
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                if (_inRange)
                {
                    Use();
                }
            }

            RangeEventArgs rangeArgs = args as RangeEventArgs;
            if(rangeArgs != null)
            {
                switch (rangeArgs.Type)
                {
                    case RangeEventArgs.EventType.Entered:
                        _inRange = true;
                        break;
                    case RangeEventArgs.EventType.Left:
                        _inRange = false;
                        Reset();
                        PostEvent(new AttackInterruptedEventArgs(this));
                        break;
                }
            }
        }
	}
}
