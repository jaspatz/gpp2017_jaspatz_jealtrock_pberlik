﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Factories;
using DarkForest.Entities;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Enums;

namespace DarkForest.Components.Components.Skills
{
	class FireballSkill : Skill
	{
        private ObjectPool<Entity> _verFireball;
        private ObjectPool<Entity> _horFireball;

        private List<Entity> _fireballs;
        
        public FireballSkill() : base(0.08f, 0.12f, 0.05f)
        {
            BaseDamage = 50;
            BaseMana = 10;

            _max_dmg = 1000;
            _min_mana = 2;
            _min_cd = 0.06f;

            _dmg_inc = 15;
            _mana_dec = 1;
            _cd_dec = 0.005f;

            Level = 1;

            Key = KeyAction.UseFireball;

            _fireballs = new List<Entity>();
            _verFireball = new ObjectPool<Entity>(() => ProjectileFactory.CreateFireball(Parent, false));
            _horFireball = new ObjectPool<Entity>(() => ProjectileFactory.CreateFireball(Parent, true));
        }

        public override void OnUse()
        {
            Entity fireball;

            if (Parent.GetOrientation().Equals("left") || Parent.GetOrientation().Equals("right"))
            {
                fireball = _horFireball.GetObject();
            }
            else
            {
                fireball = _verFireball.GetObject();
            }

            fireball.FacingDirection = Parent.FacingDirection;
            fireball.Position = new Vector2f(Parent.Position.X + 10f * Parent.FacingDirection.X, Parent.Position.Y + 10f * Parent.FacingDirection.Y);
            fireball.GetComponent<IBody>().Active = true;
            fireball.Notify(this, new AttackedEventArgs(this));
            _fireballs.Add(fireball);
        }

        private void ReturnFireball(Entity fireball)
        {
            fireball.GetComponent<IBody>().Active = false;
            _fireballs.Remove(fireball);

            if (fireball.GetOrientation() == "up" || fireball.GetOrientation() == "down")
            {
                _verFireball.PutObject(fireball);
            }
            else
            {
                _horFireball.PutObject(fireball);
            }
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
		{
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                foreach (Entity fireball in _fireballs)
                {
                    fireball.Update(updated.Delta);
                }
            }

            CollidedEventArgs collided = args as CollidedEventArgs;
            if(collided != null)
            {
                if (collided.Entity.Description.Contains("Fireball"))
                {
                    Entity victim = Game.GetEntityOfFixture(collided.Fixture);
                    if (victim != Parent)
                    {
                        //Console.WriteLine(collided.Entity.Description + "(Entity1) just hit " + victim?.Description + " (Entity2) and it is not the same as " + Parent.Description);

                        Entity fireball = collided.Entity;

                        if (victim != null)
                        {
                            victim.Notify(this, new HitEventArgs(Parent, victim, Damage));
                            ReturnFireball(fireball);
                        }
                        else
                        {
                            if(collided.Fixture.CollisionCategories == Category.Cat10) //Cat10 == Boundarys
                            {
                                ReturnFireball(fireball);
                            }
                        }
                    }
                }
            }

            RenderedEventArgs rendered = args as RenderedEventArgs;
            if(rendered != null)
            {
                foreach(Entity fireball in _fireballs)
                {
                    fireball.Draw(rendered.Renderer);
                }
            }
		}
	}
}
