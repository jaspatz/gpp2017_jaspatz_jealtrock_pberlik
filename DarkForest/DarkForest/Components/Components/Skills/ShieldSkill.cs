﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Entities;
using DarkForest.Factories;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Skills
{
    class ShieldSkill : Skill
    {
        public bool InUse { get; set; }

        private Entity _shield;

        public ShieldSkill() : base(0.08f, 0.12f, 0.5f)
        {
            BaseDamage = 0.5f;
            BaseMana = 50 * GameLogic.TICK;

            _max_dmg = 2;
            _min_mana = 20 * GameLogic.TICK;
            _min_cd = 0.5f;

            _dmg_inc = -0.5f;
            _mana_dec = 5 * GameLogic.TICK;
            _cd_dec = 0;

            Level = 1;

            Key = Enums.KeyAction.UseShield;

            _shield = ProjectileFactory.CreateShield();
        }

        public override void OnUse()
        {
            InUse = !InUse;
            if (InUse)
            {
                Parent.GetComponent<IBehaviour>().Velocity += Damage;
                Parent.GetComponent<IBody>().Body.Mass += Damage;
                Parent.GetComponent<IKillable>().Active = false;
                _shield.Position = Parent.Position;
                _shield.Notify(this, new AttackedEventArgs(this));
            }
            else
            {
                TurnOff();
            }
        }

        private void TurnOff()
        {
            Parent.GetComponent<IBehaviour>().Velocity -= Damage;
            Parent.GetComponent<IBody>().Body.Mass -= Damage;
            Parent.GetComponent<IKillable>().Active = true;
            AttackInterruptedEventArgs interrupted = new AttackInterruptedEventArgs(this);
            PostEvent(interrupted);
            _shield.Notify(this, interrupted);
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if(updated != null)
            {
                if (InUse)
                {
                    float playerMana = Parent.GetComponent<IResource>().CurrentMana;
                    if (playerMana < Mana)
                    {
                        Parent.Notify(this, new NoManaEventArgs());
                        InUse = false;
                    }
                    else
                    {
                        PostEvent(new UseManaEventArgs(-Mana));
                        _shield.Position = Parent.Position;
                        _shield.Update(updated.Delta);

                    }
                }
            }

            NoManaEventArgs noMana = args as NoManaEventArgs;
            if(noMana != null)
            {
                if (InUse)
                {
                    float playerMana = Parent.GetComponent<IResource>().CurrentMana;
                    if (playerMana < Mana)
                    {
                        TurnOff();
                        InUse = false;
                    }
                }
            }
            
            RenderedEventArgs rendered = args as RenderedEventArgs;
            if (rendered != null)
            {
                if (InUse)
                {
                    rendered.Renderer.Draw(_shield.Spritesheet);
                }
            }
        }
    }
}
