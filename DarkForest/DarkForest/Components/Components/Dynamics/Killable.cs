﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Dynamics
{
    class Killable : IKillable
    {
        public event EventHandler<HitEventArgs> GotHit;
        public event EventHandler<RefillingEventArgs> Refill;
        public event EventHandler<DieEventArgs> Died;

        public float MaxHP { get; set; }
        public float CurrentHP { get { return _currentHP; } set
            {
                if (value > _currentHP)
                {
                    if (value <= MaxHP)
                    {
                        Refill?.Invoke(this, new RefillingEventArgs(value - CurrentHP));
                        _currentHP = value;
                    }
                    else
                    {
                        CurrentHP = MaxHP;
                    }
                }
                else
                {
                    if (!IsDead)
                    {
                        if (value > 0)
                        {
                            GotHit?.Invoke(this, new HitEventArgs(null, Parent, CurrentHP - value));
                            _currentHP = value;
                        }
                        else
                        {
                            Die();
                        }
                    }
                }
            }
        }
        public bool IsDead { get; set; }
        public Entity Parent { get; set; }
        public bool Active { get; set; }
        
        private float _currentHP;
        private bool _inLava;

        public Killable(float hitpoints)
        {
            MaxHP = hitpoints;
            CurrentHP = hitpoints;
            IsDead = false;
            Active = true;
        }

        private void Revive()
        {
            IsDead = false;
            CurrentHP = MaxHP;
            Parent.ZIndex = (int) Enums.Zindex.Entity;
        }

        private void Die()
        {
            GotHit?.Invoke(this, new HitEventArgs(null, Parent, CurrentHP));
            _currentHP = 0;
            IsDead = true;
            _inLava = false;
            Parent.ZIndex = (int)Enums.Zindex.Corpse;
            DieEventArgs died = new DieEventArgs(Parent);
            PostEvent(died);
            PostEvent(new RangeEventArgs(RangeEventArgs.EventType.Left, 2f));
            Died?.Invoke(this, died);
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            if (!Active) return;

            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if(spawned != null)
            {
                Revive();
            }

            HitEventArgs hitted = args as HitEventArgs;
            if(hitted != null)
            {
                if (hitted.Victim == Parent && !IsDead)
                {
                    CurrentHP -= hitted.Damage;
                }
            }

            LavaEventArgs inLava = args as LavaEventArgs;
            if(inLava != null)
            {
                _inLava = inLava.InLava;
            }

            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if(updated != null)
            {
                if (_inLava)
                {
                    PostEvent(new HitEventArgs(null, Parent, 1));
                }
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
