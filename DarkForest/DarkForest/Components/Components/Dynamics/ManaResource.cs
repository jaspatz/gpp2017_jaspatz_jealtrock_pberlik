﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.CustomEventArgs;
using DarkForest.Entities;

namespace DarkForest.Components.Components.Dynamics
{
    class ManaResource : IResource
    {
        public event EventHandler<UseManaEventArgs> UseResource;
        public event EventHandler<RefillingEventArgs> Refill;

        public float RefillingRate { get; set; }
        public float MaxMana { get; set; }
        public float CurrentMana
        {
            get { return _currentMana; }
            set
            {
                if (value <= MaxMana && value > 0)
                {
                    if(value > _currentMana)
                    {
                        Refill?.Invoke(this, new RefillingEventArgs(_refillPerTick));
                    }
                    else
                    {
                        UseResource?.Invoke(this, new UseManaEventArgs(value - CurrentMana));
                        _timeLastUse = 0f;
                    }
                    _currentMana = value;
                }
                else if (value > MaxMana)
                    _currentMana = MaxMana;
                else if (value <= 0)
                    _currentMana = 0;
            }
        }
        public bool IsRefilling { get; set; }
        public Entity Parent { get; set; }

        private float _currentMana;
        private float _refillPerTick;
        private float _timeTillRefill;

        private float _timeLastUse;

        public ManaResource(float mana)
        {
            MaxMana = mana;
            CurrentMana = mana;
            RefillingRate = 20;

            _refillPerTick = GameLogic.TICK * RefillingRate;
            _timeTillRefill = 1f;

            IsRefilling = false;
            _timeLastUse = 0;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if(updated != null)
            {
                _timeLastUse += updated.Delta;
                if(_timeLastUse >= _timeTillRefill)
                {
                    IsRefilling = true;
                }
                else
                {
                    IsRefilling = false;
                }

                if (IsRefilling)
                {
                    CurrentMana += _refillPerTick;
                }
            }

            UseManaEventArgs used = args as UseManaEventArgs;
            if(used != null)
            {
                CurrentMana += used.Mana;
            }

            AttackedEventArgs attacked = args as AttackedEventArgs;
            if(attacked != null)
            {
                CurrentMana -= attacked.Skill.Mana;
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
