﻿using DarkForest.Components.Interfaces;
using DarkForest.Entities;
using DarkForest.Loader;
using SFML.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Audio
{
    abstract class AudioComponent : IAudio
    {
        public Entity Parent { get; set; }

        private List<Sound> _soundsPlaying;
        private Sound _currentSound;
        protected bool _soundPlayed;

        public AudioComponent()
        {
            _currentSound = null;
        }

        public void PlaySound(string filename, int volume = 100, bool looping = false)
        {
            if (!_soundPlayed)
            {
                _soundPlayed = true;
                Sound newsound = AudioLoader.GetSound("Assets/Sounds/" + filename + ".wav");
                if (_currentSound != newsound)
                {
                    _currentSound = newsound;
                    _currentSound.Volume = volume;
                    _currentSound.Play();
                }
                else
                {
                    if (_currentSound.Status == SoundStatus.Playing)
                    {
                        Sound copy = new Sound(newsound);
                        _currentSound = copy;
                    }
                    _currentSound.Play();
                }
                _currentSound.Loop = looping;
            }
        }

        public void PauseSound()
        {
            _currentSound?.Pause();
        }

        public void StopSound()
        {
            _currentSound?.Stop();
        }

        public abstract void HandleEvent(object sender, EventArgs args);

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
