﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using SFML.Audio;
using DarkForest.CustomEventArgs;
using DarkForest.Loader;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Audio
{
    class PlayerAudio : AudioComponent
    {
        private float _playerHit_cd;
        private bool _canPlayHit;
        private float _deltas;

        private Sound _shieldSound;

        public PlayerAudio()
        {
            _playerHit_cd = 0.3f;
            _canPlayHit = true;
            _deltas = 0;

            _shieldSound = AudioLoader.GetSound("Assets/Sounds/shield.wav");
        }

        public override void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if(updated != null)
            {
                _soundPlayed = false;
                if (!_canPlayHit)
                {
                    _deltas += updated.Delta;
                    if(_deltas >= _playerHit_cd)
                    {
                        _canPlayHit = true;
                        _deltas = 0;
                    }
                }
            }

            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                if(attacked.Skill is FireballSkill)
                {
                    PlaySound("fireball", 50);
                }
                if (attacked.Skill is ThunderSkill)
                {
                    PlaySound("thunder");
                }
                if (attacked.Skill is FreezeSkill)
                {
                    PlaySound("freeze");
                }
                if (attacked.Skill is ShieldSkill)
                {
                    if(_shieldSound.Status != SoundStatus.Playing)
                        PlaySound("shield", 100, true);
                }
            }

            AttackInterruptedEventArgs interrupted = args as AttackInterruptedEventArgs;
            if(interrupted != null)
            {
                if(interrupted.Skill is ShieldSkill)
                {
                    _shieldSound.Stop();
                }
            }

            SkillUpgradedEventArgs upgraded = args as SkillUpgradedEventArgs;
            if (upgraded != null)
            {
                PlaySound("lvlupsound");
            }

            HitEventArgs hitted = args as HitEventArgs;
            if (hitted != null)
            {
                if (hitted.Victim == Parent && _canPlayHit)
                {
                    PlaySound("playerhit");
                    _canPlayHit = false;
                }
            }

            NoManaEventArgs noMana = args as NoManaEventArgs;
            if(noMana != null)
            {
                PlaySound("nomana");
            }

            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if(spawned != null)
            {
                PlaySound("spawning");
            }
        }
    }
}
