﻿using DarkForest.Components.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using FarseerPhysics.Dynamics;
using SFML.System;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using FarseerPhysics;
using DarkForest.CustomEventArgs;

namespace DarkForest.Components.Components.Physicals
{
    class ProjectileBody : IBody
    {
        public Body Body { get; set; }
        public Fixture Fixture { get; set; }
        public Entity Parent { get; set; }
        public Entity Owner { get; set; }
        public bool Active { get { return _active; } set { if (value) Fixture.CollisionCategories = Category.Cat1; else Fixture.CollisionCategories = Category.None; _active = value; } }
        
        private bool _active;

        public ProjectileBody(Entity owner, Vector2i size, Vector2f position, World world)
        {
            Owner = owner;

            Body = BodyFactory.CreateBody(world, new Vector2(ConvertUnits.ToSimUnits(position.X), ConvertUnits.ToSimUnits(position.Y)));
            Body.LinearDamping = 0f;
            Body.FixedRotation = true;
            Body.BodyType = BodyType.Dynamic;

            Fixture = FixtureFactory.AttachRectangle(ConvertUnits.ToSimUnits(size.X - 2), ConvertUnits.ToSimUnits(size.Y - 2), 1f, new Vector2(), Body);
            Fixture.CollisionCategories = Category.Cat2;
            Fixture.IsSensor = true;
            Fixture.OnCollision += OnCollision;
            Fixture.OnSeparation += OnSeparation;

            Active = true;
        }

        private void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            if (fixtureB.CollisionCategories == Category.Cat1 || fixtureB.CollisionCategories == Category.Cat10 || fixtureB.CollisionCategories == Category.Cat8)
            {
                SeperatedEventArgs seperated = new SeperatedEventArgs(Parent, fixtureB);

                PostEvent(seperated);
                Owner.Notify(this, seperated);
            }
        }

        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, FarseerPhysics.Dynamics.Contacts.Contact contact)
        {
            if(fixtureB.CollisionCategories == Category.Cat1 || fixtureB.CollisionCategories == Category.Cat10 || fixtureB.CollisionCategories == Category.Cat8)
            {
                CollidedEventArgs collided = new CollidedEventArgs(Parent, fixtureB);

                PostEvent(collided);
                Owner.Notify(this, collided);
            }
            return true;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                Parent.Position = new Vector2f(ConvertUnits.ToDisplayUnits(Body.Position.X), ConvertUnits.ToDisplayUnits(Body.Position.Y));
            }

            PositionedEventArgs positioned = args as PositionedEventArgs;
            if (positioned != null)
            {
                Body.Position = new Vector2(ConvertUnits.ToSimUnits(Parent.Position.X), ConvertUnits.ToSimUnits(Parent.Position.Y));
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
