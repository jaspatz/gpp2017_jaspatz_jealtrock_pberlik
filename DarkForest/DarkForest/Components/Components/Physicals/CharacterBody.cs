﻿using DarkForest.Components.Interfaces;
using FarseerPhysics.Dynamics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkForest.Entities;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using FarseerPhysics;
using DarkForest.CustomEventArgs;
using FarseerPhysics.Dynamics.Contacts;

namespace DarkForest.Components.Components.Physicals
{
    class CharacterBody : IBody
    {
        public Body Body { get; set; }
        public Fixture Fixture { get; set; }
        public Entity Parent { get; set; }

        public bool Active { get { return _active; } set { if(value) Fixture.CollisionCategories = Category.Cat1; else Fixture.CollisionCategories = Category.None; _active = value; } }

        private Vector2 _offset;
        private bool _active;

        public CharacterBody(Vector2i size, Vector2f position, World world)
        {
            _offset = new Vector2(0f, (size.Y / 4f));

            Body = BodyFactory.CreateBody(world, new Vector2(ConvertUnits.ToSimUnits(position.X), ConvertUnits.ToSimUnits(position.Y + _offset.Y)));
            Body.LinearDamping = 20f;
            Body.Mass = 1f;
            Body.FixedRotation = true;
            Body.BodyType = BodyType.Dynamic;

            Fixture = FixtureFactory.AttachEllipse(ConvertUnits.ToSimUnits(size.X / 2 - 4), ConvertUnits.ToSimUnits(size.Y / 4 - 2), 6, 1f, Body);
            Fixture.OnCollision += OnCollision;
            Fixture.OnSeparation += OnSeparation;

            Active = true;
        }

        private void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            if (fixtureB.CollisionCategories == Category.Cat8)
            {
                Console.WriteLine("got out of lava");
                Parent.Notify(this, new LavaEventArgs(false));
            }
        }

        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if (fixtureB.CollisionCategories == Category.Cat8)
            {
                Console.WriteLine("got in lava");
                Parent.Notify(this, new LavaEventArgs(true));
            }
            return true;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if(updated != null)
            {
                Parent.Position = new Vector2f(ConvertUnits.ToDisplayUnits(Body.Position.X), ConvertUnits.ToDisplayUnits(Body.Position.Y) - _offset.Y);
            }

            PositionedEventArgs positioned = args as PositionedEventArgs;
            if(positioned != null)
            {
                Body.Position = new Vector2(ConvertUnits.ToSimUnits(Parent.Position.X), ConvertUnits.ToSimUnits(Parent.Position.Y + _offset.Y));
            }

            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if(spawned != null)
            {
                Active = true;
            }

            DieEventArgs died = args as DieEventArgs;
            if(died != null)
            {
                Active = false;
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
