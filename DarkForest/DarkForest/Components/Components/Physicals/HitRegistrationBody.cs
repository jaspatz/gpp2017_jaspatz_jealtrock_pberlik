﻿using DarkForest.CustomEventArgs;
using DarkForest.Entities;
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Physicals
{
    class HitRegistrationBody
    {
        public Body Body { get; set; }
        public Fixture Fixture { get; set; }
        public Entity Parent { get; set; }
        public bool Active { get { return _active; } set { if (value) Fixture.CollisionCategories = Category.Cat1; else Fixture.CollisionCategories = Category.None; _active = value; } }

        private bool _active;

        public HitRegistrationBody(Entity owner, Vector2i size, Vector2f position, World world)
        {
            Body = BodyFactory.CreateBody(world, new Vector2(ConvertUnits.ToSimUnits(position.X), ConvertUnits.ToSimUnits(position.Y)));
            Body.LinearDamping = 0f;
            Body.FixedRotation = true;
            Body.BodyType = BodyType.Dynamic;

            Fixture = FixtureFactory.AttachRectangle(ConvertUnits.ToSimUnits(size.X - 2), ConvertUnits.ToSimUnits(size.Y - 2), 1f, new Vector2(), Body);
            Fixture.CollisionCategories = Category.Cat2;
            Fixture.IsSensor = true;

            Active = true;
        }
        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                Parent.Position = new Vector2f(ConvertUnits.ToDisplayUnits(Body.Position.X), ConvertUnits.ToDisplayUnits(Body.Position.Y));
            }

            PositionedEventArgs positioned = args as PositionedEventArgs;
            if (positioned != null)
            {
                Body.Position = new Vector2(ConvertUnits.ToSimUnits(Parent.Position.X), ConvertUnits.ToSimUnits(Parent.Position.Y));
            }
        }

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
