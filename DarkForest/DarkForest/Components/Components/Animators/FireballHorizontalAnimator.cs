﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.CustomEventArgs;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Animators
{
    class FireballHorizontalAnimator : Animator
    {
        public FireballHorizontalAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("travel_left", new int[] { 1, 3, 5, 7, 9, 11 }, true);
            AddAnimation("travel_right", new int[] { 0, 2, 4, 6, 8, 10 }, true);

            Play("travel_left");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                String orientation = Parent.GetOrientation();
                if (attacked.Skill is FireballSkill)
                    Play("travel_" + orientation);
            }
        }
    }
}
