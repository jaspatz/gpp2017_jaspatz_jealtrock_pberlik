﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.CustomEventArgs;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Animators
{
    class FireballVerticalAnimator : Animator
    {
        public FireballVerticalAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("travel_up", new int[] { 6, 7, 8, 9, 10, 11 }, true);
            AddAnimation("travel_down", new int[] { 0, 1, 2, 3, 4, 5 }, true);
            
            Play("travel_up");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            AttackedEventArgs attacked = args as AttackedEventArgs;
            if(attacked != null)
            {
                String orientation = Parent.GetOrientation();
                if(attacked.Skill is FireballSkill)
                    Play("travel_" + orientation);
            }
        }
    }
}
