﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.CustomEventArgs;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Animators
{
    class ThunderHorizontalAnimator : Animator
    {
        public ThunderHorizontalAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("attack_right", new int[] { 1, 3, 5, 7, 9 }, false);
            AddAnimation("attack_left", new int[] { 0, 2, 4, 6, 8 }, false);

            Play("attack_left");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                String orientation = Parent.GetOrientation();
                if (attacked.Skill is ThunderSkill)
                    Play("attack_" + orientation);
            }
        }
    }
}
