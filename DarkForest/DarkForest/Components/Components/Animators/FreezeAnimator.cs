﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.CustomEventArgs;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Animators
{
    class FreezeAnimator : Animator
    {
        public FreezeAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("activate", new int[] { 3, 2, 1, 0 }, false);
            AddAnimation("deactivate", new int[] { 4, 5, 6, 7 }, false);

            Play("activate");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                if (attacked.Skill is FreezeSkill)
                {
                    Play("activate");
                    _currentAnimation.Duration = .1f;
                }
            }

            AttackInterruptedEventArgs interrupted = args as AttackInterruptedEventArgs;
            if (interrupted != null)
            {
                if (interrupted.Skill is FreezeSkill)
                {
                    Play("deactivate");
                    _currentAnimation.Duration = .1f;
                }
            }
        }
    }
}
