﻿using DarkForest.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.Components.Interfaces;

namespace DarkForest.Components.Components.Animators
{
    class PlayerAnimator : Animator
    {
        private bool _spawned;
        private float _dieTime;
        private float _alpha;
        private float _deltas;

        public PlayerAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("walk_down", new int[] { 0, 1, 2, 3, 4, 5, 6, 7 }, true);
            AddAnimation("walk_up", new int[] { 14, 15, 16, 17, 18, 19, 20, 21 }, true);
            AddAnimation("walk_left", new int[] { 28, 29, 30, 31, 32, 33, 34, 35 }, true);
            AddAnimation("walk_right", new int[] { 42, 43, 44, 45, 46, 47, 48, 49 }, true);

            AddAnimation("stand_down", new int[] { 8 }, true);
            AddAnimation("stand_up", new int[] { 22 }, true);
            AddAnimation("stand_left", new int[] { 36 }, true);
            AddAnimation("stand_right", new int[] { 50 }, true);

            AddAnimation("shoot_down", new int[] { 9, 10, 11, 12, 13 }, false);
            AddAnimation("shoot_up", new int[] { 23, 24, 25, 26, 27 }, false);
            AddAnimation("shoot_left", new int[] { 37, 38, 39, 40, 41 }, false);
            AddAnimation("shoot_right", new int[] { 51, 52, 53, 54, 55 }, false);

            Play("stand_down");

            _dieTime = 2.6f;
            _alpha = 255;
            _deltas = 0;
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                if (!isAlive)
                {
                    _deltas += updated.Delta;
                    if (_deltas < _dieTime)
                    {
                        float ds = _dieTime / updated.Delta;
                        float delta_alpha = 255 / ds;
                        _alpha -= delta_alpha;
                        if (_alpha < 0) _alpha = 0;
                        byte rounded = (byte)Math.Round(_alpha);
                        Parent.Spritesheet.Color = new SFML.Graphics.Color(150, 150, 150, rounded);
                        Parent.Position += new Vector2f(0, -1f);
                    }
                }
                if (_spawned)
                {
                    _deltas += updated.Delta;
                    if (_deltas < _dieTime)
                    {
                        float ds = _dieTime / updated.Delta;
                        float delta_alpha = 255 / ds;
                        _alpha += delta_alpha;
                        if (_alpha > 255)
                        {
                            _alpha = 255;
                        }
                        byte rounded = (byte)Math.Round(_alpha);
                        Parent.Spritesheet.Color = new SFML.Graphics.Color(255, 255, 255, rounded);
                    }
                    else
                    {
                        _deltas = 0;
                        _spawned = false;
                    }
                }
            }

            MovedEventArgs moved = args as MovedEventArgs;
            if (moved != null)
            {
                //If Player is shooting, return and let animation finish
                if (_animations["shoot_up"].IsPlaying || _animations["shoot_down"].IsPlaying || _animations["shoot_left"].IsPlaying || _animations["shoot_right"].IsPlaying)
                    return;
                
                if (moved.Direction.Equals(new Vector2f(0, 0)))
                    Play("stand_" + Parent.GetOrientation());
                else
                    Play("walk_" + Parent.GetOrientation());

                _currentAnimation.Duration = 1f / Parent.GetComponent<IBehaviour>().Velocity;
            }

            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                Play("shoot_" + Parent.GetOrientation());

                _currentAnimation.Duration = attacked.Skill.Duration;
            }

            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if(spawned != null)
            {
                _alpha = 0;
                _deltas = 0;
                isAlive = true;
                _spawned = true;
                Parent.FacingDirection = new Vector2f(0, 1);
                Play("shoot_" + Parent.GetOrientation());
                _currentAnimation.Duration = _dieTime;
                _spritesheet.Color = Color.White;
            }

            DieEventArgs died = args as DieEventArgs;
            if(died != null)
            {
                Parent.FacingDirection = new Vector2f(0, 1);
                Play("shoot_" + Parent.GetOrientation());
                _currentAnimation.Duration = _dieTime;
                isAlive = false;
            }

            HitEventArgs hitted = args as HitEventArgs;
            if(hitted != null)
            {
                if(hitted.Victim == Parent)
                {
                    Gothit = true;
                }
            }
        }
    }
}
