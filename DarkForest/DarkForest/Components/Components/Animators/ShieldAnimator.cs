﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.CustomEventArgs;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Animators
{
    class ShieldAnimator : Animator
    {
        public ShieldAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("activate", new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, true);

            Play("activate");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                if (attacked.Skill is ShieldSkill)
                {
                    Play("activate");
                    _currentAnimation.Duration = .1f;
                }
            }

            AttackInterruptedEventArgs interrupted = args as AttackInterruptedEventArgs;
            if (interrupted != null)
            {
                if (interrupted.Skill is ShieldSkill)
                    _currentAnimation.Stop();
            }
        }
    }
}
