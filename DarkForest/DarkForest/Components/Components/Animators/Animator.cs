﻿using DarkForest.Components.Interfaces;
using DarkForest.CustomEventArgs;
using DarkForest.Entities;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Animators
{
    abstract class Animator : IAnimator
    {
        public Entity Parent { get { return _parent; } set { _parent = value; _parent.Spritesheet.TextureRect = _currentAnimation.CurrentRect; } }

        protected Entity _parent;
        protected Sprite _spritesheet;
        protected Vector2i _size;

        protected Dictionary<string, Animation> _animations;
        protected Animation _currentAnimation;

        public bool Gothit { get; set; }
        private bool _recover;
        private float _hitTime;
        private float _red;
        private bool _paused;

        public bool isAlive { get; set; }

        private float _deltas;

        public Animator(Sprite spritesheet, Vector2i size)
		{
            _spritesheet = spritesheet;
            _size = size;
			_animations = new Dictionary<string, Animation>();

            Gothit = false;
            _recover = false;
            _hitTime = 0.05f;
            _red = 0;

            isAlive = true;

            _deltas = 0f;
        }

        public void AddAnimation(string name, int[] spritenumbers, bool looping)
        {
            _animations.Add(name, new Animation(_spritesheet, _size, spritenumbers, looping));
        }

        public Animation GetCurrentAnimation()
        {
            return _currentAnimation;
        }

        public bool Play(string name)
        {
            if (!isAlive || _paused) return false;

            if (_animations[name] == null)
                return false;

            if (_currentAnimation != _animations[name])
            {
                _currentAnimation?.Stop();
                _currentAnimation = _animations[name];
                _currentAnimation.Play();
            }
            else
            {
                if (!_currentAnimation.IsPlaying)
                {
                    _currentAnimation = _animations[name];
                    _currentAnimation.Play();
                }
            }
            return true;
        }

        public void Pause()
        {
            _paused = true;
            _currentAnimation.Pause();
        }

        public void Unpause()
        {
            _paused = false;
            _currentAnimation.Resume();
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            UpdatedEventArgs updated = args as UpdatedEventArgs;
            if (updated != null)
            {
                if (_currentAnimation.IsPlaying)
                    _currentAnimation.Update(updated.Delta);

                if (Gothit)
                {
                    _deltas += updated.Delta;
                    if (!_recover)
                    {
                        if (_deltas >= _hitTime / 2)
                        {
                            _recover = true;
                            _deltas = 0;

                        }
                        else
                        {
                            float ds = (_hitTime / 2) / updated.Delta;
                            float delta_alpha = 255 / ds;
                            _red += delta_alpha;
                            if (_red > 255) _red = 255;
                            byte rounded = (byte)Math.Round(_red);
                            _spritesheet.Color = new Color(rounded, 0, 0);
                        }
                    }
                    else
                    {
                        if (_deltas >= _hitTime / 2)
                        {
                            _recover = false;
                            Gothit = false;
                            _deltas = 0;
                            _spritesheet.Color = Color.White;
                        }
                        else
                        {
                            float ds = (_hitTime / 2) / updated.Delta;
                            float delta_alpha = 255 / ds;
                            _red -= delta_alpha;
                            if (_red < 0) _red = 0;
                            byte rounded = (byte)Math.Round(_red);
                            _spritesheet.Color = new Color(rounded, 0, 0);
                        }
                    }
                }
            }
            HandleOtherEvent(sender, args);
        }

        public abstract void HandleOtherEvent(object sender, EventArgs args);

        public void PostEvent(EventArgs args)
        {
            Parent.Notify(this, args);
        }
    }
}
