﻿using DarkForest.Components.Components.Skills;
using DarkForest.CustomEventArgs;
using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Components.Animators
{
    class ThunderVerticalAnimator : Animator
    {
        public ThunderVerticalAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("attack_up", new int[] { 5, 6, 7, 8, 9 }, false);
            AddAnimation("attack_down", new int[] { 0, 1, 2, 3, 4 }, false);
            
            Play("attack_up");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {
            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                String orientation = Parent.GetOrientation();
                if (attacked.Skill is ThunderSkill)
                    Play("attack_" + orientation);
            }
        }
    }
}
