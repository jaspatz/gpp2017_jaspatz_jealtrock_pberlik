﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;
using DarkForest.CustomEventArgs;
using DarkForest.Components.Interfaces;
using DarkForest.Components.Components.Skills;

namespace DarkForest.Components.Components.Animators
{
    class GoblinAnimator : Animator
    {
        public GoblinAnimator(Sprite spritesheet, Vector2i size) : base(spritesheet, size)
        {
            AddAnimation("walk_left", new int[] { 0, 1, 2, 3, 4, 5 }, true);
            AddAnimation("walk_right", new int[] { 11, 10, 9, 8, 7, 6 }, true);

            AddAnimation("stand_left", new int[] { 12 }, true);
            AddAnimation("stand_right", new int[] { 22 }, true);

            AddAnimation("attack_left", new int[] { 12, 13, 14, 15, 16 }, false);
            AddAnimation("attack_right", new int[] { 22, 21, 20, 19, 18 }, false);

            AddAnimation("die_left", new int[] { 24, 25, 26, 27 }, false);
            AddAnimation("die_right", new int[] { 33, 32, 31, 30 }, false);
            
            Play("stand_left");
        }

        public override void HandleOtherEvent(object sender, EventArgs args)
        {

            MovedEventArgs moved = args as MovedEventArgs;
            if (moved != null)
            {
                if (moved.Velocity != 0)
                {
                    Play("walk_" + Parent.GetOrientation());
                    _currentAnimation.Duration = 0.2f / moved.Velocity;
                }
                else
                {
                    Play("stand_" + Parent.GetOrientation());
                }
            }

            HitEventArgs hitted = args as HitEventArgs;
            if (hitted != null && hitted.Victim == Parent)
            {
                Gothit = true;
            }

            AttackedEventArgs attacked = args as AttackedEventArgs;
            if (attacked != null)
            {
                if (attacked.Skill is MeleeAttack)
                {
                    Play("attack_" + Parent.GetOrientation());
                    _currentAnimation.Duration = attacked.Skill.Duration;
                }
            }

            AttackInterruptedEventArgs interrupted = args as AttackInterruptedEventArgs;
            if(interrupted != null)
            {
                //_currentAnimation.Stop();
                if(Parent.GetOrientation() == "up" || Parent.GetOrientation() == "down")
                {
                    Parent.FacingDirection = new Vector2f(1, 0);
                }
                Play("stand_" + Parent.GetOrientation());
            }

            SpawnedEventArgs spawned = args as SpawnedEventArgs;
            if (spawned != null)
            {
                isAlive = true;
            }

            DieEventArgs died = args as DieEventArgs;
            if(died != null)
            {
                Unpause();
                Play("die_" + Parent.GetOrientation());
                _currentAnimation.Duration = .5f;
                isAlive = false;
            }
        }
    }
}
