﻿using DarkForest.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
	interface IComponent
	{
		Entity Parent { get; set; }
		void HandleEvent(object sender, EventArgs args);
		void PostEvent(EventArgs args);
	}
}
