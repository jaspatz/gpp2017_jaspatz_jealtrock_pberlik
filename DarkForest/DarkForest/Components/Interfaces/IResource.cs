﻿using DarkForest.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
    interface IResource : IComponent
    {
        event EventHandler<UseManaEventArgs> UseResource;
        event EventHandler<RefillingEventArgs> Refill;

        float MaxMana { get; set; }
        float CurrentMana { get; set; }

        bool IsRefilling { get; set; }
    }
}
