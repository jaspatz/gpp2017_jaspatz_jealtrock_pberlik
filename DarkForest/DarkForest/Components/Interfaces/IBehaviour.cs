﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
	interface IBehaviour : IComponent
	{
        float Velocity { get; set; }
        Vector2f Direction { get; set; }
        bool Active { get; set; }

        void Move();
	}
}
