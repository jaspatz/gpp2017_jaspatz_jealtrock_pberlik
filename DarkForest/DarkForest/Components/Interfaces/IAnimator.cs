﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
    interface IAnimator : IComponent
    {
        void AddAnimation(string name, int[] spritenumbers, bool looping);
        bool Play(string name);
        void Pause();
        void Unpause();
    }
}
