﻿using FarseerPhysics.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
    interface IBody : IComponent
    {
        Body Body { get; set; }
        Fixture Fixture { get; set; }
        bool Active { get; set; }
    }
}
