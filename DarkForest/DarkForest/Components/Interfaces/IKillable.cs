﻿using DarkForest.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
    interface IKillable : IComponent
    {
        event EventHandler<HitEventArgs> GotHit;
        event EventHandler<RefillingEventArgs> Refill;
        event EventHandler<DieEventArgs> Died;

        float MaxHP { get; set; }
        float CurrentHP { get; set; }
        bool IsDead { get; set; }
        bool Active { get; set; }
    }
}
