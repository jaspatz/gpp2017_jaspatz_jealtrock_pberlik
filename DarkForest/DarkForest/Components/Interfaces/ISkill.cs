﻿using DarkForest.CustomEventArgs;
using DarkForest.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
	interface ISkill : IComponent
	{
        event EventHandler<AttackedEventArgs> Used;
        event EventHandler<UpgradeSkillEventArgs> Upgraded;

		int Level { get; set; }             //Level of the skill, increasing level will make skill stronger
        float BaseDamage { get; set; }      //Base Damage
        float Damage { get; }          //Damage of the Skill
        float BaseMana { get; set; }        //Base manacost
        float Mana { get; }            //The manacost of a skill
        float ChannelAttack { get; set; }   //Time to channel the attack (before attack is issued)
        float ReleaseAttack { get; set; }   //Time the animation finishes after attack has been issued
        float Duration { get; }             //Time for the whole animation
        float BaseCooldown { get; set; }    //Base Cooldown
        float Cooldown { get; }        //How long skill can not be used
		float CooldownCounter { get; set; }	//How long till skill is not on cooldown
        KeyAction Key { get; set; }         //The ActionKey to press to use the skill

        bool Active { get; set; }

        void Use();
		void OnUse();

        //fireball: shoots a fireball at the direction the player faces, gets destroyed with 1st enemy to touch
        //thunderstrike : electrifies everything in front of the player, deals damage per tick (per second maybe). Lasts for very short time (0.5seconds)
        //freeze: freezes every creature in a radius around the player, frozen enemys cant attack you
        //Earthprotection: mass very big and velocity fast and protection from normal attacks as long as activated
    }
}
