﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Components.Interfaces
{
    interface IAudio : IComponent
    {
        void PlaySound(string filename, int volume, bool looping);
        void PauseSound();
        void StopSound();
    }
}
