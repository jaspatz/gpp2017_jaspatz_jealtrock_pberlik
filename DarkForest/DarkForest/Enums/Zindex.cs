﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Enums
{
    public enum Zindex
    {
        Player = 1,
        Projectile = 2,
        Entity = 3,
        Corpse = 4,
        Background = 10
    }
}
