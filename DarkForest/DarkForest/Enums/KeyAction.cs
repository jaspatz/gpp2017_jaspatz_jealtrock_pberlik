﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DarkForest.Enums
{
    public enum KeyAction
    {
        UseFireball = 'Q',
        UseFreeze = 'E',
        UseShield = 'R',
        UseThunder = 'W'
    }
}
