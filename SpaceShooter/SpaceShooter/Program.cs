﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter
{
    class Program
    {
        static void Main(string[] args)
        {
            MainMenu program = new MainMenu();
            program.Run();
        }
    }
}
