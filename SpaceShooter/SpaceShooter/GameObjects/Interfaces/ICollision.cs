﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter
{
    interface ICollision
    {
        void HandleCollision(Object sender, EventArgs e);
    }
}
