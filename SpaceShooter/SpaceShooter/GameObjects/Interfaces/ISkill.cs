﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects.Interfaces
{
    interface ISkill
    {
        int Level { get; set; }
        void Upgrade();
        void Downgrade();
    }
}
