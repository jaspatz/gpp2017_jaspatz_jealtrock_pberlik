﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects
{
	class Projectile : GameObject
	{
        public EventHandler ProjectileHit;
		public enum Type
		{
			Default,
			Rocket,
			Laserbeam
		}
		private bool destroyed;
		public bool IsDestroyed { get { return destroyed; } }

		public Projectile(Vector2f position, Vector2f direction, float velocity)
		{
			Width = 6;
			Height = 28;

			_sprite = new RectangleShape(new Vector2f(Width, Height));
            _sprite.Texture = new Texture("assets/Particles/laserBlue03.png");
            _sprite.Texture.Smooth = true;
            _sprite.Origin = new Vector2f(Width / 2, Height / 2);

			Position = position;
			Direction = direction;
			BaseVelocity = velocity;
			destroyed = false;

            //physics.OnProjectileHit += (projectile, entity) -> OnHit;
		}

		public RectangleShape getSprite()
		{
			return (RectangleShape)_sprite;
		}

		public void OnHit()
		{
			//Que Event Hit
			//hit shots ++
			Destroy();
		}

		public void OnMiss()
		{
			//Que Event Miss
			//missed shots ++
			Destroy();
		}

		public void Destroy()
		{
			destroyed = true;
		}
	}
}
