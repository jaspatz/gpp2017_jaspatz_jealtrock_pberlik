﻿using SFML.System;
using SpaceShooter.GameObjects.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects
{
    class GameObjectFactory
    {
        public Vector2f Origin { get; set; }
        public Vector2f Position { get; set; }
        public Vector2f Direction { get; set; }
        public Vector2f Rotation { get; set; }
        public Vector2f BaseVelocity { get; set; }
        public Vector2f Velocity { get; set; }
        public float Width { get; set; }
        public float Height { get; set; }

        private IBehaviour behaviour;
        private ICollision collision;

    }
}
