﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects.CustomEventArgs
{
    class RenderedEventArgs : EventArgs
    {
        public RenderWindow Window { get; set; }

        public RenderedEventArgs(RenderWindow window)
        {
            Window = window;
        }
    }
}
