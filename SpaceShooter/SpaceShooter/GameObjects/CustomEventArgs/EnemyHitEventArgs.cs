﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects.CustomEventArgs
{
    class EnemyHitEventArgs : EventArgs
    {
        public int Hitpoints { get; set; }

        public EnemyHitEventArgs(int hitpoints)
        {
            Hitpoints = hitpoints;
        }
    }
}
