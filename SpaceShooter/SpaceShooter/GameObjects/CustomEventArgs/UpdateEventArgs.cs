﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects.CustomEventArgs
{
    class UpdateEventArgs : EventArgs
    {
        public float Delta { get; set; }
        public UpdateEventArgs(float delta)
        {
            Delta = delta;
        }
    }
}
