﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects.CustomEventArgs
{
    class MovedEventArgs : EventArgs
    {
        public Vector2f OldPosition { get; set; }
        public Vector2f NewPosition { get; set; }
        public Vector2f Direction { get; set; }
        public float Velocity { get; set; }

        public MovedEventArgs(Vector2f oldPos, Vector2f newPos, Vector2f direction, float velocity)
        {
            OldPosition = oldPos;
            NewPosition = newPos;
            Direction = direction;
            Velocity = velocity;
        }
    }
}
