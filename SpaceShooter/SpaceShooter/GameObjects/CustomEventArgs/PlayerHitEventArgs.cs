﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects.CustomEventArgs
{
    class PlayerHitEventArgs : EventArgs
    {
        public int Hitpoints { get; set; }

        public PlayerHitEventArgs(int hitpoints)
        {
            Hitpoints = hitpoints;
        }
    }
}
