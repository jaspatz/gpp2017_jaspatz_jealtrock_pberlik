﻿using SFML.Graphics;
using SFML.System;
using SpaceShooter.GameObjects.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects
{
    class Enemy : GameObject
    {
        public EventHandler<EnemyHitEventArgs> EnemyHit;
        public EventHandler<EnemyDiedEventArgs> EnemyDied;

        private int _health;
		public bool isAlive { get; set; }

        public Enemy(Vector2f position)
        {
            Width = 62;
            Height = 56;
            _sprite = new RectangleShape(new Vector2f(Width, Height));
            _sprite.Texture = new Texture("assets/Enemys/enemyBlack1.png");
            _sprite.Texture.Smooth = true;

			isAlive = true;
            Position = position;
            Origin = new Vector2f(Width / 2, Height / 2);

            Health = 200;
            BaseVelocity = 200f;

			Updated += (sender, e) => OnUpdate(sender, e);
			Rendered += (sender, e) => OnRender(sender, e);
		}

		private void OnUpdate(object sender, UpdateEventArgs e)
		{

		}

		private void OnRender(object sender, RenderedEventArgs e)
		{
		}

		public RectangleShape getSprite()
        {
            return (RectangleShape)_sprite;
        }

        private void OnEnemyHit(int hitpoints)
        {
            if (EnemyHit != null)
                EnemyHit.Invoke(this, new EnemyHitEventArgs(hitpoints));
        }

        private void OnEnemyDied()
        {
			isAlive = false;
            if (EnemyDied != null)
                EnemyDied.Invoke(this, new EnemyDiedEventArgs());
        }

        public int Health
        {
            get
            {
                return _health;
            }
            set
            {
                int hitpoints = _health - value;
                _health = value;
                OnEnemyHit(hitpoints);
                if (_health <= 0)
                    OnEnemyDied();
            }
        }
    }
}
