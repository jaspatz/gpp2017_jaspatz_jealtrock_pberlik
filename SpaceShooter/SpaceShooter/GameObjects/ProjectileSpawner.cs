﻿using SFML.Graphics;
using SFML.System;
using SpaceShooter.GameObjects.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects
{
    class ProjectileSpawner
    {
		private Projectile.Type type;
        public List<Projectile> Projectiles;

        public Vector2f Position { get; set; }

        public ProjectileSpawner(Projectile.Type type)
        {
            this.type = type;
            Projectiles = new List<Projectile>();
		}

        public void CreateProjectile()
        {
            Projectiles.Add(new Projectile(Position, new Vector2f(0, -1), 500f));
        }

		public void Update(float delta)
		{
			foreach(Projectile projectile in Projectiles)
			{
				projectile.Update(delta);
				projectile.Move(new Vector2f(0, -1));
			}
		}

        public void Draw(RenderWindow window)
        {
            foreach (Projectile projectile in Projectiles)
            {
				if(!projectile.IsDestroyed)
					projectile.Draw(window);
            }
        }
	}
}
