﻿using SFML.Graphics;
using SFML.System;
using SpaceShooter.GameObjects.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects
{
    class GameObject
    {
        public EventHandler<MovedEventArgs> Moved;
        public EventHandler<UpdateEventArgs> Updated;
        public EventHandler<RenderedEventArgs> Rendered;

        protected Shape _sprite;
        protected float _velocity;
        private float _width;
        private float _height;

		public GameObject()
		{
		}

        public void Update(float delta)
        {
            _velocity = BaseVelocity * delta;
            OnUpdate(delta);
        }

        public void Draw(RenderWindow window)
        {
            if(!(_sprite == null))
                window.Draw(_sprite);
            OnRender(window);
        }

        public void Move(Vector2f direction)
        {
            OldDirection = Direction;
            Direction = direction;
            OldPosition = Position;
            Position = Position + _velocity * Direction;
            OnMove();
        }

        private void OnUpdate(float delta)
        {
            if (Updated != null)
                Updated.Invoke(this, new UpdateEventArgs(delta));
        }

        private void OnRender(RenderWindow window)
        {
            if (Rendered != null)
                Rendered.Invoke(this, new RenderedEventArgs(window));
        }

        private void OnMove()
        {
            if (Moved != null)
                Moved.Invoke(this, new MovedEventArgs(OldPosition, Position, Direction, BaseVelocity));
        }

        protected Vector2f OldPosition { get; set; }
        protected Vector2f OldDirection { get; set; }
        protected Vector2f Direction { get; set; }
        protected float Acceleration { get; set; }
        protected float BaseVelocity { get; set; }

        public Vector2f Position
        {
            get
            {
                if (!(_sprite == null))
                    return _sprite.Position;
                else
                    return new Vector2f(0,0);
            }
            set
            {
                if(!(_sprite == null))
                    _sprite.Position = value;
            }
        }
        protected Vector2f Origin
        {
            get
            {
                return _sprite.Origin;
            }
            set
            {
                _sprite.Origin = value;
            }
        }
        protected float Rotation
        {
            get
            {
                return _sprite.Rotation;
            }
            set
            {
                _sprite.Rotation = value;
            }
        }
        public float Width
        {
            get
            {
                return _width;
            }
            protected set
            {
                _width = value;
            }
        }
        public float Height
        {
            get
            {
                return _height;
            }
            protected set
            {
                _height = value;
            }
        }
    }
}
