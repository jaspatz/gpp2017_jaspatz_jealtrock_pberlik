﻿using SFML.Graphics;
using SFML.System;
using SpaceShooter.GameObjects.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter.GameObjects
{
    class Player : GameObject
    {
        public EventHandler<PlayerHitEventArgs> PlayerHit;
        public EventHandler<PlayerDiedEventArgs> PlayerDied;
        public EventHandler PlayerHealthRegen;

        private const int NO_DAMAGE = 0;
        private const int LIGHT_DAMAGE = 1;
        private const int MEDIUM_DAMAGE = 2;
        private const int HEAVY_DAMAGE = 3;

        private ProjectileSpawner _gun;
        private Vector2f _gunOffset;

        private RectangleShape[] _damage;
        private int _damageIntensity;
        private float _originalWidth;
        private float _originalHeight;

        private Stopwatch _clock;
        private float _cooldown;

        private int lvlHealth;
        private int lvlSpeed;
        private int lvlAttackspeed;
        private int lvlDamage;
        private int lvlSize;

        //private float speed;
        private float attackspeed;
        private float damage;
        private float size;

        private float baseSpeed;
        private float baseAttackspeed;
        private float baseDamage;
        private float baseSize;

        public Player(Vector2f position)
        {
            _originalWidth = 68;
            _originalHeight = 51;
            Width = _originalWidth;
            Height = _originalHeight;
            _sprite = new RectangleShape(new Vector2f(Width, Height));
            _sprite.Texture = new Texture("assets/Player/playerShip3_blue.png");
            _sprite.Texture.Smooth = true;
            
            _clock = new Stopwatch();
            _clock.Start();
            _cooldown = 0;

            _gun = new ProjectileSpawner(Projectile.Type.Default);
            Position = position;
            Origin = new Vector2f(Width / 2, Height / 2);
			_gun.Position = Position + _gunOffset;

			_damageIntensity = 0;
            _damage = new RectangleShape[4];
            for (int i = 0; i < _damage.Length; i++)
            {
                string url = "assets/Player/playerShip3_damage" + (i) + ".png";
                _damage[i] = new RectangleShape(new Vector2f(Width, Height));
                _damage[i].Texture = new Texture(url);
                _damage[i].Texture.Smooth = true;
                _damage[i].Position = Position;
                _damage[i].Origin = Origin;
            }
            
            baseSpeed = 500f;
            baseAttackspeed = 0.3f; //one attack per 1.1 second
            baseDamage = 50f;
            baseSize = 0.2f;
            Acceleration = 0.25f;

            HealthLevel = 10;
            SpeedLevel = 1;
            AttackspeedLevel = 1;
            DamageLevel = 1;
            SizeLevel = 5;

            Moved += (sender, e) => OnMoved(sender, e);
            Updated += (sender, e) => OnUpdate(sender, e);
            Rendered += (sender, e) => OnRender(sender, e);
        }

        public RectangleShape getSprite()
        {
            return (RectangleShape)_sprite;
        }

		public List<Projectile> getProjectiles()
		{
			return _gun.Projectiles;
		}

        private void OnUpdate(Object sender, UpdateEventArgs e)
        {
            _gun.Update(e.Delta);
        }

        private void OnMoved(Object sender, MovedEventArgs e)
        {
            _gun.Position = Position + _gunOffset;
            foreach(RectangleShape damage in _damage)
            {
                damage.Position = Position;
            }
        }

        private void OnRender(Object sender, RenderedEventArgs e)
        {
            RenderWindow window = e.Window;
            window.Draw(_damage[_damageIntensity]);
            _gun.Draw(window);
        }

        public void Shoot()
        {
            _cooldown = _clock.ElapsedMilliseconds / 1000f;
            if (_cooldown > attackspeed)
            {
                _gun.CreateProjectile();
                _clock.Restart();
            }
        }

        private void OnPlayerHealthRegen()
        {
            if (PlayerHealthRegen != null)
                PlayerHealthRegen.Invoke(this, EventArgs.Empty);
        }

        private void OnPlayerHit(int hitpoints)
        {
            if (PlayerHit != null)
                PlayerHit.Invoke(this, new PlayerHitEventArgs(hitpoints));
        }

        private void OnPlayerDead()
        {
            if (PlayerDied != null)
                PlayerDied.Invoke(this, new PlayerDiedEventArgs());
        }

        public int HealthLevel
        {
            get { return lvlHealth; }
            set
            {
                int hitpoints = lvlHealth - value;

                if (value > 10)
                    lvlHealth = 10;
                else if(value < 0)
                    lvlHealth = 0;
                else
                    lvlHealth = value;

                if (lvlHealth >= 8)
                    _damageIntensity = NO_DAMAGE;
                else if (lvlHealth >= 6)
                    _damageIntensity = LIGHT_DAMAGE;
                else if (lvlHealth >= 4)
                    _damageIntensity = MEDIUM_DAMAGE;
                else if (lvlHealth >= 2)
                    _damageIntensity = HEAVY_DAMAGE;

                if(hitpoints > 0)
                {
                    OnPlayerHit(hitpoints);
                    if (value <= 0)
                        OnPlayerDead();
                }
                else
                {
                    OnPlayerHealthRegen();
                }
            }
        }

        public int SpeedLevel
        {
            get { return lvlSpeed; }
            set
            {
                if (value < 1)
                {
                    lvlSpeed = 1;
                    return;
                }
                else if (value > 10)
                {
                    lvlSpeed = 10;
                    return;
                }
                lvlSpeed = value;
                BaseVelocity = baseSpeed + (lvlSpeed * 35);
            }
        }

        public int AttackspeedLevel
        {
            get { return lvlAttackspeed; }
            set
            {
                if (value < 1)
                {
                    lvlAttackspeed = 1;
                    return;
                }
                else if (value > 10)
                {
                    lvlAttackspeed = 10;
                    return;
                }
                lvlAttackspeed = value;
                attackspeed = baseAttackspeed - (lvlAttackspeed * 0.025f);
            }
        }

        public int DamageLevel
        {
            get { return lvlDamage; }
            set
            {
                if (value < 1)
                {
                    lvlDamage = 1;
                    return;
                }
                else if (value > 10)
                {
                    lvlDamage = 10;
                    return;
                }
                lvlDamage = value;
                damage = baseDamage + (lvlDamage * 35);
            }
        }

        public int SizeLevel
        {
            get { return lvlSize; }
            set
            {
                if (value < 1)
                {
                    lvlSize = 1;
                    return;
                }
                else if (value > 10)
                {
                    lvlSize = 10;
                    return;
                }
                lvlSize = value;
                size = baseSize * lvlSize;
                Width = _originalWidth * size;
                Height = _originalHeight * size;
                RectangleShape sprite = (RectangleShape)_sprite;
                sprite.Size = new Vector2f(Width, Height);
                Origin = new Vector2f(Width / 2, Height / 2);

                foreach(RectangleShape damage in _damage)
                {
                    damage.Size = sprite.Size;
                    damage.Origin = Origin;
                }
                _gunOffset = new Vector2f(0, - Height / 2 + size * 5);
                Console.WriteLine("");
            }
        }
    }
}
