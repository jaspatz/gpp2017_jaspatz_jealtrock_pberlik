﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using SpaceShooter.GameObjects;
using SpaceShooter.GameObjects.CustomEventArgs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter
{
    class Game
    {
        public const int FPS = 60;
        public const float TICK = 1 / (float) FPS;

		//window
        private RenderWindow window;
        private int width;
        private int height;
        private bool fullscreen;
        private ContextSettings context;
        private bool running;
        private bool paused;
        private bool started;
        private bool exit2Menu;
        private bool restart;
        private bool isGameOver;

        //Pause Menu
        private MenuItem pauseTitle;
        private Menu pauseMenu;

        //Game Over Menu
        private MenuItem gameoverTitle;
        private Menu gameoverMenu;

        //Before Start
        private MenuItem getReady;
        private MenuItem spaceForStart;

		//World
		private RectangleShape[,] background;

        //Dynmics
        private List<GameObject> gameObjects;
        private List<GameObject> enemyList;
        private Player player;

        //HUD 
        Font font;
        private RectangleShape hudBackground;
		private List<MenuItem> HUD;
		private List<MenuItem> shadow_HUD;
        private Color shadow;
        private Color shadowRed;
        MenuItem shadow_healthbar;
        private MenuItem healthbar;
        private MenuItem speedbar;
        private MenuItem atkspeedbar;
        private MenuItem dmgbar;
        private MenuItem sizebar;
        private MenuItem scorebar;
        private MenuItem highscorebar;
        private float hudHeight;

        //variables
		private int level;
		private int score = 0;
		private int highscore = 23984;
		private int enemysAlive = 3;

        public Game(uint width, uint height, bool fullscreen, ContextSettings context, int level)
        {
            if (!fullscreen)
                window = new RenderWindow(new VideoMode(width, height), "Space Shooter", Styles.Default, context);
            else
                window = new RenderWindow(new VideoMode(width, height), "Space Shooter", Styles.Fullscreen, context);

            this.fullscreen = fullscreen;
            this.context = context;
            this.width = (int) width;
            this.height = (int) height;
            this.level = level;

            gameObjects = new List<GameObject>();
            enemyList = new List<GameObject>();

            player = new Player(new Vector2f(width / 2, height - 200));
            player.PlayerHit += (sender, e) => OnPlayerHit(sender, e);
            player.PlayerDied += (sender, e) => OnPlayerDeath(sender, e);
            gameObjects.Add(player);

            Enemy enemy = new Enemy(new Vector2f(width / 2, 200));
            enemy.EnemyHit += (sender, e) => OnEnemyHit(sender, e);
            enemy.EnemyDied += (sender, e) => OnEnemyDeath(sender, e);
            gameObjects.Add(enemy);
            enemyList.Add(enemy);

			Enemy enemy2 = new Enemy(new Vector2f(width / 2 - 100, 200));
			enemy2.EnemyHit += (sender, e) => OnEnemyHit(sender, e);
			enemy2.EnemyDied += (sender, e) => OnEnemyDeath(sender, e);
			gameObjects.Add(enemy2);
			enemyList.Add(enemy2);

			Enemy enemy3 = new Enemy(new Vector2f(width / 2 + 100, 200));
			enemy3.EnemyHit += (sender, e) => OnEnemyHit(sender, e);
			enemy3.EnemyDied += (sender, e) => OnEnemyDeath(sender, e);
			gameObjects.Add(enemy3);
			enemyList.Add(enemy3);

			font = new Font("fonts/neuropol.ttf");

            shadow = new Color(40, 40, 40);
            shadowRed = new Color(120, 20, 20);

            InitializeWaitForStart();
            InitializeBackground();
			InitializeHUD();
            InitializePauseMenu();
            InitializeGameOverMenu();
        }

        private void OnPlayerHit(Object sender, PlayerHitEventArgs e)
        {
            shadow_healthbar.Blink(shadowRed, 6);
        }

        private void OnPlayerDeath(Object sender, PlayerDiedEventArgs e)
        {
            GameOver();
        }

        private void OnEnemyHit(Object sender, EnemyHitEventArgs e)
        {
            score += 25;
        }

        private void OnEnemyDeath(Object sender, EnemyDiedEventArgs e)
        {
            score += 100;
			enemysAlive--;
			if(enemysAlive <= 0)
			{
				GameOver();
			}
        }

        private void GameOver()
        {
            isGameOver = true;
            pauseMenu.Deactivate();
            gameoverMenu.Activate();
        }

        public void Run()
        {
            running = true;
            paused = false;
            exit2Menu = false;
            started = false;
            restart = false;
            isGameOver = false;
            float accumulator = 0;
            float delta = 0;
            window.Closed += (sender, evtArgs) => running = false;
            window.KeyPressed += (sender, e) => KeyPressed(sender, e);
            Stopwatch clock = new Stopwatch();
            clock.Start();

            while (running)
            {
                delta = clock.ElapsedTicks / (float)Stopwatch.Frequency;
                accumulator += delta;
                clock.Restart();
                window.DispatchEvents();

                if(accumulator > 0.2f)
                {
                    accumulator = 0.2f;
                }

                while (accumulator >= TICK)
                {
                    Update(TICK);
                    accumulator -= TICK;
                }
                Render();
            }
            if (exit2Menu)
            {
                window.Close();
                MainMenu program = new MainMenu();
                program.Run();
            }
            if (restart)
            {
                window.Close();
                Game game = new Game((uint) width, (uint) height, fullscreen, context, level);
                game.Run();
            }

        }

        private void Update(float delta)
        {
            UpdateHUD();
            MoveBackground(delta);
            if (paused || !started || isGameOver)
                return;
            
            foreach(GameObject go in gameObjects)
            {
                go.Update(delta);
            }

			foreach(Projectile projectile in player.getProjectiles())
			{
				if (!projectile.IsDestroyed)
				{
					foreach (Enemy enemy in enemyList)
					{
						if (enemy.isAlive)
						{
							if (projectile.getSprite().GetGlobalBounds().Intersects(enemy.getSprite().GetGlobalBounds()))
							{
								enemy.Health = enemy.Health - 35;
								projectile.Destroy();
							}
						}
						else
						{
							gameObjects.Remove(enemy);
						}
					}
				}
			}

            foreach(Enemy enemy in enemyList)
            {
				if (enemy.isAlive)
				{
					if (player.getSprite().GetGlobalBounds().Intersects(enemy.getSprite().GetGlobalBounds()))
					{
						enemy.Health--;
					}
				}
				else
				{
					gameObjects.Remove(enemy);
				}
            }

            CheckInput();
			
        }

		private void Render()
		{
			window.Clear(Color.Black);

			//World
			foreach (RectangleShape bg in background)
			{
				window.Draw(bg);
			}
            if (!isGameOver)
            {
                if (!paused)
                {
                    if (!started)
                    {
                        //Before Start
                        getReady.Draw(window);
                        spaceForStart.Draw(window);
                    }

                    //Dynamic Stuff
                    foreach (GameObject go in gameObjects)
                    {
                        go.Draw(window);
                    }
                }
                else
                {
                    //Pause Menu
                    pauseTitle.Draw(window);
                    pauseMenu.Draw(window);
                }

                //HUD
                window.Draw(hudBackground);
                foreach (MenuItem item in shadow_HUD)
                {
                    item.Draw(window);
                }
                foreach (MenuItem item in HUD)
                {
                    item.Draw(window);
                }
            }
            else
            {
                gameoverMenu.Draw(window);
                gameoverTitle.Draw(window);
            }

			window.Display();
		}

        private void CheckInput()
        {
            if (Keyboard.IsKeyPressed(Keyboard.Key.Up))
            {
                if (player.Position.Y - (player.Height / 2) > hudHeight)
                    player.Move(new Vector2f(0, -1));
                else
                    player.Position = new Vector2f(player.Position.X, hudHeight + (player.Height / 2));
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Right))
            {
                if (player.Position.X + (player.Width / 2) < width)
                    player.Move(new Vector2f(1f, 0));
                else
                    player.Position = new Vector2f(width - (player.Width / 2), player.Position.Y);
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Down))
            {
                if (player.Position.Y + (player.Height / 2) < height)
                    player.Move(new Vector2f(0, 1));
                else
                    player.Position = new Vector2f(player.Position.X, height - (player.Height / 2));
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Left))
            {
                if (player.Position.X - (player.Width / 2) > 0)
                    player.Move(new Vector2f(-1f, 0));
                else
                    player.Position = new Vector2f((player.Width / 2), player.Position.Y);
            }
            if (Keyboard.IsKeyPressed(Keyboard.Key.Space))
            {
                player.Shoot();
            }
        }

        private void KeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Escape:
                    if (!isGameOver)
                    {
                        paused = !paused;
                        pauseMenu.Activate();
                        gameoverMenu.Deactivate();
                    }
                    break;

                case Keyboard.Key.Space:
                    started = true;
                    break;

                case Keyboard.Key.W:
                    player.HealthLevel--;
                    player.SpeedLevel--;
                    player.AttackspeedLevel--;
                    player.DamageLevel--;
                    break;

                case Keyboard.Key.S:
                    player.HealthLevel++;
                    player.SpeedLevel++;
                    player.AttackspeedLevel++;
                    player.DamageLevel++;
                    break;

				case Keyboard.Key.D:
					player.SizeLevel++;
					break;

				case Keyboard.Key.A:
					player.SizeLevel--;
					break;
			}
        }

        private void UpdateHUD()
		{
			int health = player.HealthLevel;
			string stringHealth = "";
			for (int i = 0; i < health; i++)
				stringHealth += "|";
			healthbar.Text.DisplayedString = (stringHealth);

			int speed = player.SpeedLevel;
			string stringSpeed = "";
			for (int i = 0; i < speed; i++)
				stringSpeed += "|";
			speedbar.Text.DisplayedString = (stringSpeed);

			int atkSpeed = player.AttackspeedLevel;
			string stringAtk = "";
			for (int i = 0; i < atkSpeed; i++)
				stringAtk += "|";
			atkspeedbar.Text.DisplayedString = (stringAtk);

			int damage = player.DamageLevel;
			string stringDamage = "";
			for (int i = 0; i < damage; i++)
				stringDamage += "|";
			dmgbar.Text.DisplayedString = (stringDamage);

			int size = player.SizeLevel;
			string stringSize = "";
			for (int i = 0; i < size; i++)
				stringSize += "|";
			sizebar.Text.DisplayedString = (stringSize);
			
			scorebar.Text.DisplayedString = ("Score: " + score.ToString("D6"));
			highscorebar.Text.DisplayedString = ("Highscore: " + highscore.ToString("D6"));
		}

		private void MoveBackground(float delta)
		{
			for(int i = 0; i < background.GetLength(0); i++)
			{
				for(int j = 0; j < background.GetLength(1); j++)
				{
					Vector2f position = background[i, j].Position;
					float bgVelocity = delta * (50f + i * 80f);
					background[i, j].Position = new Vector2f(position.X, position.Y + bgVelocity);
					if (position.Y > height)
						background[i, j].Position = new Vector2f(position.X, - height);
				}
			}
		}

        private void InitializeWaitForStart()
        {
            getReady = new MenuItem("Get Ready", new Vector2f(width / 2, height / 2 - 60), font, 110);
            getReady.Text.FillColor = Menu.TITLE_COLOR;
            spaceForStart = new MenuItem("Press Space To Start", new Vector2f(width / 2, height / 2 + 20), font, 40);
        }

        private void InitializeGameOverMenu()
        {
            gameoverTitle = new MenuItem("Game Over", new Vector2f(width / 2, height / 2 - 200), font, 110);
            gameoverTitle.Text.Style = (Text.Styles.Underlined);
            gameoverTitle.Text.FillColor = Menu.TITLE_COLOR;

            gameoverMenu = new Menu(window, new Vector2f(width / 2, height / 2), font, 80);

            MenuItem.ActivateFunction startagain = () => {
                running = false;
                restart = true;
            };
            gameoverMenu.AddMenuItem("Start Again", startagain);
            MenuItem.ActivateFunction mainMenu = () =>
            {
                running = false;
                exit2Menu = true;
            };
            gameoverMenu.AddMenuItem("Main Menu", mainMenu);
            MenuItem.ActivateFunction exit = () => running = false;
            gameoverMenu.AddMenuItem("Exit", exit);

            gameoverMenu.Deactivate();
        }

        private void InitializePauseMenu()
        {
            pauseTitle = new MenuItem("Game Paused", new Vector2f(width / 2, height / 2 - 200), font, 110);
            pauseTitle.Text.Style = (Text.Styles.Underlined);
            pauseTitle.Text.FillColor = Menu.TITLE_COLOR;

            pauseMenu = new Menu(window, new Vector2f(width / 2, height / 2), font, 80);

            MenuItem.ActivateFunction resume = () => paused = false;
            pauseMenu.AddMenuItem("Resume", resume);
            MenuItem.ActivateFunction mainMenu = () =>
            {
                running = false;
                exit2Menu = true;
            };
            pauseMenu.AddMenuItem("Main Menu", mainMenu);
            MenuItem.ActivateFunction exit = () => running = false;
            pauseMenu.AddMenuItem("Exit", exit);

            pauseMenu.Deactivate();
        }

        private void InitializeBackground()
        {
            background = new RectangleShape[3, 2];

            Texture texture100 = new Texture("assets/Backgrounds/parallax100.png");
            background[0, 0] = new RectangleShape(new Vector2f(width, height));
            background[0, 0].Texture = texture100;
            background[0, 0].Texture.Smooth = true;

            background[0, 1] = new RectangleShape(new Vector2f(width, height));
            background[0, 1].Texture = texture100;
            background[0, 1].Texture.Smooth = true;
            background[0, 1].Position = new Vector2f(0, -height);

            Texture texture80 = new Texture("assets/Backgrounds/parallax80.png");
            background[1, 0] = new RectangleShape(new Vector2f(width, height));
            background[1, 0].Texture = texture80;
            background[1, 0].Texture.Smooth = true;

            background[1, 1] = new RectangleShape(new Vector2f(width, height));
            background[1, 1].Texture = texture80;
            background[1, 1].Texture.Smooth = true;
            background[1, 1].Position = new Vector2f(0, -height);

            Texture texture60 = new Texture("assets/Backgrounds/parallax60.png");
            background[2, 0] = new RectangleShape(new Vector2f(width, height));
            background[2, 0].Texture = texture60;
            background[2, 0].Texture.Smooth = true;

            background[2, 1] = new RectangleShape(new Vector2f(width, height));
            background[2, 1].Texture = texture60;
            background[2, 1].Texture.Smooth = true;
            background[2, 1].Position = new Vector2f(0, -height);
        }

		private void InitializeHUD()
		{
			HUD = new List<MenuItem>();
			shadow_HUD = new List<MenuItem>();

            hudHeight = 0.08f * height;
			hudBackground = new RectangleShape(new Vector2f(width, hudHeight));
			hudBackground.FillColor = Color.Black;
			
			uint healthfontsize = 38;
			Vector2f healthbarPosition = new Vector2f(45, 5);
            MenuItem healthbarLabel = new MenuItem("+ ", healthbarPosition, font, healthfontsize);
			healthbarLabel.HorizontalAlignment(Menu.H_Alignment.Right);
			healthbarLabel.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(healthbarLabel);
			healthbar = new MenuItem("||||||||||", healthbarPosition, font, healthfontsize);
			healthbar.HorizontalAlignment(Menu.H_Alignment.Left);
			healthbar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(healthbar);
			shadow_healthbar = new MenuItem("||||||||||", healthbarPosition, font, healthfontsize);
			shadow_healthbar.HorizontalAlignment(Menu.H_Alignment.Left);
			shadow_healthbar.VerticalAlignment(Menu.V_Alignment.Top);
			shadow_healthbar.Text.FillColor = (shadow);
            shadow_HUD.Add(shadow_healthbar);

			uint fontsize = 19;

			Vector2f speedbarPosition = new Vector2f(width/2 - 100, 5);
			MenuItem speedbarLabel = new MenuItem("Speed: ", speedbarPosition, font, fontsize);
			speedbarLabel.HorizontalAlignment(Menu.H_Alignment.Right);
			speedbarLabel.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(speedbarLabel);
			speedbar = new MenuItem("|", speedbarPosition, font, 19);
            speedbar.Text.FillColor = new Color(140, 140, 0);
			speedbar.HorizontalAlignment(Menu.H_Alignment.Left);
			speedbar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(speedbar);
			MenuItem shadow_speedbar = new MenuItem("||||||||||", speedbarPosition, font, fontsize);
			shadow_speedbar.HorizontalAlignment(Menu.H_Alignment.Left);
			shadow_speedbar.VerticalAlignment(Menu.V_Alignment.Top);
			shadow_speedbar.Text.FillColor = (shadow);
			shadow_HUD.Add(shadow_speedbar);

			Vector2f atkspeedbarPosition = new Vector2f(width/2 - 100, 25);
			MenuItem atkspeedbarLabel = new MenuItem("AttackSpeed: ", atkspeedbarPosition, font, fontsize);
			atkspeedbarLabel.HorizontalAlignment(Menu.H_Alignment.Right);
			atkspeedbarLabel.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(atkspeedbarLabel);
			atkspeedbar = new MenuItem("|", atkspeedbarPosition, font, fontsize);
            atkspeedbar.Text.FillColor = new Color(0, 80, 120);
            atkspeedbar.HorizontalAlignment(Menu.H_Alignment.Left);
			atkspeedbar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(atkspeedbar);
			MenuItem shadow_atkspeedbar = new MenuItem("||||||||||", atkspeedbarPosition, font, fontsize);
			shadow_atkspeedbar.HorizontalAlignment(Menu.H_Alignment.Left);
			shadow_atkspeedbar.VerticalAlignment(Menu.V_Alignment.Top);
			shadow_atkspeedbar.Text.FillColor = (shadow);
			shadow_HUD.Add(shadow_atkspeedbar);

			Vector2f dmgbarPosition = new Vector2f(width / 2 + 130, 5);
			MenuItem dmgbarLabel = new MenuItem("Damage: ", dmgbarPosition, font, fontsize);
			dmgbarLabel.HorizontalAlignment(Menu.H_Alignment.Right);
			dmgbarLabel.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(dmgbarLabel);
			dmgbar = new MenuItem("|", dmgbarPosition, font, fontsize);
            dmgbar.Text.FillColor = new Color(65, 130, 0);
            dmgbar.HorizontalAlignment(Menu.H_Alignment.Left);
			dmgbar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(dmgbar);
			MenuItem shadow_dmgbar = new MenuItem("||||||||||", dmgbarPosition, font, fontsize);
			shadow_dmgbar.HorizontalAlignment(Menu.H_Alignment.Left);
			shadow_dmgbar.VerticalAlignment(Menu.V_Alignment.Top);
			shadow_dmgbar.Text.FillColor = (shadow);
			shadow_HUD.Add(shadow_dmgbar);

			Vector2f sizebarPosition = new Vector2f(width / 2 + 130, 25);
			MenuItem sizebarLabel = new MenuItem("Size: ", sizebarPosition, font, fontsize);
			sizebarLabel.HorizontalAlignment(Menu.H_Alignment.Right);
			sizebarLabel.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(sizebarLabel);
			sizebar = new MenuItem("|||||", sizebarPosition, font, fontsize);
            sizebar.Text.FillColor = new Color(130, 0, 0);
            sizebar.HorizontalAlignment(Menu.H_Alignment.Left);
			sizebar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(sizebar);
			MenuItem shadow_sizebar = new MenuItem("||||||||||", sizebarPosition, font, fontsize);
			shadow_sizebar.HorizontalAlignment(Menu.H_Alignment.Left);
			shadow_sizebar.VerticalAlignment(Menu.V_Alignment.Top);
			shadow_sizebar.Text.FillColor = (shadow);
			shadow_HUD.Add(shadow_sizebar);

			scorebar = new MenuItem("Score: " + score.ToString("D6"), new Vector2f(width - 10, 5), font, fontsize);
			scorebar.HorizontalAlignment(Menu.H_Alignment.Right);
			scorebar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(scorebar);

			highscorebar = new MenuItem("Highscore: " + highscore.ToString("D6"), new Vector2f(width - 10, 25), font, fontsize);
			highscorebar.HorizontalAlignment(Menu.H_Alignment.Right);
			highscorebar.VerticalAlignment(Menu.V_Alignment.Top);
			HUD.Add(highscorebar);
		}

	}
}
