﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.Window;
using System.Diagnostics;
using SFML.System;

namespace SpaceShooter
{
    class MainMenu
    {
        private uint width;
        private uint height;
        private bool running;
		private bool started;
        private RenderWindow window;
        private string title;
        private ContextSettings context;
        private bool fullscreen;

        private RectangleShape logo;
        private Font font;
        private MenuItem logoOptions;
        private RectangleShape[] background;
        private Menu menu;
        private Menu optionsMenu;
        private MenuItem credits;
        private bool optionsView;

        private int level;
        private uint[,] dimensions;
        private int dimensionIndex;

        public MainMenu()
        {
            dimensionIndex = 0;
            dimensions = new uint[3, 2];

            dimensions[0, 0] = 1280;
            dimensions[0, 1] = 720;

            dimensions[1, 0] = 1600;
            dimensions[1, 1] = 900;

            dimensions[2, 0] = 1920;
            dimensions[2, 1] = 1080;

            width = dimensions[dimensionIndex, 0];
            height = dimensions[dimensionIndex, 1];

			background = new RectangleShape[2];

			Texture texture = new Texture("assets/Backgrounds/bg.png");
			background[0] = new RectangleShape(new Vector2f(width, height));
			background[0].Texture = new Texture(texture);
			background[0].Texture.Smooth = true;

			background[1] = new RectangleShape(new Vector2f(width, height));
			background[1].Texture = new Texture(texture);
			background[1].Texture.Smooth = true;
			background[1].Position = new Vector2f(- width, 0);

			title = "Space Shooter - Main Menu";
            context = new ContextSettings(24, 8, 8);
            fullscreen = false;

            window = new RenderWindow(new VideoMode(width, height), title, Styles.Default, context);
            window.SetKeyRepeatEnabled(false);

            level = 0;
			started = false;

            optionsView = false;
            font = new Font("fonts/neuropol.ttf");
            menu = new Menu(window, new Vector2f(width / 2, height / 2), font, 80);

            logo = new RectangleShape(new Vector2f(0.75f * width, 0.75f * height));
            logo.Texture = new Texture("assets/Logos/logo.png");
            logo.Position = new Vector2f(width / 2 - logo.GetGlobalBounds().Width / 2, -0.1f * height);
            logo.Texture.Smooth = true;

            MenuItem.ActivateFunction start = () => StartGame();
            menu.AddMenuItem("Start Game", start);
            MenuItem.ActivateFunction options = () => OpenOptionsView();
            menu.AddMenuItem("Options", options);
            MenuItem.ActivateFunction exit = () => running = false;
            menu.AddMenuItem("Exit", exit);

            credits = new MenuItem("Copyright (c) - Jens Altrock, Pascal Berlik, Janusz Spatz", new Vector2f(5, height - 5), font, 15);
            credits.VerticalAlignment(Menu.V_Alignment.Bottom);
            credits.HorizontalAlignment(Menu.H_Alignment.Left);

            optionsMenu = new Menu(window, new Vector2f(width / 2, height / 2), font, 40);

            logoOptions = new MenuItem("Options", new Vector2f(width / 2, height / 2 - 200), font, 110);
            logoOptions.Text.Style = Text.Styles.Underlined;
            logoOptions.Text.FillColor = Menu.TITLE_COLOR;

            MenuItem.ActivateFunction empty = () => { };
            optionsMenu.AddMenuItem("Resolution:", empty);
            MenuItem.ActivateFunction changeResolution = () => ChangeResolution();
            optionsMenu.AddMenuItem("<- " + width + " x " + height + " ->", changeResolution);
            optionsMenu.AddMenuItem("Fullscreen:", empty);
            MenuItem.ActivateFunction changeFullscreen = () => ChangeFullscreen();
            optionsMenu.AddMenuItem("<- No ->", changeFullscreen);
            optionsMenu.AddMenuItem("Level:", empty);
            MenuItem.ActivateFunction changeLevel = () => ChangeLevel();
            optionsMenu.AddMenuItem("<- " + level + " ->", changeLevel);
            MenuItem.ActivateFunction _return = () => OpenMainMenuView();
            optionsMenu.AddMenuItem("Return", _return);
            
            OpenMainMenuView();
        }

        private void StartGame()
        {
            running = false;
			started = true;
        }

        private void OpenMainMenuView()
        {
            title = "Space Shooter - Main Menu";
            window.SetTitle(title);

            optionsView = false;

            optionsMenu.Deactivate();
            menu.Activate();
        }

        private void OpenOptionsView()
        {
            title = "Space Shooter - Options";
            window.SetTitle(title);

            optionsView = true;

            optionsMenu.Activate();
            menu.Deactivate();
        }

        public void Run()
        {
            running = true;
            window.Closed += (sender, evtArgs) => running = false;
            Stopwatch clock = new Stopwatch();
            clock.Start();

            while (running)
            {
                float delta = clock.ElapsedTicks / (float) Stopwatch.Frequency;
                clock.Restart();
				Update(delta);
                Render();
                window.DispatchEvents();
            }
			if (started)
			{
				window.Close();
				Game game = new Game(width, height, fullscreen, context, 0);
				game.Run();
			}
        }

        private void Update(float delta)
		{
			for (int i = 0; i < background.Length; i++)
			{
				Vector2f position = background[i].Position;
				float bgVelocity = 50f * delta;
				background[i].Position = new Vector2f(position.X + bgVelocity, position.Y);
				if (position.X > width)
					background[i].Position = new Vector2f(- width, position.Y);
			}
		}

        private void Render()
        {
            window.Clear();

			for (int i = 0; i < background.Length; i++)
			{
				window.Draw(background[i]);
			}

            if (!optionsView)
            {
                window.Draw(logo);
                menu.Draw(window);
            }
            else
            {
                logoOptions.Draw(window);
                optionsMenu.Draw(window);
            }

            credits.Draw(window);

            window.Display();
        }

        private void ChangeResolution()
        {
            dimensionIndex++;
            if (dimensionIndex >= dimensions.GetLength(0))
            {
                dimensionIndex = 0;
            }
            width = dimensions[dimensionIndex, 0];
            height = dimensions[dimensionIndex, 1];
            optionsMenu.MenuItemAt(1).Text.DisplayedString = ("<- " + width + " x " + height + " ->");
        }

        private void ChangeFullscreen()
        {
            if (!fullscreen)
            {
                fullscreen = true;
                optionsMenu.MenuItemAt(3).Text.DisplayedString = ("<- " + "Yes" + " ->");
            }
            else
            {
                optionsMenu.MenuItemAt(3).Text.DisplayedString = ("<- " + "No" + " ->");
                fullscreen = false;
            }
        }

        private void ChangeLevel()
        {
            level++;
            if(level == 10)
            {
                level = 0;
            }
            optionsMenu.MenuItemAt(5).Text.DisplayedString = ("<- " + level + " ->");
        }

    }
}
