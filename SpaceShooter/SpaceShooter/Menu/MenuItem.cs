﻿using SFML.Graphics;
using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpaceShooter
{
    class MenuItem
    {

        public delegate void ActivateFunction();
        
        public Text Text { get; set; }
        private Vector2f position;
        private Vector2f origin;
        private ActivateFunction function;
        private bool _blinking;
        private Color _color;
        private Timer _timer;
        private int _amount;
        private int _maxAmount;
		public bool Selectable { get; set; }

        public MenuItem(String label, Vector2f position, Font font, uint size)
        {
            Text = new Text(label, font, size);
            Text.Origin = new Vector2f(Text.GetGlobalBounds().Width / 2, Text.GetGlobalBounds().Height / 2);
            origin = Text.Origin;
            Text.Position = position;
            this.position = position;
            SetInactive();
            _timer = new Timer(BlinkOn);
			Selectable = true;
        }

        public void Blink(Color color, int amount)
        {
            _timer.Dispose();
            _color = Menu.SHADOW_COLOR;
            _amount = 0;
            _maxAmount = amount;
            _blinking = false;
            _timer = new Timer(new TimerCallback(BlinkOn), color, 0, 300);
        }

        public void BlinkOn(Object color)
        {
            if (!_blinking)
            {
                Text.FillColor = (Color)color;
            }
            else
            {
                Text.FillColor = _color;
            }
            _blinking = !_blinking;
            _amount++;
            if (_amount >= _maxAmount)
            {
                _timer.Dispose();
                Text.FillColor = _color;
            }
        }

        public void SetActive()
        {
            Text.FillColor = Menu.ACTIVE_COLOR;
            Text.Style = Text.Styles.Bold;
            Text.Origin = new Vector2f(Text.GetGlobalBounds().Width / 2, Text.GetGlobalBounds().Height / 2);
        }

        public void SetInactive()
        {
            Text.FillColor = Menu.INACTIVE_COLOR;
            Text.Style = Text.Styles.Regular;
            Text.Origin = new Vector2f(Text.GetGlobalBounds().Width / 2, Text.GetGlobalBounds().Height / 2);
        }

        public void SetActivateFunction(ActivateFunction del)
        {
            function = del;
        }

        public void VerticalAlignment(Menu.V_Alignment arg)
        {
            switch (arg)
            {
                case Menu.V_Alignment.Top:
                    origin = new Vector2f(Text.Origin.X, 0);
                    break;
                case Menu.V_Alignment.Center:
                    origin = new Vector2f(Text.Origin.X, Text.GetGlobalBounds().Height / 2);
                    break;
                case Menu.V_Alignment.Bottom:
                    origin = new Vector2f(Text.Origin.X, Text.GetGlobalBounds().Height);
                    break;
            }
            Text.Origin = origin;
        }

        public void HorizontalAlignment(Menu.H_Alignment arg)
        {
            switch (arg)
            {
                case Menu.H_Alignment.Left:
                    origin = new Vector2f(0, Text.Origin.Y);
                    break;
                case Menu.H_Alignment.Center:
                    origin = new Vector2f(Text.GetGlobalBounds().Width / 2, Text.Origin.Y);
                    break;
                case Menu.H_Alignment.Right:
                    origin = new Vector2f(Text.GetGlobalBounds().Width, Text.Origin.Y);
                    break;
            }
            Text.Origin = origin;
        }

        public void Activate()
        {
            function();
        }

        public void Draw(RenderWindow window)
        {
            window.Draw(Text);
        }
    }
}
