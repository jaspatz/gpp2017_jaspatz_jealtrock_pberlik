﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter
{
    class Menu
    {
        public static readonly Color TITLE_COLOR = new Color(128, 128, 128);
        public static readonly Color ACTIVE_COLOR = new Color(50, 50, 255);
        public static readonly Color INACTIVE_COLOR = Color.White;	//new Color(200, 200, 200);
		public static readonly Color UNSELECTABLE_COLOR = new Color(200, 200, 200);
        public static readonly Color SHADOW_COLOR = new Color(40, 40, 40);

        public enum V_Alignment
        {
            Top,
            Center,
            Bottom
        }
        public enum H_Alignment
        {
            Left,
            Center,
            Right
        }

        private int width;
        private int height;
        private RenderWindow window;

        private Font font;
        private uint size;
        private Vector2f position;
        private List<MenuItem> items;
		private int selected_index;
        private int Selected_Index {
			get
			{
				return selected_index;
			}
			set
			{
				if(value >= 0 && value <= items.Count - 1)
				{
					if(items.ElementAt(selected_index).Selectable)
					{
						selected_index = value;
						SetActive(items.ElementAt(selected_index));
					}
				}
			}
		}
        private bool activated;

        public Menu(RenderWindow window, Vector2f position, Font font, uint size)
        {
            this.window = window;
            window.KeyPressed += KeyPressed;
            width = (int) window.Size.X;
            height = (int) window.Size.Y;

            activated = true;
            this.position = position;
            items = new List<MenuItem>();
            Selected_Index = 0;
            this.font = font;
            this.size = size;
        }

        public void AddMenuItem(String label, MenuItem.ActivateFunction function)
        {
            Vector2f position = new Vector2f(this.position.X, this.position.Y + (items.Count * size));
            MenuItem item = new MenuItem(label, position, font, size);
            item.SetActivateFunction(function);
            items.Add(item);
            if(items.Count == 1)
            {
                SetActive(item);
            }
        }

        public void Activate()
        {
            if (!activated)
            {
                window.KeyPressed += KeyPressed;
                activated = true;
            }
        }

        public void Deactivate()
        {
            if (activated)
            {
                window.KeyPressed -= KeyPressed;
                activated = false;
            }
        }

        public MenuItem MenuItemAt(int index)
        {
            return items.ElementAt(index);
        }

        private void KeyPressed(object sender, KeyEventArgs e)
        {
            switch (e.Code)
            {
                case Keyboard.Key.Up:
					Selected_Index--;
                    break;

                case Keyboard.Key.Down:
					Selected_Index++;
                    break;

                case Keyboard.Key.Return:
                    items.ElementAt(Selected_Index).Activate();
                    break;
            }
        }

        private void SetActive(MenuItem menu_item)
        {
            foreach (MenuItem item in items)
            {
                item.SetInactive();
            }

            menu_item.SetActive();
        }

        public void Draw(RenderWindow window)
        {
            foreach (MenuItem item in items)
            {
                item.Draw(window);
            }
        }
    }
}
