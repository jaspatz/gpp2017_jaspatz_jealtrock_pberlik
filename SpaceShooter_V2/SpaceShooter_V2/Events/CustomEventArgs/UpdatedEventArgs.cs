﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Events.CustomEventArgs
{
    class UpdatedEventArgs : EventArgs
    {
        public float Delta { get; private set; }
        public UpdatedEventArgs(float delta)
        {
            Delta = delta;
        }
    }
}
