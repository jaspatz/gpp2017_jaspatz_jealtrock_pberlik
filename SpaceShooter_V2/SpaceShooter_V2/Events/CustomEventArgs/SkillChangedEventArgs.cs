﻿using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Events.CustomEventArgs
{
    class SkillChangedEventArgs : EventArgs
    {
        public enum Type
        {
            UPGRADE,
            DOWNGRADE
        };

        public int Level { get; private set; }
        public Type EventType { get; private set; }
        public ISkill Skill { get; private set; }
        public int Arg0 { get; private set; }

        public SkillChangedEventArgs(ISkill skill, Type type, int level, int arg0 = 0)
        {
            EventType = type;
            Level = level;
            Skill = skill;
            Arg0 = arg0;
        }
    }
}
