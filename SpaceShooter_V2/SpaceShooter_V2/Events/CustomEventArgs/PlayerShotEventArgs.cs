﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Events.CustomEventArgs
{
    class PlayerShotEventArgs : EventArgs
    {
        public int Damage { get; private set; }
        public PlayerShotEventArgs(int damage)
        {
            Damage = damage;
        }
    }
}
