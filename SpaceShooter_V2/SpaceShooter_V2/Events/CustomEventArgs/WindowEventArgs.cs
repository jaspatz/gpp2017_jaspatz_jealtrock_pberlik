﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Events.CustomEventArgs
{
	class WindowEventArgs : EventArgs
	{
		public enum Type
		{
			CLOSED,
			CREATED,
			RESIZED,
		};

        public RenderWindow Window { get; private set; }
		public Type EventType { get; private set; }
		public int Width { get; private set; }
		public int Height { get; private set; }

		public WindowEventArgs(RenderWindow window, Type type, int width = 0, int height = 0)
		{
            Window = window;
			EventType = type;
			Width = width;
			Height = height;
		}
	}
}
