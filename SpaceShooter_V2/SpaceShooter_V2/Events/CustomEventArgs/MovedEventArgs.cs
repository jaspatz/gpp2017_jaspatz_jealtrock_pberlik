﻿using SFML.System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Events.CustomEventArgs
{
    class MovedEventArgs : EventArgs
    {
        public Vector2f OldPosition { get; private set; }
        public Vector2f NewPosition { get; private set; }

        public MovedEventArgs(Vector2f oldpos, Vector2f newpos)
        {
            OldPosition = oldpos;
            NewPosition = newpos;
        }
    }
}
