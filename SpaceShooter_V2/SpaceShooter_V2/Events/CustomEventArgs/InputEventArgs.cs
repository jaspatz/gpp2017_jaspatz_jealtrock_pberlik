﻿using SFML.System;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Events.CustomEventArgs
{
    class InputEventArgs : EventArgs
    {
        public List<Keyboard.Key> KeysPressed { get; private set; }
        
        public InputEventArgs(Keyboard.Key key)
        {
            KeysPressed = new List<Keyboard.Key>();
            KeysPressed.Add(key);
        }

        public InputEventArgs(List<Keyboard.Key> keysPressed)
        {
            KeysPressed = keysPressed;
        }
    }
}
