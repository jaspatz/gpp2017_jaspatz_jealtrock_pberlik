﻿using SFML.Graphics;
using SFML.System;
using SpaceShooter_V2.Events.CustomEventArgs;
using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
    class GameObject
    {
        public Vector2f Position { get { return _position; } set { Vector2f oldpos = _position; _position = value; Notify(this, new MovedEventArgs(oldpos, _position)); } }
        public float Rotation { get { return _rotation; } set { _rotation = value; Notify(this, new RotatedEventArgs(value - _rotation, _rotation)); } }
        public Vector2f Scale { get { return _scale; } set { _scale = value; Notify(this, new ScaledEventArgs(_scale)); } }
        public Vector2f Direction { get; set; }
        public float BaseVelocity { get; set; }
        public float Velocity { get; private set; }
        
        private Vector2f _position;
        private Vector2f _scale;
        private float _rotation;

        private List<IComponent> _components;

        public GameObject()
        {
            InitializeGameObject();
        }

        public GameObject(Vector2f position, float rotation = 0f)
        {
            InitializeGameObject();
            Position = position;
            Rotation = rotation;
        }

        private void InitializeGameObject()
        {
            _components = new List<IComponent>();
            Position = new Vector2f();
            Direction = new Vector2f();
            Scale = new Vector2f(1, 1);
            Rotation = 0f;
            BaseVelocity = 0f;
            Velocity = 0f;
        }

        public void Update(float delta)
        {
            Velocity = delta * BaseVelocity;
            Position = Position + Velocity * Direction;
            Notify(this, new UpdatedEventArgs(delta));

            Direction = new Vector2f(0, 0);
        }

        public void RotateBy(float degrees)
        {
            Rotation += degrees;
        }

        public void Notify(object sender, EventArgs args)
        {
            foreach(IComponent c in _components)
            {
                c.HandleEvent(this, args);
            }
        }

        public T GetComponent<T>() where T : IComponent
        {
			foreach(IComponent c in _components)
			{
				if (c is T)
				{
					return (T) c;
				}
			}
			
            return default(T);
        }

        public void AttachComponent(IComponent component)
        {
            _components.Add(component);
        }

        public void DetachComponent(IComponent component)
        {
            _components.Remove(component);
        }
    }
}
