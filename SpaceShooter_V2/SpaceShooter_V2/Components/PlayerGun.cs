﻿using SpaceShooter_V2.Components.Skills;
using SpaceShooter_V2.Events.CustomEventArgs;
using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Components
{
    class PlayerGun : IWeapon
    {
        public EventHandler<PlayerShotEventArgs> PlayerShot;
		
        private int _damage;
		private float _attackspeed;

        public PlayerGun()
        {
            _damage = 30;
			_attackspeed = 1f;
        }

        public void Shoot()
        {
			Console.WriteLine("player shot with normal gun");
			PlayerShot?.Invoke(this, new PlayerShotEventArgs(_damage));
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            SkillChangedEventArgs skillargs = args as SkillChangedEventArgs;
            if(skillargs != null)
            {
                if(skillargs.Skill is DamageSkill)
                {
					_damage = skillargs.Arg0;
                }
            }
        }

        public void PostEvent(EventArgs args)
        {
        }
    }
}
