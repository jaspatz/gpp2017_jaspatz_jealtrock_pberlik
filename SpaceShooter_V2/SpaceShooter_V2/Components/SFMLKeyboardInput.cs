﻿using SFML.Graphics;
using SFML.System;
using SFML.Window;
using SpaceShooter_V2.Events.CustomEventArgs;
using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
    class SFMLKeyboardInput : IInput
	{
		public const Keyboard.Key KEY_UP = Keyboard.Key.Up;
		public const Keyboard.Key KEY_DOWN = Keyboard.Key.Down;
		public const Keyboard.Key KEY_RIGHT = Keyboard.Key.Right;
		public const Keyboard.Key KEY_LEFT = Keyboard.Key.Left;
		public const Keyboard.Key KEY_ATTACK = Keyboard.Key.Space;
		public const Keyboard.Key KEY_ACTION = Keyboard.Key.Return;
		public const Keyboard.Key KEY_PAUSE = Keyboard.Key.Escape;

		public void ProcessInput()
		{
            List<Keyboard.Key> keysPressed = new List<Keyboard.Key>();

			if (Keyboard.IsKeyPressed(KEY_UP) && !Keyboard.IsKeyPressed(KEY_DOWN))
            {
                keysPressed.Add(KEY_UP);
            }
			if (Keyboard.IsKeyPressed(KEY_RIGHT) && !Keyboard.IsKeyPressed(KEY_LEFT))
            {
                keysPressed.Add(KEY_RIGHT);
            }
			if (Keyboard.IsKeyPressed(KEY_DOWN) && !Keyboard.IsKeyPressed(KEY_UP))
            {
                keysPressed.Add(KEY_DOWN);
            }
			if (Keyboard.IsKeyPressed(KEY_LEFT) && !Keyboard.IsKeyPressed(KEY_RIGHT))
            {
                keysPressed.Add(KEY_LEFT);
            }
			if (Keyboard.IsKeyPressed(KEY_ATTACK))
            {
                keysPressed.Add(KEY_ATTACK);
            }

            if(keysPressed.Count > 0)
            {
                PostEvent(new InputEventArgs(keysPressed));
            }
		}

        public void HandleEvent(object sender, EventArgs args)
        {
            KeyEventArgs keyargs = args as KeyEventArgs;
            if (keyargs != null)
            {
                switch (keyargs.Code)
                {
                    case KEY_PAUSE:
                        PostEvent(new InputEventArgs(KEY_PAUSE));
                        Console.WriteLine("(Un-)Pause Game");
                        break;
                    case KEY_ACTION:
                        PostEvent(new InputEventArgs(KEY_ACTION));
                        Console.WriteLine("Action Key");
                        break;
                }
            }
		}

        public void PostEvent(EventArgs args)
        {
            EventBus.Notify(this, args);
        }
    }
}
