﻿using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
	class SpaceWorld : IGameWorld
	{
		public void Update(float delta, List<GameObject> gameObjects)
		{
            foreach(GameObject go in gameObjects)
            {
                go.Update(delta);
            }
		}

		public void HandleEvent(object sender, EventArgs args)
		{

		}

		public void PostEvent(EventArgs args)
		{

		}
	}
}
