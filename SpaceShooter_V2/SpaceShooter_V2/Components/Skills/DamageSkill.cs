﻿using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Components.Skills
{
    class DamageSkill : ISkill
    {
        public int Level { get; set; }

        public void Downgrade()
        {
        }

        public void Upgrade()
        {
        }

        public void HandleEvent(object sender, EventArgs args)
        {
        }

        public void PostEvent(EventArgs args)
        {
        }
    }
}
