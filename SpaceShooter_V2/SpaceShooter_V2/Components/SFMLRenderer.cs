﻿using SFML.Graphics;
using SFML.Window;
using SpaceShooter_V2.Events.CustomEventArgs;
using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
    class SFMLRenderer : IRenderer
    {
		private RenderWindow _window;

        public SFMLRenderer(int width, int height, bool fullscreen, string title)
        {
            VideoMode videomode = new VideoMode((uint)width, (uint)height);
            ContextSettings context = new ContextSettings(24, 8, 8);
			Styles style = fullscreen ? Styles.Fullscreen : Styles.Default;
            _window = new RenderWindow(videomode, title, style, context);
			_window.Closed += (sender, e) => OnClose(sender, e);
            _window.KeyPressed += (sender, e) => PostEvent(e);
			PostEvent(new WindowEventArgs(_window, WindowEventArgs.Type.CREATED, width, height));
        }

		private void OnClose(object sender, EventArgs e)
        {
            _window.KeyPressed -= (sender2, e2) => PostEvent(e);
            _window.Close();
            PostEvent(new WindowEventArgs(_window, WindowEventArgs.Type.CLOSED));
		}

		public void DispatchEvents()
        {
            _window.DispatchEvents();
        }

        public void HandleEvent(object sender, EventArgs args)
        {

        }

        public void PostEvent(EventArgs args)
        {
			EventBus.Notify(this, args);
		}

		public void Draw(List<GameObject> objects)
		{
			_window.Clear();

			foreach (GameObject go in objects)
			{
				IShape shape_component = go.GetComponent<IShape>();
				if (shape_component != null)
				{
					_window.Draw(shape_component.Shape);
				}
			}

			_window.Display();
		}
	}
}
