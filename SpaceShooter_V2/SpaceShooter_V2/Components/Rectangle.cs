﻿using SFML.Graphics;
using SFML.System;
using SpaceShooter_V2.Events.CustomEventArgs;
using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Components
{
    class Rectangle : IShape
    {
        public Shape Shape { get { return _shape; } set { _shape = (RectangleShape)value; } }
        public float Width { get; set; }
        public float Height { get; set; }

        private RectangleShape _shape;

        public Rectangle(float width, float height)
        {
            CreateRectangle(width, height);
            _shape.FillColor = Color.White;
        }

        public Rectangle(float width, float height, string url)
        {
            CreateRectangle(width, height);
            _shape.Texture = new Texture(url);
            _shape.Texture.Smooth = true;
        }

        private void CreateRectangle(float width, float height)
        {
            Width = width;
            Height = height;
            _shape = new RectangleShape(new Vector2f(width, height));
            _shape.Origin = new Vector2f(width/2, height/2);
        }

        public void SetTexture(string url)
        {
            _shape.Texture = new Texture(url);
            _shape.Texture.Smooth = true;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            MovedEventArgs movedargs = args as MovedEventArgs;
            if (movedargs != null)
            {
                _shape.Position = movedargs.NewPosition;
            }

            RotatedEventArgs rotargs = args as RotatedEventArgs;
            if (rotargs != null)
            {
                _shape.Rotation = rotargs.Rotation;
            }

            ScaledEventArgs scaledargs = args as ScaledEventArgs;
            if (scaledargs != null)
            {
                _shape.Scale = scaledargs.Scale;
            }
        }

        public void PostEvent(EventArgs args)
        {
        }
    }
}
