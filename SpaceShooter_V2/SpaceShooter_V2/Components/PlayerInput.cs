﻿using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SpaceShooter_V2.Events.CustomEventArgs;
using SFML.Window;

namespace SpaceShooter_V2.Components
{
    class PlayerInput : IInputHandler
    {

        private GameObject _player;

        public void RegisterHandler(GameObject go)
        {
            _player = go;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
            InputEventArgs inputargs = args as InputEventArgs;
            if (inputargs != null)
            {
                Vector2f direction = new Vector2f(0, 0);
                foreach (Keyboard.Key key in inputargs.KeysPressed)
                {
                    switch (key)
                    {
                        case SFMLKeyboardInput.KEY_UP:
                            direction.Y = -1f;
                            break;
                        case SFMLKeyboardInput.KEY_DOWN:
                            direction.Y = 1f;
                            break;
                        case SFMLKeyboardInput.KEY_RIGHT:
                            direction.X = 1f;
                            break;
                        case SFMLKeyboardInput.KEY_LEFT:
                            direction.X = -1f;
                            break;
                        case SFMLKeyboardInput.KEY_ATTACK:
                            _player.GetComponent<IWeapon>().Shoot();
                            break;
                    }
                }
                _player.Direction = direction;
            }
        }

        public void PostEvent(EventArgs args)
        {
            _player.Notify(this, args);
        }
    }
}
