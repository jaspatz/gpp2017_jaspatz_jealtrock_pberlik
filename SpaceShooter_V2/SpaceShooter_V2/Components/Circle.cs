﻿using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.Graphics;
using SFML.System;

namespace SpaceShooter_V2.Components
{
    class Circle : IShape
    {
        public Shape Shape { get { return _shape; } set { _shape = (CircleShape) value; } }
        public float Radius { get; set; }

        private CircleShape _shape;
        private GameObject _go;

        public Circle(float r)
        {
            CreateCircle(r);
            _shape.FillColor = Color.White;
        }

        public Circle(float r, string url)
        {
            CreateCircle(r);
            _shape.Texture = new Texture(url);
            _shape.Texture.Smooth = true;
        }

        private void CreateCircle(float r)
        {
            Radius = r;
            _shape = new CircleShape(r);
            _shape.Origin = new Vector2f(r, r);
            _shape.Position = _go.Position;
            _shape.Rotation = _go.Rotation;
            _shape.Scale = _go.Scale;
        }

        public void SetTexture(string url)
        {
            _shape.Texture = new Texture(url);
            _shape.Texture.Smooth = true;
        }

        public void HandleEvent(object sender, EventArgs args)
        {
        }

        public void PostEvent(EventArgs args)
        {
        }
    }
}
