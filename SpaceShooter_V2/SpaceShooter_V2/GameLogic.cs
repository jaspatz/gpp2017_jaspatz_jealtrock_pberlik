﻿using SpaceShooter_V2.Components;
using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
    class GameLogic
    {
        public const int FPS = 60;
        public const float TICK = 1 / (float)FPS;

		private List<GameObject> _gameobjects;
		
        private IInput input;
		private IGameWorld world;
		private IAudio audio;
		private IRenderer renderer;
        private IInputHandler playerInput;

        public GameLogic()
        {
			_gameobjects = new List<GameObject>();

            input = new SFMLKeyboardInput();
			EventBus.Attach(input);
            playerInput = new PlayerInput();
            EventBus.Attach(playerInput);
			world = new SpaceWorld();
			EventBus.Attach(world);
			audio = new SFMLAudio();
			EventBus.Attach(audio);
			renderer = new SFMLRenderer(500, 500, false, "SFML Engine Test");
			EventBus.Attach(renderer);

            GameObject testobject = new GameObject(new SFML.System.Vector2f(250, 250));
            testobject.BaseVelocity = 150f;
            testobject.AttachComponent(new Rectangle(68, 51));
            testobject.AttachComponent(new PlayerGun());
            _gameobjects.Add(testobject);
            
            playerInput.RegisterHandler(testobject);
        }

        public void Run()
        {
            float accumulator = 0;
            float delta = 0;
            Stopwatch clock = new Stopwatch();
            clock.Start();

            while (true)
            {
                delta = clock.ElapsedTicks / (float)Stopwatch.Frequency;
                accumulator += delta;
                clock.Restart();

                if (accumulator > 0.2f)
                    accumulator = 0.2f;

                while (accumulator >= TICK)
                {
					renderer.DispatchEvents();
                    Update(TICK);
                    accumulator -= TICK;
                }
                Render();
            }
        }

        public void Update(float delta)
        {

			//COLLISION GROUPS: const int PLAYER = 0;
			// const int ENEMY = 1;

            input.ProcessInput();
            foreach(GameObject go in _gameobjects)
            {
                go.Update(delta);
            }

            

        }

        public void Render()
        {
			renderer.Draw(_gameobjects);
        }

    }
}
