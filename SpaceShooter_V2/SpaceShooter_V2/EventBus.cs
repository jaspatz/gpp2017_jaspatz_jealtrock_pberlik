﻿using SpaceShooter_V2.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
    static class EventBus
    {
        private static List<IComponent> _components = new List<IComponent>();

		public static void Attach(IComponent component)
        {
            _components.Add(component);
        }

		public static void Detach(IComponent component)
        {
            _components.Remove(component);
        }

        public static void Notify(object sender, EventArgs args)
        {
            foreach(IComponent component in _components)
            {
				component.HandleEvent(sender, args);
            }
        }
    }
}
