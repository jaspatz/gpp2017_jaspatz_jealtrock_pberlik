﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2
{
    class LevelManager
    {
        public static int Level
		{
			get
			{
				return _level;
			}
			private set
			{
				_level = value;
			}
		}
		private static int _level = 0;
        
        public Level NextLevel()
        {
            Level++;
            return new Level("Levels/level1.xml", Level);
        }

    }
}
