﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Interfaces
{
    interface IRenderer : IComponent
    {
        void DispatchEvents();
        void Draw(List<GameObject> objects);
    }
}
