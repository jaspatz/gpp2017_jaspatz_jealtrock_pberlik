﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Interfaces
{
    interface IComponent
    {
		GameObject Parent { get; set; }
        void HandleEvent(object sender, EventArgs args);
        void PostEvent(EventArgs args);
    }
}
