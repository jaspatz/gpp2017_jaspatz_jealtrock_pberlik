﻿using SFML.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Interfaces
{
	interface IShape : IComponent
	{
		Shape Shape { get; set; }

        void SetTexture(string url);
    }
}
