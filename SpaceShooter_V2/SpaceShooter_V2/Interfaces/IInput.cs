﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpaceShooter_V2.Interfaces
{
    interface IInput : IComponent
    {
		void ProcessInput();
        /*void NotifyUp();
        void NotifyDown();
        void NotifyLeft();
        void NotifyRight();
        void NotifyShoot();
        void NotifyPauseGame();*/
    }
}
