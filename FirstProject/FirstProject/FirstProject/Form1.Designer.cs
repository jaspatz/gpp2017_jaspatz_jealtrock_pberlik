﻿using System;

namespace FirstProject
{
    partial class ConfigWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
                Console.Write("dispose");
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Singleplayer = new System.Windows.Forms.Button();
            this.Multiplayer = new System.Windows.Forms.Button();
            this.Resolution_List = new System.Windows.Forms.ComboBox();
            this.label_Resolution = new System.Windows.Forms.Label();
            this.Fullscreen = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // Singleplayer
            // 
            this.Singleplayer.BackColor = System.Drawing.Color.LightGreen;
            this.Singleplayer.Location = new System.Drawing.Point(12, 13);
            this.Singleplayer.Name = "Singleplayer";
            this.Singleplayer.Size = new System.Drawing.Size(386, 78);
            this.Singleplayer.TabIndex = 2;
            this.Singleplayer.Text = "Singleplayer";
            this.Singleplayer.UseVisualStyleBackColor = false;
            this.Singleplayer.Click += new System.EventHandler(this.Singleplayer_Click);
            // 
            // Multiplayer
            // 
            this.Multiplayer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Multiplayer.ForeColor = System.Drawing.Color.Black;
            this.Multiplayer.Location = new System.Drawing.Point(12, 110);
            this.Multiplayer.Name = "Multiplayer";
            this.Multiplayer.Size = new System.Drawing.Size(386, 79);
            this.Multiplayer.TabIndex = 3;
            this.Multiplayer.Text = "Multiplayer";
            this.Multiplayer.UseVisualStyleBackColor = false;
            this.Multiplayer.Click += new System.EventHandler(this.Multiplayer_Click);
            // 
            // Resolution_List
            // 
            this.Resolution_List.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Resolution_List.FormattingEnabled = true;
            this.Resolution_List.Items.AddRange(new object[] {
            "1280 x 720",
            "1366 x 768",
            "1920 x 1080"});
            this.Resolution_List.Location = new System.Drawing.Point(12, 220);
            this.Resolution_List.Name = "Resolution_List";
            this.Resolution_List.Size = new System.Drawing.Size(201, 24);
            this.Resolution_List.TabIndex = 4;
            this.Resolution_List.SelectedIndex = 2;
            // 
            // label_Resolution
            // 
            this.label_Resolution.AutoSize = true;
            this.label_Resolution.Location = new System.Drawing.Point(12, 197);
            this.label_Resolution.Name = "label_Resolution";
            this.label_Resolution.Size = new System.Drawing.Size(75, 17);
            this.label_Resolution.TabIndex = 5;
            this.label_Resolution.Text = "Resolution";
            // 
            // Fullscreen
            // 
            this.Fullscreen.AutoSize = true;
            this.Fullscreen.Location = new System.Drawing.Point(303, 220);
            this.Fullscreen.Name = "Fullscreen";
            this.Fullscreen.Size = new System.Drawing.Size(95, 21);
            this.Fullscreen.TabIndex = 6;
            this.Fullscreen.Text = "Fullscreen";
            this.Fullscreen.UseVisualStyleBackColor = true;
            // 
            // ConfigWindow
            // 
            this.ClientSize = new System.Drawing.Size(410, 253);
            this.Controls.Add(this.Fullscreen);
            this.Controls.Add(this.label_Resolution);
            this.Controls.Add(this.Resolution_List);
            this.Controls.Add(this.Multiplayer);
            this.Controls.Add(this.Singleplayer);
            this.MaximizeBox = false;
            this.Name = "ConfigWindow";
            this.Text = "Pong Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button Singleplayer;
        private System.Windows.Forms.Button Multiplayer;
        private System.Windows.Forms.ComboBox Resolution_List;
        private System.Windows.Forms.Label label_Resolution;
        private System.Windows.Forms.CheckBox Fullscreen;
    }
}

