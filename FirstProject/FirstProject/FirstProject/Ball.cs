﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SFML.Graphics;

namespace FirstProject
{
	class Ball
	{
		private Vector2f direction;
		private float velocity;
		private CircleShape ball;
        private Vector2f center;
		private float factor;

		public Ball(Vector2f position, float radius, float faktor, Color color)
		{
			direction = new Vector2f(0,1);
			factor = faktor;
			velocity = .1f * factor;
			ball = new CircleShape(radius * factor);
			ball.Position = position;
			ball.FillColor = color;
            SetRandomDirection();
		}

        public void SetRandomDirection()
        {
            Transform transform = new Transform(1f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 1f);
            Random rand = new Random();
            int angle = 0;
            do
            {
                angle = rand.Next(361);
            } while ((angle >= 315 || angle <= 45) || (angle >= 135 && angle <= 225));
            transform.Rotate(angle);
            direction = transform.TransformPoint(direction.X, direction.Y);
        }
		public Color Color
		{
			get { return ball.FillColor; }
			set { ball.FillColor = value; }
		}
		public Vector2f Position
		{
			get { return ball.Position; }
			set { ball.Position = value; }
		}
        public Vector2f Center
        {
            get { return (new Vector2f(ball.Position.X + Radius, ball.Position.Y + Radius)); }
            set { center = value; ball.Position = new Vector2f(value.X-Radius, value.Y-Radius); }
        }
		public float Radius
		{
			get { return ball.Radius; }
			set { Radius = value; }
		}
		public float Velocity
		{
			get { return velocity; }
			set { velocity = value; }
		}
		public Vector2f Direction
		{
			get { return direction; }
			set { direction = value; }
		}
		public CircleShape Circle
		{
			get { return ball; }
		}
	}
}
