﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

namespace FirstProject
{
    public partial class ConfigWindow : Form
    {

        private Program game;

        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ConfigWindow());
        }

        public ConfigWindow()
        {
            InitializeComponent();
            game = new Program();
        }

        private void Singleplayer_Click(object sender, EventArgs e)
        {
            startGame(true);
        }

        private void Multiplayer_Click(object sender, EventArgs e)
        {
            startGame(false);
        }

        private void startGame(bool bot)
        {
            uint[] res = new UInt32[2];
            float faktor = 1;
            switch (Resolution_List.SelectedItem.ToString())
            {
                case ("1280 x 720"):
                    res[0] = 1280;
                    res[1] = 720;
                    faktor = 1f;
                    break;
                case ("1366 x 768"):
                    res[0] = 1366;
                    res[1] = 768;
                    faktor = (1366 * 768) / (1280 * 720);
                    break;
                case ("1920 x 1080"):
                    res[0] = 1920;
                    res[1] = 1080;
                    faktor = (1920 * 1080) / (1280 * 720);
                    break;
            }

            game.Run(res, Fullscreen.Checked, faktor, bot);
            Application.Exit();
        }
    }
}
