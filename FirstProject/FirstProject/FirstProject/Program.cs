﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.Graphics;
using SFML.Window;
using SFML.System;
using System.Windows.Forms;

namespace FirstProject
{
    class Program
    {
        private RenderWindow window;
        private DateTime current_time;
        private DateTime previous_time;
        private bool stopGame = false;
		private bool pauseGame = true;
        private bool bot = false;
        private Text rlyQuit;
        private Text scoreMsgLeft;
        private Text scoreMsgRight;
		private Text startMessage;
		private uint[] res;
        private float faktor;
        public bool running = true;
        private RectangleShape UpperDivider;
        private RectangleShape LowerDivider;

        private Player[] player;
		private Ball ball;

        public void Run(UInt32[] res, bool fullscreen, float faktor, bool bot)
        {
            this.bot = bot;
            this.res = res;
            this.faktor = faktor;
            Initialize(fullscreen);
            while (running)
            {
                current_time = DateTime.Now;
                double delta = current_time.Subtract(previous_time).TotalMilliseconds;
                previous_time = current_time;

				window.DispatchEvents();

				Update(delta);
                Render();
            }
        }

        private void Initialize(bool fullscreen)
        {
            if (fullscreen)
            {
                window = new RenderWindow(new VideoMode(res[0], res[1]), "Pong", Styles.Fullscreen);
            }
            else
            {
                window = new RenderWindow(new VideoMode(res[0], res[1]), "Pong", Styles.Default);
            }

			window.Closed += (sender, evtArgs) => stopGame = true;
			window.KeyPressed += keyHandlerPress;
            window.KeyReleased += keyHandlerRelease;
            previous_time = DateTime.Now;
            player = new Player[2];

            initializeFont();

            //player = new Player(new Vector2u(20,20), new Vector2f(10,10));
            //player.setTexture("C:/Users/Jens/Desktop/6. Semester/Programming Patterns/FirstProject/pall.png");

            UpperDivider = new RectangleShape(new Vector2f(res[0], 20));
            LowerDivider = new RectangleShape(new Vector2f(res[0], 20));
            UpperDivider.Position = new Vector2f(0, 0);
            LowerDivider.Position = new Vector2f(0, res[1] - 20);
            UpperDivider.FillColor = Color.Green;
            LowerDivider.FillColor = Color.Green;

            GameRestart();
        }

        private void initializeFont()
        {
            Font standardFont = new Font("../../../firstProject/font/coure.ttf");
            float staticCharSizerS;
            Vector2f Bounds = new Vector2f();

            //Quit Message
            rlyQuit = new Text();
            rlyQuit.Font = standardFont;
            rlyQuit.DisplayedString = "Do you really want to quit? \nPress ENTER to commit \nPress ESC to deny";
            rlyQuit.Color = Color.Green;
            Bounds.X = rlyQuit.GetGlobalBounds().Width;
            Bounds.Y = rlyQuit.GetGlobalBounds().Height;
            rlyQuit.Position = new Vector2f((res[0] / 2) - (Bounds.X / 2), (res[1] / 2) - (Bounds.Y / 2));

			//Start Message
			startMessage = new Text();
			startMessage.Font = standardFont;
			startMessage.DisplayedString = "Press SPACE to start the round";
			startMessage.Color = Color.Green;
			Bounds.X = startMessage.GetGlobalBounds().Width;
			Bounds.Y = startMessage.GetGlobalBounds().Height;
			startMessage.Position = new Vector2f((res[0] / 2) - (Bounds.X / 2), ((res[1] / 3)*2) - (Bounds.Y / 2));

			//Score Left
			scoreMsgLeft = new Text();
            scoreMsgLeft.Font = standardFont;
            scoreMsgLeft.DisplayedString = "0";
            staticCharSizerS = 100 * faktor;
            scoreMsgLeft.Color = Color.Green;
            Bounds.X = scoreMsgLeft.GetGlobalBounds().Width;
            Bounds.Y = scoreMsgLeft.GetGlobalBounds().Height;
            scoreMsgLeft.Position = new Vector2f((res[0] / 4)-Bounds.X, 20);
            scoreMsgLeft.CharacterSize = (uint) staticCharSizerS;

            //Score Right
            scoreMsgRight = new Text();
            scoreMsgRight.Font = standardFont;
            scoreMsgRight.DisplayedString = "0";
            scoreMsgRight.Color = Color.Green;
            Bounds.X = scoreMsgRight.GetGlobalBounds().Width;
            Bounds.Y = scoreMsgRight.GetGlobalBounds().Height;
            scoreMsgRight.Position = new Vector2f(((res[0] / 2) + (res[0] / 4))-Bounds.X, 20);
            scoreMsgRight.CharacterSize = (uint) staticCharSizerS;
        }

        private void Update(double delta)
        {
            //Player movement
            if (!stopGame && !pauseGame)
            {
                foreach (Player p in player)
                {
                    Vector2f pos = p.getPosition();
                    float velocity = (float)delta * p.Velocity;

                    if (p.getMovingUp())
                    {
                        if (pos.Y > 20)
                        {
                            pos.Y -= velocity;
                        }
                        p.setPositionRect(pos);
                    }
                    else if (p.getMovingDown())
                    {
                        if (pos.Y < (res[1] - 110*faktor))
                        {
                            pos.Y += velocity;
                        }
                        p.setPositionRect(pos);
                    }
                }
                if (bot)
                {
                    botMovement();
                }

                //Check if a Player scored
                if (ball.Center.X <= 0 || ball.Center.X >= res[0])
                {
                    if (ball.Center.X <= res[0])
                    {
                        player[1].Scored();
                        scoreMsgRight.DisplayedString = "" + player[1].Score;
                    }
                    else
                    {
                        player[0].Scored();
                        scoreMsgLeft.DisplayedString = "" + player[0].Score;
                    }
                    RoundRestart();
                }

                //collision detection with borders
                if (ball.Center.Y <= 20 + ball.Radius || ball.Center.Y >= res[1]-20 - ball.Radius)
                {
                    ball.Direction = new Vector2f(ball.Direction.X, -ball.Direction.Y);
                }

                //ball movement with collision detection
                foreach (Player p in player)
                {
                    if (!IsCollision(ball, p))
                    {
                        ball.Position = ball.Position + ((float) delta * ball.Direction * ball.Velocity);
                    }
                    else
                    {
                    }
                }
            }
        }

		private bool IsCollision(Ball ball, Player player)
		{

            if (player.getRect().GetGlobalBounds().Contains(ball.Center.X + (ball.Velocity * ball.Direction.X), ball.Center.Y + (ball.Velocity * ball.Direction.Y)))
            {
                ball.Direction = new Vector2f(-ball.Direction.X, ball.Direction.Y);
                ball.Velocity = ball.Velocity + (0.03f);
            }

            //(x-m)^2 + (y-m)^2 = r^2
            /*if (player.isRightPlayer)
            {
                //float x = player.getPosition().X - ball.Radius;
                //float y = player.getPosition().Y + t * (player.getPosition().Y + 1.0f - player.getPosition().Y);

                //float x = ball.Position.X + v * (ball.Velocity * ball.Direction.X);
                //float y = ball.Position.Y + v * (ball.Velocity * ball.Direction.Y);

                float px = player.getPosition().X;
                float py = player.getPosition().Y;

                float rx = 0;
                float ry = (player.getPosition().Y + 1.0f - player.getPosition().Y);

                float bx = ball.Position.X;
                float by = ball.Position.Y;

                float kx = ball.Velocity * ball.Direction.X;
                float ky = ball.Velocity * ball.Direction.Y;

                //float lambda = 

            }*/
			return false;
		}

        private void GameRestart()
        {
            player[0] = new Player(10, 100, new Vector2f(0, 0), Color.Green, faktor);
            player[1] = new Player(10, 100, new Vector2f(0, 0), Color.Green, faktor, true, 0);
            RoundRestart();
        }

        private void RoundRestart()
        {
			pauseGame = true;
            player[0].setPositionRect(new Vector2f(10, (res[1] / 2) - (player[0].getRect().GetGlobalBounds().Height / 2)));
            player[1].setPositionRect(new Vector2f((res[0] - 30), (res[1] / 2) - (player[1].getRect().GetGlobalBounds().Height / 2)));
            ball = new Ball(new Vector2f(res[0] / 2, res[1] / 2), 10, faktor, Color.Cyan);
        }

        private void Render()
		{
			window.Clear(Color.Black);
			if (stopGame)
			{
				window.Draw(rlyQuit);
			}
			if (pauseGame && !stopGame)
			{
				window.Draw(startMessage);
			}
			window.Draw(UpperDivider);
            window.Draw(LowerDivider);
			window.Draw(player[0].getRect());
            window.Draw(player[1].getRect());
			window.Draw(ball.Circle);
            window.Draw(scoreMsgLeft);
            window.Draw(scoreMsgRight);
            window.Display();
		}

        private void botMovement()
        {
            float playerPos = player[1].getPosition().Y + (player[1].getRect().GetGlobalBounds().Height / 2);
            if (playerPos < ball.Center.Y)
            {
                player[1].setMovingUp(false);
                player[1].setMovingDown(true);
            }else if(playerPos > ball.Center.Y)
            {
                player[1].setMovingDown(false);
                player[1].setMovingUp(true);
            }else if (playerPos == ball.Center.Y)
            {
                player[1].setMovingDown(false);
                player[1].setMovingUp(false);
            }
        }

        private void keyHandlerPress(object sender, SFML.Window.KeyEventArgs ev)
        {
            switch (ev.Code)
            {
                case Keyboard.Key.W:
                    player[0].setMovingDown(false);
                    player[0].setMovingUp(true);
                    break;
                case Keyboard.Key.S:
                    player[0].setMovingUp(false);
                    player[0].setMovingDown(true);
                    break;
                case Keyboard.Key.Up:
                    if (!bot)
                    {
                        player[1].setMovingDown(false);
                        player[1].setMovingUp(true);
                    }
                    break;
                case Keyboard.Key.Down:
                    if (!bot)
                    {
                        player[1].setMovingUp(false);
                        player[1].setMovingDown(true);
                    }
                    break;
                case Keyboard.Key.Escape:
                    stopGame = !stopGame;
                    break;
                case Keyboard.Key.Return:
                    if (stopGame)
                    {
                        running = false;
                    }
                    break;
				case Keyboard.Key.Space:
					if (pauseGame & !stopGame)
					{
						pauseGame = false;
					}
					break;
            }
        }

        private void keyHandlerRelease(object sender, SFML.Window.KeyEventArgs ev)
        {
            switch (ev.Code)
            {
                case Keyboard.Key.W:
                    player[0].setMovingUp(false);
                    break;
                case Keyboard.Key.S:
                    player[0].setMovingDown(false);
                    break;
                case Keyboard.Key.Up:
                    player[1].setMovingUp(false);
                    break;
                case Keyboard.Key.Down:
                    player[1].setMovingDown(false);
                    break;
            }
        }
    }
}
