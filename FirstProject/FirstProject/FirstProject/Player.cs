﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SFML.System;
using SFML.Graphics;

namespace FirstProject
{
    class Player
    {

        //Falls ein Sprite geladen wird
        private Texture texture;
        private Sprite sprite = new Sprite();

        //Falls ein Shape generiert wird
        private RectangleShape rect;

        //In jedem Fall benötigt
        private Vector2f position;
        private int rotate;
        private float faktor;
        private bool movingUp;
        private bool movingDown;
        private float velocity;
        private int score;
        public bool isRightPlayer;

		public Player(Vector2u size, Vector2f position, int rotate = 0)
        {
            sprite.Position = position;
            sprite.Rotation = rotate;
            this.rotate = rotate;
            this.position = position;
        }

        public Player(float width, float height, Vector2f position, Color color, float faktor, bool isRightPlayer = false, int rotate = 0)
        {
            rect = new RectangleShape(new Vector2f((width * faktor), (height * faktor)));
            this.velocity = .4f * faktor;
            this.faktor = faktor;
            rect.FillColor = color;
            rect.Rotation = rotate;
            rect.Position = position;
            this.rotate = rotate;
            this.position = position;
            score = 0;
            this.isRightPlayer = true;
        }

        public Vector2f GetCollisionSide()
        {
            if (isRightPlayer)
            {
                
            }
            return new Vector2f(0, 0);
        }

        public void Scored()
        {
            score++;
        }

		public float Velocity
		{
			get { return velocity; }
			set { velocity = value * faktor; }
		}

        public float Score
        {
            get { return score; }
        }

        public void setTexture(String url)
        {
            texture = new Texture(new Image(url));
            sprite.Texture = texture;
        }

        public void setColor(Color c)
        {
            sprite.Color = c;
        }

        public Vector2f getPosition()
        {
            return position;
        }

        public int getRotation()
        {
            return rotate;
        }

        public void setPositionSprite(Vector2f pos)
        {
            sprite.Position = pos;
            position = pos;
        }

        public void setPositionRect(Vector2f pos)
        {
            rect.Position = pos;
            position = pos;
        }

        public void setRotation(int rotate)
        {
            sprite.Rotation = rotate;
        }

        public Sprite getSprite()
        {
            return sprite;
        }

        public RectangleShape getRect()
        {
            return rect;
        }

        public bool getMovingUp()
        {
            return movingUp;
        }

        public bool getMovingDown()
        {
            return movingDown;
        }

        public void setMovingUp(bool moving)
        {
            movingUp = moving;
        }

        public void setMovingDown(bool moving)
        {
            movingDown = moving;
        }

    }
}
